#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <string.h>
#define MaxSize 100		//串中最多字符个数

void DispStr(char* s);
int Index(char* s, char* t);

int main()
{
    int i, j, n;
    char  ss[100], tt[100];
    //printf("请输入主串：\n");
    scanf("%s", ss);
    //printf("请输入子串：\n");
    scanf("%s", tt);
    //printf("请输入在字符串中从第i位开始输出长度为j的子串：\n");
    //scanf("%d%d", &i, &j);
    n = Index(ss, tt);
    if (n != -1)
        printf("%d\n", n);
    else
        printf("fail\n");
    return 0;
}
void DispStr(char* s)
{
    int i;
    for (i = 0; s[i] != '\0'; i++)
        printf("%c", s[i]);
    printf("\n");
}

int Index(char* s, char* t)
{
    /********** Begin **********/
    int i, j;
    int slen = strlen(s);
    int tlen = strlen(t);
    if (slen < tlen)
    {
        return 0;
    }
    for (i = 0; i < slen; i++)
    {
        int ret = i;
        for (j = 0; j < tlen + 1; j++)
        {
            if (t[j] == '\0')
            {
                return i - tlen;
            }
            if (s[i] != t[j])
            {
                break;
            }
            i++;

        }
        i = ret;
    }
    return 0;
    /********** End **********/
}