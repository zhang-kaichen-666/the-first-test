#define _CRT_SECURE_NO_WARNINGS 1
//按如下函数原型编程实现字符串连接函数strcat()的功能。
//void MyStrcat(char dstStr[], char srcStr[]);
//输入提示信息：
//"Input a string:"
//"Input another string:"
//输入字符串用gets()
//输出提示信息和格式："Concatenate results:%s\n"
//#include<stdio.h>
//#include<string.h>
//void MyStrcat(char dstStr[], char srcStr[])
//{
//	int i = strlen(dstStr);
//	for (int j = 0; j <= strlen(srcStr); j++)
//	{
//		dstStr[i++] = srcStr[j];
//	}
//}
//int main()
//{
//	char str1[100];
//	char str2[100];
//	printf("Input a string:");
//	gets(str1);
//	printf("Input another string:");
//	gets(str2);
//	MyStrcat(str1, str2);
//	printf("Concatenate results:%s\n", str1);
//	return 0;
//}

//用字符指针作函数参数编程实现如下功能：在字符串中删除与某字符相同的字符。
//** 提示信息：
//"Input a string:"
//"Input a character:"
//* *输入格式要求："%s"
//* *输出格式要求："Results:%s\n"
//程序运行示例1如下：
//Input a string : hello, world!
//Input a character : o
//Results : hell, wrld!
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char str[100];
//	char c;
//	int i = 0;
//	printf("Input a string:");
//	gets(str);
//	printf("Input a character:");
//	scanf("%c", &c);
//	while (str[i] != '\0')
//	{
//		if (str[i] == c)
//		{
//			int j = i;
//			while (str[j] != '\0')
//			{
//				str[j] = str[j + 1];
//				j++;
//			}
//		}
//		else
//		{
//			i++;
//		}
//	}
//	printf("Results:%s\n", str);
//	return 0;
//}