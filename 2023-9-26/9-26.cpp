#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>


//使用所学文件操作，在当前目录下放一个文件data.txt，写一个程序，将data.txt文件拷贝一份，生成data_copy.txt文件。
//
//基本思路：
//
//打开文件data.txt，读取数据
//打开文件data_copy.txt，写数据
//从data.txt中读取数据存放到data_copy.txt文件中，直到文件结束。
//关闭两个文件
//int main()
//{
//	FILE* pfread = fopen("data.txt", "r");
//	if (pfread == NULL)
//	{
//		perror("fopen-pfread");
//		return 1;
//	}
//	FILE* pfwrite = fopen("data_copy", "w");
//	if (pfwrite == NULL)
//	{
//		fclose(pfread);
//		pfread = NULL;
//		perror("fopen-pfwrite");
//		return 1;
//	}
//	int ch = 0;
//	while ((ch = getc(pfread)) != EOF)
//	{
//		fputc(ch, pfwrite);
//	}
//	fclose(pfread);
//	pfread = NULL;
//	fclose(pfwrite);
//	pfwrite = NULL;
//
//	return 0;
//}

//写一个宏，可以将一个整数的二进制位的奇数位和偶数位交换。
//01010101010101010101010101010101
//10101010101010101010101010101010
//#define SWAP(n) (n = (((n)&(0x55555555))<<1) + (((n)&(0xAAAAAAAA))>>1))
//int main()
//{
//	int a = 10;
//	a = SWAP(a);
//	printf("%d\n", a);
//	return 0;
//}