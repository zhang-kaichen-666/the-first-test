#include<stdio.h>
#include<string.h>
#include<stdlib.h>
//模仿qsort的功能实现一个通用的冒泡排序
int_cmp(const void* e1, const void* e2)
{
	return *((int*)e1) - *((int*)e2);
}
char_cmp(const void* e1, const void* e2)
{
	return *((char*)e1)-*((char*)e2);

}
swap(void* p1, void* p2, int size)
{
	int i = 0;
	for (i = 0; i < size; i++)
	{
		char tmp = *((char*)p1 + i);
		*((char*)p1 + i) = *((char*)p2 + i);
		*((char*)p2 + i) = tmp;
	}
}
print_int(int arr[], int sz)
{
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");
}
print_str(char str[], size_t sz)
{
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%c ", str[i]);
	}
}
void bubble_sort(void* base, int count, int size, int (*cmp)(void*, void*))
{
	int i = 0;
	int j = 0;
	for (i = 0; i < count - 1; i++)
	{
		for (j = 0; j < count - 1 - i; j++)
		{
			if (cmp((char*)base + j * size , (char*)base+(j + 1) * size) > 0)
			{
				swap((char*)base + j * size,(char*)base + (j + 1) * size,size);
			}
		}
	}
}
int main()
{
	int arr[10] = { 5,6,1,2,3,4,8,7,9,0 };
	char str[5] = {'a','b','d','e','c'};
	int sz = sizeof(arr) / sizeof(arr[0]);
	int sz2 = sizeof(str) / sizeof(str[0]);
	bubble_sort(arr, sz, sizeof(arr[0]), int_cmp);
	print_int(arr, sz);
	bubble_sort(str, sz2, sizeof(str[0]), char_cmp);
	print_str(str, sz2);
	return 0;
}