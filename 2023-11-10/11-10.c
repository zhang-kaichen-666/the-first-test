﻿#define _CRT_SECURE_NO_WARNINGS 1
//求数组a[10]中所有素数的元素之和，函数int isprime（int  n）用来判断n是否是素数。素数是只能被1和本身整除且大于1的自然数。
//输入提示："input 10 numbers:"
//输入格式："%d"
//输出格式："sum=%d\n"
//#include<stdio.h>
//#include<math.h>
//int isprime(int n)
//{
//	if (n == 2)
//	{
//		return 1;
//	}
//	for (int i = 2; i <= sqrt(n); i++)
//	{
//		if (n % i == 0)
//		{
//			return 0;
//		}
//	}
//	return 1;
//}
//int main()
//{
//	int a[10],sum = 0;
//	printf("input 10 numbers:");
//	for (int i = 0; i < 10; i++)
//	{
//		scanf("%d", &a[i]);
//		if (isprime(a[i]))
//		{
//			sum += a[i];
//		}
//	}
//	printf("sum=%d\n", sum);
//	return 0;
//}

//从键盘任意输入a，b，c的值，编程计算并输出一元二次方程ax2 + bx + c = 0的根
//（较小的先输出，即先输出p - q，后输出p + q）。根据一元二次方程的求根公式，令
//p = −b2a, q = b2−4ac√2a
//假设a，b，c的值能保证方程有两个不相等的实根（即b2 - 4ac > 0）
//* *输入提示信息："Please enter the coefficients a,b,c:"
//* *输入格式要求："%f,%f,%f"
//* *输出格式要求：
//"x1=%7.4f, x2=%7.4f\n"
//#include<stdio.h>
//#include<math.h>
//int main()
//{
//	float a, b, c, p, q;
//	printf("Please enter the coefficients a,b,c:");
//	scanf("%f,%f,%f", &a, &b, &c);
//	p = (-b) / (2 * a);
//	q = sqrt(b * b - 4 * a * c) / (2 * a);
//	printf("x1=%7.4f, x2=%7.4f\n", p - q, p + q);
//	return 0;
//}

//计算体指数
//从键盘输入某人的身高（以厘米为单位，如174cm）和体重（以公斤为单位，如70公斤），
// 将身高（以米为单位，如1.74m）和体重（以斤为单位，如140斤）输出在屏幕上，
// 并按照以下公式计算并输出体指数，要求结果保留到小数点后2位。
//假设体重为w公斤，身高为h米，则体指数的计算公式为：t = w / (h * h)
//以下是程序的输出示例：
//Input weight, height:
//70, 174↙
//weight = 140
//height = 1.74
//t = 23.12
//输入格式 : "%d,%d"
//输入提示信息："Input weight, height:\n"    (注意：在height和逗号之间有一个空格)
//体重输出格式："weight=%d\n"
//身高输出格式："height=%.2f\n"
//体指数输出格式："t=%.2f\n"
//#include<stdio.h>
//int main()
//{
//	int height,weight;
//	double t,h;
//	printf("Input weight, height:\n");
//	scanf("%d,%d", &weight, &height);
//	h = (double)height / 100;
//	printf("weight=%d\n", weight * 2);
//	printf("height=%.2f\n", h);
//	t = weight / (h * h);
//	printf("t=%.2f\n", t);
//	return 0;
//}

//用const常量定义圆周率pi（取值为3.14159），编程从键盘输入圆的半径r，计算并输出圆的周长和面积。输出的数据保留两位小数点。
//* *输入格式要求："%lf" 提示信息："Input r:"
//* *输出格式要求："printf WITHOUT width or precision specifications:\n" 
//"circumference = %f, area = %f\n" 
//"printf WITH width and precision specifications:\n" 
//"circumference = %7.2f, area = %7.2f\n"
//程序运行示例如下：
//Input r : 5.3
//printf WITHOUT width or precision specifications :
//circumference = 33.300854, area = 88.247263
//printf WITH width and precision specifications :
//circumference = 33.30, area = 88.25
//#include<stdio.h>
//int main()
//{
//	const double pi = 3.14159;
//	double r, circumference, area;
//	printf("Input r:");
//	scanf("%lf", &r);
//	printf("printf WITHOUT width or precision specifications:\n");
//	circumference = 2 * pi * r;
//	area = pi * r * r;
//	printf("circumference = %f, area = %f\n", circumference, area);
//	printf("printf WITH width and precision specifications:\n");
//	printf("circumference = %7.2f, area = %7.2f\n", circumference, area);
//	return 0;
//}

//用计数控制的循环实现正数累加求和
//输入一些整数，编程计算并输出其中所有正数的和，输入负数时不累加，继续输入下一个数。
//输入零时，表示输入数据结束。要求最后统计出累加的项数。
//程序运行结果示例：
//Input a number :
//1↙
//Input a number :
//3↙
//Input a number :
//4↙
//Input a number :
//2↙
//Input a number :
//-8↙
//Input a number :
//-9↙
//Input a number :
//0↙
//sum = 10, count = 4
//输入格式 : "%d"
//输出格式：
//输入提示信息： "Input a number:\n"
//输出格式： "sum=%d,count=%d\n"
//#include<stdio.h>
//int main()
//{
//	int count = 0, sum = 0;
//	while (1)
//	{
//		printf("Input a number:\n");
//		int a;
//		scanf("%d", &a);
//		if (a > 0)
//		{
//			sum += a;
//			count++;
//		}
//		else if (a < 0)
//		{
//			continue;
//		}
//		else
//		{
//			break;
//		}
//	}
//	printf("sum=%d,count=%d\n", sum, count);
//	return 0;
//}

//从键盘为3 * 3的矩阵输入数据，找出主对角线上最大的元素，以及所在的行号。
//* *输入格式要求："%d"
//* *输出格式要求："max=%d ,row=%d"
//程序的运行示例如下：
//1 2 3
//4 5 6
//7 8 9
//max = 9, row = 2
//#include<stdio.h>
//int main()
//{
//	int arr[3][3],max = 0,row;
//	for (int i = 0; i < 3; i++)
//	{
//		for (int j = 0; j < 3; j++)
//		{
//			scanf("%d", &arr[i][j]);
//			if (i == j && max < arr[i][j])
//			{
//				max = arr[i][j];
//				row = i;
//			}
//		}
//	}
//	printf("max=%d ,row=%d", max, row);
//	return 0;
//}