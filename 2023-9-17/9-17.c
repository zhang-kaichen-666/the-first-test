﻿#include<stdio.h>
#include<stdlib.h>

//结构体实现位段
//位段(基于结构体的)(用来节省空间)：位->二进制位
//位段的成员必须是int unsigned int 或signed int 
//例：
//struct A//(结构体实现的位段)
//{
//	int _a : 2;//2指的是_a占2个bit位
//	int _b : 5;
//	int _c : 10;
//	int _d : 30;
//};
//struct B
//{
//	int _a;
//	int _b;
//	int _c;
//	int _d;
//};
//int main()
//{
//	printf("%d\n", sizeof(struct A));//8
//	printf("%d\n", sizeof(struct B));//16
//	return 0;
//}

//位段的内存分配
//1. 位段的成员可以是 int unsigned int signed int 或者是 char 等类型
//2. 位段的空间上是按照需要以4个字节（ int ）或者1个字节（ char ）的⽅式来开辟的。
//3. 位段涉及很多不确定因素，位段是不跨平台的，注重可移植的程序应该避免使⽤位段。
// 
//当开辟了内存后，内存中每个比特位从左向右使用还是从右向左使用是不确定的(VS2022中从右向左使用)
//当前面使用后，剩余的空间不足下一个成员使用时，剩余的空间是否使用，这个不确定(VS2022中剩余空间不使用)
//struct S
//{
//	char a : 3;
//	char b : 4;
//	char c : 5;
//	char d : 4;
//};
//int main()
//{
//	printf("%d", sizeof(struct S));//3
//	return 0;
//}

//位段的跨平台问题:
//1. int 位段被当成有符号数还是⽆符号数是不确定的。
//2. 位段中最⼤位的数⽬不能确定。（16位机器最⼤16，32位机器最⼤32，写成27，在16位机器会出问题。
//3. 位段中的成员在内存中从左向右分配，还是从右向左分配标准尚未定义。
//4. 当⼀个结构包含两个位段，第⼆个位段成员⽐较⼤，⽆法容纳于第⼀个位段剩余的位时，是舍弃剩余的位还是利⽤，这是不确定的。
//位段的注意事项：
//位段的⼏个成员共有同⼀个字节，这样有些成员的起始位置并不是某个字节的起始位置，那么这些位
//置处是没有地址的。内存中每个字节分配⼀个地址，⼀个字节内部的bit位是没有地址的。
//所以不能对位段的成员使⽤& 操作符，这样就不能使⽤scanf直接给位段的成员输⼊值，只能是先输⼊
//放在⼀个变量中，然后赋值给位段的成员。
//struct A
//{
//	int _a : 2;
//	int _b : 5;
//	int _c : 10;
//	int _d : 30;
//};
//int main()
//{
//	struct A sa = { 0 };
//	//scanf("%d", &sa._b);//这是错误的
//
//	//正确的⽰范
//	int b = 0;
//	scanf("%d", &b);
//	sa._b = b;
//	return 0;
//}

//自定义类型：联合体
//像结构体⼀样，联合体也是由⼀个或者多个成员构成，这些成员可以不同的类型。但是编译器只为最
//⼤的成员分配⾜够的内存空间。联合体的特点是所有成员共⽤同⼀块内存空间。所以联合体也叫：共⽤体。

//结构体
//struct S
//{
//	char C;
//	int i;
//};
//联合体(共用体)
//union U
//{
//	char C;
//	int i;
//};
//int main()
//{
//	union U u = { 0 };
//	printf("%d\n", sizeof(struct S));//8
//	printf("%d\n", sizeof(u));//4
//
//	printf("%p\n", & u);
//	printf("%p\n", &(u.i));
//	printf("%p\n", &(u.C));//三个地址相同
//	return 0;
//}

//union Un
//{
//	char c;
//	int i;
//};
//int main()
//{
//	//联合变量的定义
//	union Un un = { 0 };
//	un.i = 0x11223344;
//	un.c = 0x55;
//	printf("%x\n", un.i);// 11223355
//	return 0;
//}

// 大小计算:
//联合的⼤⼩⾄少是最⼤成员的⼤⼩。
//当最⼤成员⼤⼩不是最⼤对⻬数的整数倍的时候，就要对⻬到最⼤对⻬数的整数倍。
//union Un1
//{
//	char c[5];//1  8  1     5->8
//	int i;//4  8  4
//};
//union Un2
//{
//	short c[7];//2  8  2    14->16
//	int i;//4  8  4
//};
//int main()
//{
//	printf("%d\n", sizeof(union Un1));//8
//	printf("%d\n", sizeof(union Un2));//16
//	return 0;
//}
//联合体的大小，是最大成员的大小。(错误)
//使用联合体可以节省空间

//使用联合体实例：
//struct gift_list
//{
//	int stock_number;//库存量
//	double price; //定价
//	int item_type;//商品类型
//
//	union {
//		struct
//		{
//			char title[20];//书名
//			char author[20];//作者
//			int num_pages;//⻚数
//		}book;
//		struct
//		{
//			char design[30];//设计
//		}mug;
//		struct
//		{
//			char design[30];//设计
//			int colors;//颜⾊
//			int sizes;//尺⼨
//		}shirt;
//	}item;
//};

//写⼀个程序，判断当前机器是⼤端？还是⼩端？(使用联合体)
//int check_sys()
//{
//	union
//	{
//		int i;
//		char c;
//	}un;
//	un.i = 1;//0x 01 00 00 00
//	return un.c;
//}
//int main()
//{
//	if (check_sys() == 1)
//		printf("小端");
//	else
//		printf("大端");
//	return 0;
//}

//自定义类型：枚举类型(枚举常量一般用大写)
//例:
//enum Day//星期
//{
//	//列出的是枚举类型的可能取值
//	//这些列出的可能取值被称为：枚举常量
//	Mon,
//	Tues,
//	Wed,
//	Thur,
//	Fri,
//	Sat,
//	Sun
//};
//enum Sex//性别
//{
//	MALE = 4,//0     可以在这里修改初始值->4
//	FEMALE,//1                           ->5
//	SECRET//2                            ->6
//};
//enum Color//颜⾊
//{
//	RED,
//	GREEN,
//	BLUE
//};
//int main()
//{
//	printf("%d %d %d %d %d %d %d \n", Mon, Tues, Wed, Thur, Fri, Sat, Sun);//0 1 2 3 4 5 6
//	printf("%d %d %d \n", MALE, FEMALE, SECRET);//0 1 2
//	//MALE = 4;//会报错，因为常量不能被修改
//	
//	//真实用法：
//	enum Sex s = MALE;
//	enum Day d = Sun;
//	return 0;
//}

//枚举类型的优点:
//1. 增加代码的可读性和可维护性
//2. 和#define定义的标识符⽐较枚举有类型检查，更加严谨。
//3. 便于调试，预处理阶段会删除 #define 定义的符号
//4. 使⽤⽅便，⼀次可以定义多个常量
//5. 枚举常量是遵循作⽤域规则的，枚举声明在函数内，只能在函数内使⽤
// 
//enum Color//颜⾊
//{
//	RED = 1,
//	GREEN = 2,
//	BLUE = 4
//};
//enum Color clr = GREEN;//使⽤枚举常量给枚举变量赋值
//int main()
//{
//	printf("%d\n", clr);
//	return 0;
//}

//动态内存管理
//int main()
//{
//	int val = 20;//在栈空间上开辟四个字节
//	char arr[10] = { 0 };//在栈空间上开辟10个字节的连续空间
//	//但是   • 空间开辟⼤⼩是固定的。
//	//       • 数组在申明的时候，必须指定数组的⻓度，数组空间⼀旦确定了⼤⼩不能调整
//	//变长数组只能说是数组的大小可以使用变量来指定，但是数组一旦创建好后，依然不能调整大小
//}
//由此引入动态内存分配