#include<stdio.h>
#include<stdlib.h>
#include<string.h>

//编程将字符串s倒序输出，要求利用函数递归实现。
//** 输入格式要求："%s" 提示信息："input your string:\n"
//* *输出格式要求："%c"
//程序运行的输入输出样例：
//屏幕先输出提示信息：
//input your string :
//然后用户键盘输入：
//abcdefg
//最后屏幕输出：
//gfedcba
//void reverse(char* s)
//{
//	if (strlen(s) == 1)
//	{
//		printf("%c", s[0]);
//	}
//	else
//	{
//		reverse(s + 1);
//		printf("%c", s[0]);
//	}
//}
//int main()
//{
//	char s[100] = "";
//	printf("input your string:\n");
//	scanf("%s", s);
//	reverse(s);
//
//	return 0;
//}

//#include "stdio.h"
//#include "string.h"
//main()
//{
//    int i = 0;
//    int findFlag = 1;
//    char x[13];
//    char* str[13] = { "Pascal","Basic","Fortran", "Java","Visual C", "Visual Basic" };
//
//    printf("Input string:\n");
//    gets(x);
//
//    while (i < 6 && findFlag)
//    {
//        if (strcmp(x, str[i]) == 0)
//        {
//            findFlag = 0;
//        }
//        i++;
//    }
//    if (findFlag == 0)
//    {
//        printf("%s\n", x);
//    }
//    else
//    {
//        printf("Not find!\n");
//    }
//}

//在主函数中输入一个3X4的整型矩阵，求其转置矩阵并存放在另一个二维数组中 在主函数中将转置后的新数组输出。（注：转置矩阵 把矩阵的行换成相应的列，得到的新矩阵称为其转置矩阵）
//利用以下函数完成转置功能。
//void Convert（int a[][4], int b[][3]);
//
//**输入格式要求： "%d"
//* *输出格式要求：" %4d"
//void Convert(int a[][4], int b[][3])
//{
//	for (int i = 0; i < 3; i++)
//	{
//		for (int j = 0; j < 4; j++)
//		{
//			b[j][i] = a[i][j];
//		}
//	}
//}
//int main()
//{
//	int arr[3][4] = {0};
//	for (int i = 0; i < 3; i++)
//	{
//		for (int j = 0; j < 4; j++)
//		{
//			scanf("%d", &arr[i][j]);
//		}
//	}
//	int arr2[4][3] = {0};
//	Convert(arr,arr2);
//	for (int i = 0; i < 4; i++)
//	{
//		for (int j = 0; j < 3; j++)
//		{
//			printf(" %4d", arr2[i][j]);
//		}
//		printf("\n");
//	}
//	return 0;
//}

//从键盘输入n个整数，用函数编程实现计算其最大值和最小值，并互换它们所在数组中的位置。
//要求按如下函数原型编写程序
//void ReadData(int a[], int n);
//void PrintData(int a[], int n);
//void  MaxMinExchang(int a[], int n);
//**输入提示信息要求：
//"Input n(n<=10):\n"
//"Input %d numbers:\n"
//* *要求输入格式为："%d"
//* *输出提示信息："Exchange results:"
//* *要求输出格式为："%5d"
//void ReadData(int a[], int n)
//{
//	for (int i = 0; i < n; i++)
//	{
//		scanf("%d", &a[i]);
//	}
//}
//void PrintData(int a[], int n)
//{
//	for (int i = 0; i < n; i++)
//	{
//		printf("%5d", a[i]);
//	}
//}
//void  MaxMinExchang(int a[], int n)
//{
//	int max = 0;
//	int min = 0;
//	for (int i = 0; i < n; i++)
//	{
//		if (a[max] <= a[i])
//		{
//			max = i;
//		}
//		if (a[min] >= a[i])
//		{
//			min = i;
//		}
//	}
//	a[max] = a[max] ^ a[min];
//	a[min] = a[max] ^ a[min];
//	a[max] = a[max] ^ a[min];
//
//}
//int main()
//{
//	int a[100] = { 0 };
//	int n = 0;
//	printf("Input n(n<=10):\n");
//	scanf("%d", &n);
//	printf("Input %d numbers:\n",n);
//	ReadData(a, n);
//	MaxMinExchang(a, n);
//	printf("Exchange results:");
//	PrintData(a, n);
//	return 0;
//}

//建立一个链表，使链表中从头到尾的结点数据域依次是一个数组的各个元素的值。程序先建立链表然后再遍历输出（假定链表和数组均有6个整型元素）。
//程序运行示例如下：
//输入数组6个元素的值。
//1 3 5 7 9 11
//此链表各个结点的数据域为：1 3 5 7 9 11
//typedef struct Linklist* PL;
//struct Linklist
//{
//	int info;
//	PL link;
//};
//PL CreateLinkList(int a[])
//{
//	PL phead = (PL)malloc(sizeof(struct Linklist));
//	PL p,q;
//	p = phead;
//	phead->info = 6;
//	phead->link = NULL;
//	for (int i = 0; i < 6; i++)
//	{
//		q = (PL)malloc(sizeof(struct Linklist));
//		q->info = a[i];
//		q->link = NULL;
//		p->link = q;
//		p = q;
//	}
//	return phead;
//}
//void PrintLinkList(PL pl)
//{
//	PL p = pl;
//	p = p->link;
//	while (p->link != NULL)
//	{
//		printf("%d ", p->info);
//		p = p->link;
//	}
//	printf("%d", p->info);
//}
//int main()
//{
//	int a[6] = { 0 };
//	printf("输入数组6个元素的值。\n");
//	for (int i = 0; i < 6; i++)
//	{
//		scanf("%d", &a[i]);
//	}
//	PL pl = CreateLinkList(a);
//	printf("此链表各个结点的数据域为：");
//	PrintLinkList(pl);
//
//	return 0;
//}

//已知立方和不等式为 1^3 + 2^3 + … + m^3 < n 对指定的n值，试求满足上述立方和不等式的m的整数解。
//
//	输入提示信息："Please enter n:"
//
//	输入格式："%ld"
//
//	输出格式："m<=%1d\n"
//int main()
//{
//	int n = 0;
//	int i = 0;
//	int ret = 0;
//	printf("Please enter n:");
//	scanf("%ld", &n);
//	while (ret < n)
//	{
//		i++;
//		ret = ret + (i * i * i);
//	}
//	printf("m<=%1d\n", i - 1);
//	return 0;
//}

//打印100~1000之间能同时被3、5、17整除的数。
//* *输出格式要求："%d\n"
//int main()
//{
//	int a = 100;
//	for (a = 100; a <= 1000; a++)
//	{
//		if (a % 3 == 0 && a % 5 == 0 && a % 17 == 0)
//		{
//			printf("%d\n", a);
//		}
//	}
//	return 0;
//}

//输入被除数和除数，求商。
//** 输入格式要求："%d%d"  提示信息："Enter two numbers:"
//* *输出格式要求："cannot divide by zero.\n"（被0除）  "%d"(输出a / b)
//程序运行示例如下：
//Enter two numbers : 8 3
//2
//程序运行示例如下：
//Enter two numbers : 6 3
//2
//程序运行示例如下：
//Enter two numbers : 9 0
//cannot divide by zero.
//int main()
//{
//	int p = 0;
//	int q = 0;
//	printf("Enter two numbers:");
//	scanf("%d%d", &p, &q);
//	if (q == 0)
//	{
//		printf("cannot divide by zero.\n");
//	}
//	else
//	{
//		printf("%d", p / q);
//	}
//	return 0;
//}

//一辆卡车违反了交通规则，撞人后逃逸。现场有三人目击该事件，但都没有记住车号，只记住车号的一些特征。
// 甲说：车号的前两位数字是相同的；
// 乙说：车号的后两位数字是相同的，
// 但与前两位不同；丙是位数学家，他说：4位的车号正好是一个整数的平方。请根据以上线索编程协助警方找出车号，以便尽快破案，抓住交通肇事犯。
//** 输出格式要求："k=%d, m=%d\n" (k为车号，k = m * m)

int main()
{
	int i, j, k, m;
	for (i = 0; i <= 9; i++)
	{
		for (j = 0; j <= 9; j++)
		{
			if (i != j)
			{
				k = i * 1000 + i * 100 + j * 10 + j;
				for (m = 3; m * m <= k; m++)
				{
					if (m * m == k)
					{
						printf("k=%d, m=%d\n", k,m);
					}
				}
			}
		}
	}
	return 0;
}