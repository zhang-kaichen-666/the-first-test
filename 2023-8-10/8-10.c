#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>

//模拟实现库函数strlen
//int len_str(char* arr)
//{
//	int count = 0;
//	while (*arr != '\0')
//	{
//		count++;
//		arr++;
//	}
//	return count;
//}
//int main()
//{
//	char str[] = "abcdef";
//	int len = len_str(str);
//	printf("%d\n", len);
//	return 0;
//}


//实现一个函数，可以左旋字符串中的k个字符。
//例如：
// abcd左旋一个字符得到bcda
//abcd左旋两个字符得到cdab
//char left_str(char *str,int n)
//{
//	int sz = strlen(str);
//	char i;
//	int z = 0;
//	while (n)
//	{
//		i = str[0];
//		for (z = 0; z < sz; z++)
//		{
//			if (z == sz - 1)
//			{
//				str[z] = i;
//			}
//			else if (z <= sz - 2)
//			{
//				str[z] = str[z + 1];
//			}
//
//		}
//		n--;
//	}
//}
//int main()
//{
//	char str1[] = "abcdef";
//	int n = 0;
//	printf("请输入左旋的次数：");
//	scanf("%d",&n);
//	left_str(str1,n);
//	printf("%s\n", str1);
//	return 0;
//}

//逆序
//void turn_against(char* str)
//{
//    int sz = strlen(str);
//    int i = 0;
//    int y = sz - 1;
//    int c = 5;
//    for (i = 0; i < sz && i < c; i++)
//    {
//        for (y = sz - 1; y >= 0; y--)
//        {
//            if (i + y == sz - 1)
//            {
//                char z = str[i];
//                str[i] = str[y];
//                str[y] = z;
//                c = y;
//            }
//        }
//
//    }
//}
//int main()
//{
//    char str[10000] = {0};
//    scanf("%[^\n]", &str);
//    turn_against(str);
//    printf("%s\n", str);
//    return 0;
//}