#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>

typedef int SLDataType;
typedef struct SListNode
{
	SLDataType val;
	struct SListNode* next; 
}SLNode;

void SLTPrint(SLNode* phead);
void SLTPushBack(SLNode** pphead, SLDataType x);
void SLTPushFront(SLNode** pphead, SLDataType x);
SLNode* CreatNode(SLDataType x);
void SLTPopBack(SLNode** pphead);
void SLTPopFront(SLNode** pphead);
SLNode* SLTFind(SLNode* phead,SLDataType x);
void SLTInsert(SLNode** pphead, SLNode* pos, SLDataType x);
void SLTInsertAfter(SLNode* pos, SLDataType x);
void SLTErase(SLNode** pphead, SLNode* pos);
void SLTDestroy(SLNode** pphead);
