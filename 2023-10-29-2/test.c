#include"SList.h"

void test()
{
	SLNode* plist = NULL;
	SLTPushBack(&plist, 1);
	SLTPushBack(&plist, 2);
	SLTPushBack(&plist, 3);
	SLTPushBack(&plist, 4);
	SLTPrint(plist);//1 2 3 4

	SLTPushFront(&plist, 5);
	SLTPushFront(&plist, 6);
	SLTPushFront(&plist, 7);
	SLTPrint(plist);//7 6 5 1 2 3 4

	SLTPopBack(&plist);//7 6 5 1 2 3
	SLTPrint(plist);

	SLTPopFront(&plist);
	SLTPrint(plist);//6 5 1 2 3

	SLNode* pos = SLTFind(plist, 1);
	SLTInsert(&plist, pos, 20);
	SLTPrint(plist);//6 5 20 1 2 3

	SLTInsertAfter(pos, 30);
	SLTPrint(plist);//6 5 20 1 30 2 3

	SLTErase(&plist, pos);
	SLTPrint(plist);//6 5 20 30 2 3

	SLTDestroy(&plist);
	SLTPrint(plist);//NULL
}

int main()
{
	test();
	return 0;
}