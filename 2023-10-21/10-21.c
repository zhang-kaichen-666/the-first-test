#define _CRT_SECURE_NO_WARNINGS 1
//输入10个数，将10个整数按升序排列输出，并且奇数在前，偶数在后。
// 如果输入的10个数是:10 9 8 7 6 5 4 3 2 1 ，则输出：1 3 5 7 8 2 4 6 8 10。
// （编程提示：可利用2个数组变量，一个用来存放输入的整数，输入后，对这个数组进行排序，
// 然后将数据复制到另一个数组中，先复制奇数再复制偶数）。
//输入提示：Input 10 numbers :
//	输入格式： % d
//	输出格式：Output： % d, % d, % d, % d, % d, % d, % d, % d, % d, % d
//#include<stdio.h>
//void Bubble_sort(int arr1[], int sz)
//{
//	//冒泡排序
//	for (int i = 0; i < sz - 1; i++)
//	{
//		for (int j = 0; j < sz - 1 - i; j++)
//		{
//			if (arr1[j] > arr1[j + 1])//前比后大，交换两个数
//			{
//				arr1[j] = arr1[j] ^ arr1[j + 1];
//				arr1[j + 1] = arr1[j] ^ arr1[j + 1];
//				arr1[j] = arr1[j] ^ arr1[j + 1];
//			}
//		}
//	}
//}
//int main()
//{
//	int arr1[10];
//	int arr2[10];
//	int z = 0;
//	printf("Input 10 numbers:");
//	for (int i = 0; i < 10; i++)
//	{
//		scanf("%d", &arr1[i]);
//	}
//	Bubble_sort(arr1, sizeof(arr1) / sizeof(arr1[0]));
//	for (int i = 0; i < 10; i++)
//	{
//		if (arr1[i] % 2 != 0)//奇数
//		{
//			arr2[z++] = arr1[i];
//		}
//	}
//	for (int i = 0; i < 10; i++)
//	{
//		if (arr1[i] % 2 == 0)//偶数
//		{
//			arr2[z++] = arr1[i];
//		}
//	}
//	printf("Output: ");
//	for (int i = 0; i < 9; i++)
//	{
//		printf("%d,", arr2[i]);
//	}
//	printf("%d",arr2[9]);
//	
//	return 0;
//}

//请将下面程序补全。
//#include<stdio.h>
//#include<stdlib.h>
//typedef struct stack
//{
//    int data;
//    struct stack* next;
//} STACK;
//STACK* head, * pr;
//int nodeNum = 0;                            /* 堆栈节点数寄存器 */
//STACK* CreateNode(int num);
//STACK* PushStack(int num);
//int PopStack(void);
//int main()
//{
//    int pushNum[5] = { 111, 222, 333, 444, 555 }, popNum[5], i;
//    for (i = 0; i < 5; i++)
//    {
//        PushStack(pushNum[i]);
//        printf("Push %dth Data:%d\n", i + 1, pushNum[i]);
//    }
//    for (i = 0; i < 5; i++)
//    {
//        popNum[i] = PopStack();
//        printf("Pop %dth Data:%d\n", 5 - i, popNum[i]);
//    }
//    return 0;
//}
// 函数功能：生成一个新的节点，并为该节点赋初值，返回指向新的节点的指针 
//STACK* CreateNode(int num)
//{
//    STACK* p = (STACK*)malloc(sizeof(STACK));
//    p->data = num;
//    p->next = NULL;
//    return p;
//}
// 函数功能：将整型变量num的值压入堆栈，返回指向链表新节点的指针 
//STACK* PushStack(int num)
//{
//    STACK* newhead = CreateNode(num);
//    newhead->next = head;
//    head = newhead;
//    return head;
//}
//  函数功能：将当前栈顶的数据弹出堆栈，返回从堆栈中弹出的数据 
//int PopStack(void)
//{
//    STACK* p = head;
//    int result = head->data;
//    head = head->next;
//    free(p);
//    p = NULL;
//    return result;
//}

//数组旋转：
//编写程序将如下的一个固定大小的整数数组a[2][3]向右旋转90度，构成新的数组b[3][2]，并打印出来。
//例如：a = 1 2 3
//          4 5 6
//旋转后
//b = 4 1
//    5 2
//	  6 3
//* *输入提示信息："Array a:\n"
//* *输入格式要求："%d"
//* *输出格式要求： "Array b:\n"
//输出时每个元素的大小为"%4d"
//程序运行示例如下：
//Array a :
//1 2 3
//4 5 6
//Array b :
//4   1
//5   2
//6   3
//#include<stdio.h>
//int main()
//{
//	int arrA[2][3];
//	int arrB[3][2];
//	printf("Array a:\n");
//	for (int i = 0; i < 2; i++)
//	{
//		for (int j = 0; j < 3; j++)
//		{
//			scanf("%d", &arrA[i][j]);
//		}
//	}
//	for (int j = 0; j < 3; j++)//转置
//	{
//		for (int i = 0; i < 2; i++)
//		{
//			arrB[j][i] = arrA[i][j];
//		}
//	}
//	for (int i = 0; i < 3; i++)//水平镜像
//	{
//		for (int j = 0; j < 1; j++)
//		{
//			arrB[i][j] = arrB[i][j] ^ arrB[i][1];
//			arrB[i][1] = arrB[i][j] ^ arrB[i][1];
//			arrB[i][j] = arrB[i][j] ^ arrB[i][1];
//		}
//	}
//	printf("Array b:\n");
//	for (int i = 0; i < 3; i++)
//	{
//		for (int j = 0; j < 2; j++)
//		{
//			printf("%4d", arrB[i][j]);
//		}
//		printf("\n");
//	}
//	return 0;
//}

//输入一个整形数，然后按汉语的习惯，将其读出来并输出。如1052，读作：一千零五十二。
//输入样例：
//1052
//输出样例：
//一千零五十二
//#include <stdio.h>
//#include <string.h>
//#include <stdlib.h>
//const char shuzi[10][4] = { "零", "一", "二", "三", "四", "五", "六", "七", "八", "九" };
//const char weishu[5][4] = { "十", "百", "千", "万", "亿" };
//
//void read(char* str);
//
//int main()
//{
//    char str[11] = { '0' };
//    scanf("%s", str + 1);
//    read(str + 1);
//    return 0;
//}
//
//void read(char* str)
//{
//    if (strlen(str) == 0)
//        return;
//    else
//    {
//        switch (str[0])
//        {
//        case '0':
//            if (str[1] != '0' && strlen(str) >= 2 && strlen(str) != 5)
//                printf("%s", shuzi[0]);
//            break;
//        case '1':
//            if (strlen(str) != 2 && strlen(str) != 6)
//                printf("%s", shuzi[1]);
//            else if (*(str - 1) != '0')
//                printf("%s", shuzi[1]);
//            break;
//        case '2':
//            printf("%s", shuzi[2]);
//            break;
//        case '3':
//            printf("%s", shuzi[3]);
//            break;
//        case '4':
//            printf("%s", shuzi[4]);
//            break;
//        case '5':
//            printf("%s", shuzi[5]);
//            break;
//        case '6':
//            printf("%s", shuzi[6]);
//            break;
//        case '7':
//            printf("%s", shuzi[7]);
//            break;
//        case '8':
//            printf("%s", shuzi[8]);
//            break;
//        case '9':
//            printf("%s", shuzi[9]);
//            break;
//        default:
//            return;
//        }
//        switch (strlen(str))
//        {
//        case 9:
//            printf("%s", weishu[4]);
//            break;
//        case 8:
//            if (str[0] != '0')
//                printf("%s", weishu[2]);
//            break;
//        case 7:
//            if (str[0] != '0')
//                printf("%s", weishu[1]);
//            break;
//        case 6:
//            if (str[0] != '0')
//                printf("%s", weishu[0]);
//            break;
//        case 5:
//            printf("%s", weishu[3]);
//            break;
//        case 4:
//            if (str[0] != '0')
//                printf("%s", weishu[2]);
//            break;
//        case 3:
//            if (str[0] != '0')
//                printf("%s", weishu[1]);
//            break;
//        case 2:
//            if (str[0] != '0')
//                printf("%s", weishu[0]);
//            break;
//        case 1:
//            break;
//        default:
//            return;
//        }
//        read(++str);
//    }
//}

//【附加题】从键盘输入某单位职工的月收入(人数最多不超过40人)，
//当输入负值时，表示输入结束，编程从键盘任意输入一个职工号，查找该职工的月收入。
//程序运行示例1：
//Input person's ID and income:001 564↙
//Input person's ID and income:002 365↙
//Input person's ID and income:003 564↙
//Input person's ID and income:004 654↙
//Input person's ID and income:005 -9↙
//Total number is 4
//Input the searching ID : 004↙
//income = 654
//程序运行示例2：
//Input person's ID and income:001 234↙
//Input person's ID and income:002 654↙
//Input person's ID and income:003 897↙
//Input person's ID and income:004 321↙
//Input person's ID and income:005-7↙
//Total number is 4
//Input the searching ID : 009↙
//Not found!
//下面给出的程序存在多处错误，请修正所有错误，使之能够得到正确的运行结果，并将正确程序填写在答题区。
//#include <stdio.h> 
//#define N 40 
//int ReadScore(int income[], long num[]);
//int BinSearch(long num[], long x, int n);
//int main()
//{
//    int income[N], n, pos;
//    long num[N], x;
//    n = ReadScore(income, num) - 1;
//    printf("Total number is %d\n", n);
//    printf("Input the searching ID:");
//    scanf("%d", &x);
//    pos = BinSearch(num, x, n);
//    if (pos != -1)
//    {
//        printf("income = %d\n", income[pos]);
//    }
//    else
//    {
//        printf("Not found!\n");
//    }
//    return 0;
//}
//
//int ReadScore(int income[], long num[])
//{
//    int i = 0;
//    do
//    {
//        i++;
//        printf("Input person's ID and income:");
//        scanf("%d%d", &num[i], &income[i]);
//    } while (num[i] > 0 && income[i] >= 0);
//    return i;
//}
//
//int BinSearch(long num[], long x, int n)
//{
//    int  low, high, mid;
//    low = 0;
//    high = n;
//    while (low < high)
//    {
//        mid = (high + low) / 2;
//        if (x > num[mid])
//        {
//            low = mid + 1;
//        }
//        else  if (x < num[mid])
//        {
//            high = mid - 1;
//        }
//        else
//        {
//            return (mid);
//        }
//    }
//    return(-1);
//}

//请安如下函数编程实现将一个字符串内所有的小写字母转换成大写字母，字符串由大、小写英文字母、数字、空格等构成。
//函数原型：
//void Change(char string[])
//输入提示信息："please input a string:"
//输出提示信息及格式："changed string is %s\n"
//< 友情提示 >
////小写字母和大写字母的ASCII码值之间的差是32
//#include<stdio.h>
//#include<string.h>
//#include<ctype.h>
//void Change(char string[])
//{
//	for (int i = 0; i < strlen(string); i++)
//	{
//		if (islower(string[i]))
//		{
//			string[i] -= 32;
//		}
//	}
//}
//int main()
//{
//	char str[100];
//	printf("please input a string:");
//	gets(str);
//	Change(str);
//	printf("changed string is %s\n", str);
//	return 0;
//}

//对输入的8个字符串(每个字符串长度不超过20)按照字典顺序进行排序并输出。不限定排序方法，不能使用goto语句。
//输入输出格式：
//输入：% s
//输出：% s\n
//
//输入：多个字符串用空格分隔
//输出：排序好的字符串，每行一个字符串，末尾有空行。
//
//输入输出样例：
//输入：
//diankuang liuxu sui fengwu qingbo taohua zhu shuiliu
//输出：
//diankuang
//fengwu
//liuxu
//qingbo
//shuiliu
//sui
//taohua
//zhu
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char str[8][20];
//	for (int i = 0; i < 8; i++) {
//		scanf("%s", str[i]);
//	}
//	for (int i = 0; i < 7; i++)
//	{
//		for (int j = 0; j < 7 - i; j++)
//		{
//			if (strcmp(str[j], str[j + 1]) > 0)
//			{
//				char a[100];
//				strcpy(a, str[j]);
//				strcpy(str[j], str[j + 1]);
//				strcpy(str[j + 1], a);
//			}
//		}
//	}
//	for (int i = 0; i < 8; i++)
//	{
//		printf("%s\n", str[i]);
//	}
//	return 0;
//}