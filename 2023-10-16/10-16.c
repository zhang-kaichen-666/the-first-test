#define _CRT_SECURE_NO_WARNINGS 1
//写一个程序包含如下的函数，完成：
//(a) 输入current_date的值：
//void input_date(struct date_rec* current_date)
//(b)将current_date增加1天：
//void increment_date(struct date_rec* current_date)
//(c)显示current_date的值：
//void output_date(struct date_rec* current_date)
//考虑每个月的实际天数，同时也考虑闰年的情况。
//
//** 输入格式要求："%d%d%d" 提示信息："请输入当前日期（年 月 日）："
//* *输出格式要求："当前日期：%d年%d月%d日！" （加1天后的新日期）
//#include<stdio.h>
//#include<stdlib.h>
//struct date_rec
//{
//    int day;
//    int month;
//    int year;
//};
//void input_date(struct date_rec* current_date)
//{
//    printf("请输入当前日期（年 月 日）：");
//    scanf("%d%d%d", &(current_date->year), &(current_date->month), &(current_date->day));
//}
//void increment_date(struct date_rec* current_date)
//{
//    int monthday1[12] = { 31,28,31,30,31,30,31,31,30,31,30,31 };
//    int monthday2[12] = { 31,29,31,30,31,30,31,31,30,31,30,31 };
//    if (current_date->year % 400 == 0 || (current_date->year % 4 == 0 && current_date->year % 100 != 0))//是闰年
//    {
//        if (current_date->day + 1 > monthday2[current_date->month - 1])
//        {
//            current_date->day = 1;
//            if (current_date->month == 12)
//            {
//                current_date->year++;
//            }
//            else
//            {
//                current_date->month++;
//            }
//        }
//        else
//        {
//            current_date->day++;
//        }
//    }
//    else//不是闰年
//    {
//
//        if (current_date->day + 1 > monthday1[current_date->month - 1])
//        {
//            current_date->day = 1;
//            if (current_date->month == 12)
//            {
//                current_date->year++;
//            }
//            else
//            {
//                current_date->month++;
//            }
//        }
//        else
//        {
//            current_date->day++;
//        }
//    }
//}
//void output_date(struct date_rec* current_date)
//{
//    printf("当前日期：%d年%d月%d日！", current_date->year, current_date->month, current_date->day);
//}
//int main()
//{
//    struct date_rec data = { 0,0,0 };
//    struct date_rec* current_date = &data;
//    input_date(current_date);
//    increment_date(current_date);
//    output_date(current_date);
//    return 0;
//}

//满足特异条件的数列。输入m和n（20≥m≥n≥0），求出满足以下方程式的正整数数列i1, i2, …, in，
//使得i1 + i2 + … + in = m，且i1≥i2≥…≥in。例如：
//当n = 4，m = 8时，将得到如下5个数列：
//5 1 1 1  4 2 1 1  3 3 1 1  3 2 2 1  2 2 2 2
//* *输入格式要求："%d" 提示信息："Please enter requried terms (<=10):"
//"                             their sum:"
//* *输出格式要求："There are following possible series:\n" "[%d]:" "%d"
//程序运行示例：
//Please enter requried terms(<= 10) : 4 8
//their sum : There are following possible series :
//[1] : 5111
//[2] : 4211
//[3] : 3311
//[4] : 3221
//[5] : 2222
//#include<stdio.h>
//#define NUM 10
//int i[NUM];
//int main()
//{
//    int sum, n, total, k, flag, count = 0;
//    printf("Please enter requried terms(<=10):");
//    scanf("%d", &n);
//    printf("                             their sum:");
//    scanf("%d", &total);
//    sum = 0;
//    k = n;
//    i[n] = 1;
//    printf("There are following possible series:\n");
//    while (1)
//    {
//        if (sum + i[k] < total)
//        {
//            if (k <= 1)
//            {
//                i[1] = total - sum;
//                flag = 1;
//            }
//            else
//            {
//                sum = sum + i[k];
//                k--;
//                i[k] = i[k + 1];
//                continue;
//            }
//        }
//        else if (sum + i[k] > total || k != 1)
//        {
//            sum -= i[++k];
//            flag = 0;
//        }
//        else
//            flag = 1;
//        if (flag)
//        {
//            printf("[%d]:", ++count);
//            for (flag = 1; flag <= n; ++flag)
//                printf("%d", i[flag]);
//            printf("\n");
//        }
//        if (++k > n)
//            break;
//        sum -= i[k];
//        i[k]++;
//    }
//    return 0;
//}
