#include"Stack.h"

void test()
{
	Stack st;
	StackInit(&st);
	StackPush(&st, '1');
	printf("size = %d ,data = %d\n", StackSize(&st), StackTop(&st));
	StackPop(&st);
	StackPush(&st, '2');
	StackPush(&st, '#');
	StackPush(&st, '&');
	while (!StackEmpty(&st))
	{
		printf("size = %d ,data = %d\n", StackSize(&st), StackTop(&st));
		StackPop(&st);
	}
	StackDestroy(&st);
}
int main()
{
	test();
	return 0;
}