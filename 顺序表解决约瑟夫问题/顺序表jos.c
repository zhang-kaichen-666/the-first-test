#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>

struct List
{
	int max;
	int n;
	int* element;
};
typedef struct List* PList;
PList create(int m)//创建空表
{
	PList p = (PList)malloc(sizeof(struct List));
	if (p != NULL)
	{
		p->element = (int*)malloc(sizeof(int) * m);
		if (p->element != NULL)
		{
			p->max = m;
			p->n = 0;
			return p;
		}
		else
			free(p);
	}
	printf("未成功申请空间.");
	return NULL;
}
int insert(PList p, int i, int x)//在第i下标的元素前插入元素x
{
	int q;
	if (i < 0 || i > p->max)
	{
		printf("在插入数据过程中输入的下标不合理.");
		return 0;
	}
	else
	{
		for (q = p->n - 1; q >= i; q--)
		{
			p->element[q + 1] = p->element[q];
		}
		p->element[i] = x;
		p->n = p->n + 1;
		return 1;
	}
}
int delete(PList p, int i)//删除下标为i的元素
{
	int q;
	if (i < 0 || i > p->max)
	{
		printf("在删除数据时输入的下标不合理.");
		return 0;
	}
	else
	{
		for (q = i; q < p->n - 1; q++)
		{
			p->element[q] = p->element[q + 1];
		}
		p->n = p->n - 1;
		return 1;
	}
}
void josephus(PList p, int s, int m)
{
	int s1, i, w;
	s1 = s - 1;
	for (i = p->n; i > 0; i--)
	{
		s1 = (s1 - 1 + m) % i;
		w = p->element[s1];
		delete(p, s1);
		printf("kill the %d\n", w);
	}
}
void print(PList p)
{
	int i;
	for (i = 0; i < p->n; i++)
	{
		printf("%d ", p->element[i]);
	}
	printf("\n");
}
int main()
{
	PList jos;
	int i, n, s, m;
	printf("请输入n的值：");//一共n个人
	scanf("%d", &n);
	printf("请输入s的值：");//从第s个人开始数
	scanf("%d", &s);
	printf("请输入m的值：");//数m个人
	scanf("%d", &m);
	jos = create(n);
	if (jos != NULL)
	{
		for (i = 0; i < n; i++)
		{
			insert(jos, i, i + 1);
		}
		printf("原始顺序表为：");
		print(jos);
		josephus(jos, s, m);
	}
	return 0;
}