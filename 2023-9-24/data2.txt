#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

//fputc适用所有输出流
//int main()
//{
//	//FILE* pf = fopen("test.txt", "w");
//	//if (pf == NULL)
//	//{
//	//	perror("fopen");
//	//}
//	////写文件
//	//fputc('q', pf);//写入到文件里
//
//	fputc('b', stdout);//打印到屏幕中
//	fputc('i', stdout);
//	fputc('t', stdout);
//
//	//fclose(pf);
//	//pf = NULL;
//	return 0;
//}

//fgetc使用于所有输入流
//int main()
//{
//	//int ch = fgetc(stdin);//stdin是标准输入流
//	//printf("%c\n", ch);
//	//int ch2 = getchar();//这两个函数类似
//	//printf("%c\n", ch2);
//
//	FILE* pf = fopen("test.txt", "r");//test.txt中放的是abc
//	if (pf == NULL)
//	{
//		perror("fopen");
//	}
//	int ch = fgetc(pf);
//	printf("%c\n", ch);//a
//	ch = fgetc(pf);
//	printf("%c\n", ch);//b
//	ch = fgetc(pf);
//	printf("%c\n", ch);//c
//	fclose(pf);	
//	printf("%d\n", ch);
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}

//文件的拷贝
int main()
{
	FILE* pfread = fopen("data1.txt", "r");
	if (pfread == NULL)
	{
		perror("fopen - 1");
		return 1;
	}
	FILE* pfwrite = fopen("data2.txt", "w");
	if (pfwrite == NULL)
	{
		perror("fopen - 2");
		fclose(pfread);
		pfread = NULL;
		return 1;
	}

	int ch = 0;
	while ((ch = fgetc(pfread)) != EOF)
	{
		fputc(ch, pfwrite);
	}
	fclose(pfread);
	fclose(pfwrite);
	pfread = NULL;
	pfwrite = NULL;
	return 0;
}