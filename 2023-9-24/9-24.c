﻿#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include <windows.h>

//fputc适用所有输出流
//int main()
//{
//	//FILE* pf = fopen("test.txt", "w");
//	//if (pf == NULL)
//	//{
//	//	perror("fopen");
//	//}
//	////写文件
//	//fputc('q', pf);//写入到文件里
//
//	fputc('b', stdout);//打印到屏幕中
//	fputc('i', stdout);
//	fputc('t', stdout);
//
//	//fclose(pf);
//	//pf = NULL;
//	return 0;
//}

//fgetc使用于所有输入流
//int main()
//{
//	//int ch = fgetc(stdin);//stdin是标准输入流
//	//printf("%c\n", ch);
//	//int ch2 = getchar();//这两个函数类似
//	//printf("%c\n", ch2);
//
//	FILE* pf = fopen("test.txt", "r");//test.txt中放的是abc
//	if (pf == NULL)
//	{
//		perror("fopen");
//	}
//	int ch = fgetc(pf);
//	printf("%c\n", ch);//a
//	ch = fgetc(pf);
//	printf("%c\n", ch);//b
//	ch = fgetc(pf);
//	printf("%c\n", ch);//c
//	fclose(pf);	
//	printf("%d\n", ch);
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}

//文件的拷贝
//int main()
//{
//	FILE* pfread = fopen("data1.txt", "r");
//	if (pfread == NULL)
//	{
//		perror("fopen - 1");
//		return 1;
//	}
//	FILE* pfwrite = fopen("data2.txt", "w");
//	if (pfwrite == NULL)
//	{
//		perror("fopen - 2");
//		fclose(pfread);
//		pfread = NULL;
//		return 1;
//	}
//
//	int ch = 0;
//	while ((ch = fgetc(pfread)) != EOF)
//	{
//		fputc(ch, pfwrite);
//	}
//	fclose(pfread);
//	pfread = NULL;
//	fclose(pfwrite);
//	pfwrite = NULL;
//	return 0;
//}

//fputs和fgets函数
//int main()
//{
//	FILE* pf = fopen("data.txt", "r");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//	fputs("helloworld\n", pf);
//	fputs("hehe\n", pf);
//
//	char arr[20] = { 0 };
//	fgets(arr, 5, pf);//读取到\n停止        参数为5实际读取4个字符
//	printf("%s\n", arr);
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}

//fscnaf 和 fprintf
//struct S
//{
//	int n;
//	float f;
//	char arr[20];
//};
//int main()
//{
//	struct S s = { 100,3.14f,"zhangsan" };
//	FILE* pf = fopen("data.txt", "w");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//	fprintf(pf, "%d %f %s", s.n, s.f, s.arr);
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}
//struct S
//{
//	int n;
//	float f;
//	char arr[20];
//};
//int main()
//{
//	struct S s = {0};
//	FILE* pf = fopen("data.txt", "r");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//	fscanf(pf, "%d %f %s", &(s.n),&(s.f), s.arr);
//	printf("%d %f %s", s.n, s.f, s.arr);
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}

//scanf-针对标准输入流（stdin）的格式化输入函数
//printf-针对标准输出流（stdout）的格式化输出函数
// 
//fscanf-针对所有输入流的格式化输入函数
//fprintf-针对所有输出流的格式化输入函数
//
//sscanf-从字符串中读取格式化的数据
//sprintf-将格式化的数据写到字符串中
//struct S
//{
//	int n;
//	float f;
//	char arr[20];
//};
//int main()
//{
//	struct S s = { 100,3.14f,"zhangsan" };
//	char arr[30] = { 0 };
//	sprintf(arr,"%d %f %s", s.n, s.f, s.arr);
//
//	//从arr这个字符串中提取出格式化的数据
//	struct S t = { 0 };
//	sscanf(arr, "%d %f %s", &(t.n), &(t.f), t.arr);
//	printf("%d %f %s\n", t.n, t.f, t.arr);
//
//	printf("%s\n", arr);
//}

//fread和fwrite  -  二进制的方式读写文件
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7 };
//	FILE* pf = fopen("data.txt", "wb");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//	fwrite(arr,sizeof(int), 7, pf);//二进制形式写入
//
//
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}
//int main()
//{
//	int arr[10] = {0};
//	FILE* pf = fopen("data.txt", "rb");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//	fread(arr,sizeof(int),7,pf);//二进制方式读取
//	for (int i = 0; i < 10; i++)
//	{
//		printf("%d\n", arr[i]);
//	}
//
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}

//文件的随机读写fseek ：根据⽂件指针的位置和偏移量来定位⽂件指针。
//ftell - 返回文件指针相对起始位置的偏移量
//rewind - 让文件指针的位置回到文件的起始位置
//int main()
//{
//	int arr[10] = { 0 };
//	FILE* pf = fopen("data.txt", "r");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//	//定位文件指针
//	fseek(pf, 6, SEEK_SET);//SEEK_SET -- 文件开始位置  SEEK_END -- 文件末尾位置    SEEK_CUR -- 文件当前位置
//	
//	int ch = fgetc(pf);
//	printf("%c\n", ch);
//
//	int pos = ftell(pf);
//	printf("%d\n", pos);
//
//	rewind(pf);
//	ch = fgetc(pf);
//	printf("%c\n", ch);
//
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}

//文件读取结束的判定
// feof用来检测读取结束的原因
//被错误使⽤的 feof
//牢记：在⽂件读取过程中，不能⽤feof函数的返回值直接来判断⽂件的是否结束。
//feof 的作⽤是：当⽂件读取结束的时候，判断是读取结束的原因是否是：遇到⽂件尾结束
//文件读取结束：原因可能有：1.文件遇到末尾。2.文件读取错误
//1. ⽂本⽂件读取是否结束，判断返回值是否为 EOF （ fgetc ），或者 NULL （ fgets ）
//例如：
//• fgetc 判断是否为 EOF .
//• fgets 判断返回值是否为 NULL .
//2. ⼆进制⽂件的读取结束判断，判断返回值是否⼩于实际要读的个数。
//例如：
//• fread判断返回值是否⼩于实际要读的个数。(fread返回实际读到的元素个数)
//int main(void)
//{
//	int c; // 注意：int，⾮char，要求处理EOF
//	FILE* fp = fopen("test.txt", "r");
//	if (!fp) {
//		perror("File opening failed");
//		return -1;
//	}
//	//fgetc 当读取失败的时候或者遇到⽂件结束的时候，都会返回EOF
//	while ((c = fgetc(fp)) != EOF) // 标准C I/O读取⽂件循环
//	{
//		putchar(c);
//	}
//	//判断是什么原因结束的
//	if (ferror(fp))
//		puts("I/O error when reading");
//	else if (feof(fp))
//		puts("End of file reached successfully");
//	fclose(fp);
//}
//enum { SIZE = 5 };
//int main(void)
//{
//	double a[SIZE] = { 1.,2.,3.,4.,5. };
//	FILE* fp = fopen("test.bin", "wb"); // 必须⽤⼆进制模式
//	fwrite(a, sizeof * a, SIZE, fp); // 写 double 的数组
//	fclose(fp);
//	double b[SIZE];
//	fp = fopen("test.bin", "rb");
//	size_t ret_code = fread(b, sizeof * b, SIZE, fp); // 读 double 的数组
//	if (ret_code == SIZE) {//ret_code是实际读取元素的个数
//		puts("Array read successfully, contents: ");
//		for (int n = 0; n < SIZE; ++n) printf("%f ", b[n]);
//		putchar('\n');
//	}
//	else { // error handling
//		if (feof(fp))
//			printf("Error reading test.bin: unexpected end of file\n");
//		else if (ferror(fp)) {
//			perror("Error reading test.bin");
//		}
//	}
//	fclose(fp);
//}

//文件缓冲区测试
//int main()
//{
//	FILE* pf = fopen("test.txt", "w");
//	fputs("abcdef", pf);//先将代码放在输出缓冲区
//	printf("睡眠10秒-已经写数据了，打开test.txt⽂件，发现⽂件没有内容\n");
//	Sleep(10000);
//	printf("刷新缓冲区\n");
//	fflush(pf);//刷新缓冲区时，才将输出缓冲区的数据写到⽂件（磁盘）
//	//注：fflush 在⾼版本的VS上不能使⽤了
//	printf("再睡眠10秒-此时，再次打开test.txt⽂件，⽂件有内容了\n");
//	Sleep(10000);
//	fclose(pf);
//	//注：fclose在关闭⽂件的时候，也会刷新缓冲区
//	pf = NULL;
//	return 0;
//}