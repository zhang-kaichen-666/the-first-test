#include"SList.h"


void sltest()
{
	SLTNode* plist = NULL;
	SLTPushBack(&plist, 1);
	SLTPushBack(&plist, 2);
	SLTPushBack(&plist, 3);
	SLTPushBack(&plist, 4);
	SLTPrint(plist);//1-2-3-4

	SLTPushFront(&plist, 3);
	SLTPushFront(&plist, 2);
	SLTPrint(plist);//2-3-1-2-3-4
	
	SLTPopBack(&plist);
	SLTPopBack(&plist);
	SLTPrint(plist);//2-3-1-2
	
	SLTPopFront(&plist);
	SLTPrint(plist);//3-1-2

	SLTNode* find = SLTFind(plist, 3);
	SLTInsert(&plist, find, 5);
	SLTPrint(plist);//5-3-1-2

	find = SLTFind(plist, 1);
	SLTErase(&plist, find);
	SLTPrint(plist);//5-3-2

	find = SLTFind(plist, 3);
	SLTInsertAfter(find, 6);
	SLTPrint(plist);//5-3-6-2

	find = SLTFind(plist, 3);
	SLTEraseAfter(find);
	SLTPrint(plist);//5-3-2

	SListDesTroy(&plist);
	SLTPrint(plist);
}
int main()
{
	sltest();
	return 0;
}