#define _CRT_SECURE_NO_WARNINGS 1
//输入6个字符串，输出最小串及最大串。
//** 输入格式要求："%s" 提示信息："请输入6行字符串：\n"
//* *输出格式要求："The max string is: %s\n" "The min string is: %s\n"
//程序示例运行如下：
//请输入6行字符串：
//hello, world
//vb
//vc
//Java
//c++
//c#
//The max string is : vc
//The min string is : Java
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char str[6][10];
//	int max = 0;;
//	int min = 0;
//	printf("请输入6行字符串：\n");
//	for (int i = 0; i < 6; i++)
//	{
//		scanf("%s", str[i]);
//		if (strcmp(str[i],str[min]) < 0)
//		{
//			min = i;
//		}
//		if(strcmp(str[i], str[max]) > 0)
//		{
//			max = i;
//		}
//	}
//	printf("The max string is: %s\n", str[max]);
//	printf("The min string is: %s\n", str[min]);
//}

//橙子0.8元一个，第一天买2个橙子，第二天开始，每天买前一天的2倍，直至当天购买的橙子个数达到不超过100的最大值。
//编写程序求每天平均花多少钱？
//* *要求输入提示信息为：无输入提示信息
//* *要求输入格式为：无
//* *要求输出格式为："%.2f\n"
//注：不允许使用goto语句
//#include<stdio.h>
//int main()
//{
//	int num = 2;
//	int day = 1;
//	int sum = 2;
//	double average = 0;
//	while (num < 100)
//	{
//		num *= 2;
//		sum += num;
//		day++;
//	}
//	sum -= num;
//	day--;
//	average = sum * 0.8 / day;
//	printf("%.2f\n", average);
//	return 0;
//}

//从键盘输入10个整数，编程统计其中奇数(大于0)的个数。
//要求输入：% d
//输出：the odd number is% d
//(注意输出语句末尾不需要输出换行符\n)
//#include<stdio.h>
//int main()
//{
//	int count = 0;
//	int arr[10];
//	for (int i = 0; i < 10; i++)
//	{
//		scanf("%d", &arr[i]);
//		if (arr[i] % 2 == 1)
//		{
//			count++;
//		}
//	}
//	printf("the odd number is% d", count);
//	return 0;
//}

//输出 2~n之间所有素数，并求和，n由键盘输入。
// 素数是只能被1和自身整除的整数。
// 要求编写函数判断自然数x是否为素数，函数原型为：
//int IsPrime(unsigned int x); //x是素数返回1，不是素数返回0。
//输入提示信息："Please input n(n>1):"
//输入格式："%d"
//每个素数的输出格式："%d\n"
//求和输出格式："sum of prime numbers:%d"
//#include<stdio.h>
//#include<math.h>
//int IsPrime(unsigned int x)
//{
//	for (int i = 2; i <= sqrt(x); i++)
//	{
//		if (x % i == 0)
//		{
//			return 0;
//		}
//	}
//	return 1;
//
//}
//int main()
//{
//	int n = 0;
//	int sum = 0;
//	printf("Please input n(n>1):");
//	scanf("%d", &n);
//	for (int i = 2; i <= n; i++)
//	{
//		if (IsPrime(i))
//		{
//			printf("%d\n", i);
//			sum += i;
//		}
//	}
//	printf("sum of prime numbers:%d", sum);
//	return 0;
//}

//使用单分支的条件语句编程，计算并输出两个整数的最大值。
//** 输入格式要求："%d,%d" 提示信息："Input a, b:"
//* *输出格式要求："max = %d\n"
//程序运行示例如下：
//Input a, b:3, 5
//max = 5
//#include<stdio.h>
//int main()
//{
//	int a = 0;
//	int b = 0;
//	int max = 0;
//	printf("Input a, b:");
//	scanf("%d,%d", &a, &b);
//	if (a > b)
//	{
//		max = a;
//	}
//	else
//	{
//		max = b;
//	}
//	printf("max = %d\n", max);
//	return 0;
//}