#define _CRT_SECURE_NO_WARNINGS 1
//按如下函数原型编程实现字符串连接函数strcat()的功能。
//void  MyStrcat(char* dstStr, char* srcStr);
//输入提示信息：
//"Input a string:"
//"Input another string:"
//输入字符串用gets()
//输出提示信息和格式："Concatenate results:%s\n"
//#include<stdio.h>
//#include<string.h>
//void  MyStrcat(char* dstStr, char* srcStr)
//{
//	int i = strlen(dstStr);
//	for (int j = 0; j < strlen(srcStr); j++)
//	{
//		dstStr[i++] = srcStr[j];
//	}
//	dstStr[i] = '\0';
//}
//int main()
//{
//	char dstStr[100], srcStr[100];
//	printf("Input a string:");
//	gets(dstStr);
//	printf("Input another string:");
//	gets(srcStr);
//	MyStrcat(dstStr, srcStr);
//	printf("Concatenate results:%s\n", dstStr);
//
//	return 0;
//}

//马克思手稿中的数学问题。马克思手稿中有一道趣味数学题：
// 有30个人，其中有男人、女人和小孩，在一家饭馆吃饭共花了50先令：
// 每个男人花3先令，每个女人花2先令，每个小孩花1先令，
// 问男人、女人和小孩各几人？
//** 输入格式要求：无
//** 输出格式要求："\tMEN\tWOMEN\tCHILDREN\n"
//"-----------------------------------------\n"
//"%2d:\t%d\t%d\t%d\n"
//程序运行示例如下：
//MEN	WOMEN	CHILDREN
//---------------------------------------- -
//1:	0	20	10
//2 : 1	18	11
//3 : 2	16	12
//4 : 3	14	13
//5 : 4	12	14
//6 : 5	10	15
//7 : 6	8	16
//8 : 7	6	17
//9 : 8	4	18
//10 : 9	2	19
//11 : 10	0	20
//#include<stdio.h>
//int main()
//{
//	int x = 0, y = 0, z = 0,count = 1;
//	printf("\tMEN\tWOMEN\tCHILDREN\n");
//	printf("-----------------------------------------\n");
//	while (x <= 17)
//	{
//		y = 0;
//		while (y <= 25)
//		{
//			z = 0;
//			while (z < 50)
//			{
//				if (x + y + z == 30 && 3 * x + 2 * y + z == 50)
//				{
//					printf("%2d:\t%d\t%d\t%d\n", count++, x, y, z);
//				}
//				z++;
//			}
//			y++;
//		}
//		x++;
//	}
//	return 0;
//}

//从键盘任意输入一个字符串，计算其实际字符个数并打印输出，
// 要求不能使用字符串处理函数strlen()，使用自定义子函数Mystrlen()实现计算字符个数的功能。
//函数原型：int  MyStrlen(char str[])
//函数功能：计算存放在字符数组str[]中的字符串的实际字符个数(即不包括'/0'）
//
//    输入提示信息："Please enter a string:"
//    输入格式要求：用gets()函数
//    输出格式要求："The length of the string is: %d\n"
//    程序的运行示例1：
//    Please enter a string : Hello China↙
//    The length of the string is : 11
//#include<stdio.h>
//int  MyStrlen(char str[])
//{
//	int count = 0;
//	while (str[count] != '\0')
//	{
//		count++;
//	}
//	return count;
//}
//int main()
//{
//	char str[100];
//	printf("Please enter a string:");
//	gets(str);
//	printf("The length of the string is: %d\n", MyStrlen(str));
//	return 0;
//}

//写一个函数测试某个整数值是否落在某个范围之内。函数原型如下：
//int range_test(int val, int low, int high);
//其中val是要测试的值，low是范围的最小值，high是范围的最大值。
// 如果落在指定的范围内，函数返回1，否则返回0。编写main函数调用它并进行测试。
//
//** 输入格式要求："%d%d%d" 提示信息："请输入数值、下界和上界：\n"
//* *输出格式要求："函数测试输出为%d！\n" （输出为：1或者0）
//#include<stdio.h>
//int range_test(int val, int low, int high)
//{
//	if (val < low || val > high)
//	{
//		return 0;
//	}
//	return 1;
//}
//int main()
//{
//	int val = 0, low = 0, high = 0;
//	printf("请输入数值、下界和上界：\n");
//	scanf("%d%d%d", &val, &low, &high);
//	printf("函数测试输出为%d！\n", range_test(val, low, high));
//	return 0;
//}

//输入一个正整数n，再输入n个学生的成绩，计算平均分，并统计不及格成绩的个数。
//** 输入格式要求："%d" 提示信息："Enter grade #%d: "  "%lf" 提示信息："Enter n: "
//* *输出格式要求："Grade average = %.2f\n"  "Number of failures = %d\n"
//程序运行示例如下：
//Enter n : 4
//Enter grade #1: 67
//Enter grade #2: 54
//Enter grade #3: 88
//Enter grade #4: 73
//Grade average = 70.50
//Number of failures = 1
//#include<stdio.h>
//int main()
//{
//	int n,count = 0;
//	double grade[100], sum = 0, average = 0;
//	printf("Enter n: ");
//	scanf("%d", &n);
//	for (int i = 0; i < n; i++)
//	{
//		printf("Enter grade #%d: ", i + 1);
//		scanf("%lf", &grade[i]);
//		sum += grade[i];
//	}
//	average = sum / n;
//	for (int i = 0; i < n; i++)
//	{
//		if (grade[i] < 60)
//		{
//			count++;
//		}
//	}
//	printf("Grade average = %.2f\n", average);
//	printf("Number of failures = %d\n", count);
//	return 0;
//}

//有一分数序列：2 / 1，3 / 2，5 / 3，8 / 5，13 / 8，21 / 13，……求出这个数列的前20项之和。
//* *输出格式要求：总和 = % 9.6f\n"
//#include<stdio.h>
//int main()
//{
//	double sum = 0;
//	int x = 2,y = 1;
//	for (int i = 0; i < 20; i++)
//	{
//		sum += (double)x / (double)y;
//		int tmpy = x;
//		x = x + y;
//		y = tmpy;
//	}
//	printf("总和=%9.6f\n", sum);
//	return 0;
//}

//输入一个整数，判断该数是否能被7整除.
//输入格式："%d"
//如能被7整除，则输出："此数能被7整除"
//否则输出："此数不能被7整除"
//#include<stdio.h>
//int main()
//{
//	int m;
//	scanf("%d", &m);
//	if (m % 7 == 0)
//	{
//		printf("此数能被7整除");
//	}
//	else
//	{
//		printf("此数不能被7整除");
//	}
//	return 0;
//}

//求以下分数序列通项式，并求出前 n项之和。要求：求和的结果通过函数返回数值。
//如： n = 10， 输出： 16.479905
//2 / 1, 3 / 2, 5 / 3, 8 / 5, 13 / 8, 21 / 13
//* *输入格式要求："%d" 提示信息："请输入n的值：\n"
//* *输出格式要求："n项之和为：%lf\n"
//程序运行示例如下：
//请输入n的值：
//20
//n项之和为：32.660261
//#include<stdio.h>
//int main()
//{
//	double sum = 0;
//	int x = 2, y = 1,n;
//	printf("请输入n的值：\n");
//	scanf("%d", &n);
//	for (int i = 0; i < n; i++)
//	{
//		sum += (double)x / (double)y;
//		int tmpy = x;
//		x = x + y;
//		y = tmpy;
//	}
//	printf("n项之和为：%lf\n", sum);
//	return 0;
//}
