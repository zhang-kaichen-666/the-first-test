#define _CRT_SECURE_NO_WARNINGS 1
//猴子吃桃程序_扩展2
//猴子第一天摘了若干个桃子，吃了一半，不过瘾，又多吃了1个。
// 第二天早上将剩余的桃子又吃掉一半，并且又多吃了1个。
// 此后每天都是吃掉前一天剩下的一半零一个。
// 到第n天再想吃时，发现只剩下1个桃子，
// 问第一天它摘了多少桃子？
// 为了加强交互性，由用户输入不同的天数n进行递推，即假设第n天的桃子数为1。
// 同时还要增加对用户输入数据的合法性验证（如 : 不允许输入的天数是0和负数）
//
//程序运行结果示例：
//Input days :
//0↙
//Input days :
//-5↙
//Input days :
//a↙
//Input days :
//3↙
//x = 10
//
//输入格式 : "%d"
//输出格式：
//输入提示信息："Input days:\n"
//输出："x=%d\n"
//#include<stdio.h>
//int calculate(int n)//第n天只剩一个，寻找第一天有多少个
//{
//	if (n == 1)
//	{
//		return 1;
//	}
//	return (calculate(n - 1) + 1) * 2;
//}
//int main()
//{
//	int n = 0;
//	while (n <= 0)
//	{
//		printf("Input days:\n");
//		int flag = scanf("%d", &n);
//		getchar();
//		if (flag == 0)
//		{
//			continue;
//		}
//	}
//	int x = calculate(n);
//	printf("x=%d\n", x);
//	return 0;
//}

//从键盘输入n个整数，用函数编程实现最大值和最小值的交换，并打印结果。
//** 输入格式要求："%d"
//提示信息："Input n(n<=10):"  "Input %d numbers:"
//* *输出结果格式要求："%5d"
//提示信息："Exchange results:"
//请按如下框架编程，在“......”的位置添加缺失的语句，使程序完整。
//#include  <stdio.h>
//#define N 10
//void ReadData(int a[], int n);
//void PrintData(int a[], int n);
//void Swap(int* x, int* y);
//int main()
//{
//    int  a[N], i, n;
//    int minIndex; // 最小值的下标
//    int maxIndex; // 最大值的下标
//    printf("Input n(n<=10):");
//    scanf("%d", &n);
//    printf("Input %d numbers:", n);
//    ReadData(a, n);
//
//        //交换数组a中的最小值和最大值
//    minIndex = maxIndex = 0;
//    for (int i = 0; i < n; i++)
//    {
//        if (a[minIndex] > a[i])
//        {
//            minIndex = i;
//        }
//        if (a[maxIndex] < a[i])
//        {
//            maxIndex = i;
//        }
//    }
//    Swap(&a[maxIndex], &a[minIndex]);
//
//    printf("Exchange results:");
//    PrintData(a, n);
//
//    return 0;
//}
//void ReadData(int a[], int n)
//{
//    int i;
//    for (i = 0; i < n; i++)
//    {
//        scanf("%d", &a[i]);
//    }
//}
//void PrintData(int a[], int n)
//{
//    int i;
//    for (i = 0; i < n; i++)
//    {
//        printf("%5d", a[i]);
//    }
//}
/////* 函数功能：两整数值互换 */
//void  Swap(int* x, int* y)
//{
//    int  temp;
//    temp = *x;
//    *x = *y;
//    *y = temp;
//
//}

//编写一个程序，将用户输入的由数字字符和非数字字符组成的字符串中的数字提取出来
// （例如：输入asd123, 34fgh_566kkk789，则产生的数字分别是123、34、789）。
//* *输入格式要求：提示信息："Please enter a string:"
//* *输出格式要求："the result of output:\n" "%10d\n"
//程序运行示例如下：
//Please enter a string :
//abc123def456ghi111bbbccc99go100
//the result of output :
//123
//456
//111
//99
//100
//#include <stdio.h>
//#include <string.h>
//int main()
//{
//    printf("Please enter a string:");
//    char arr[100];
//    char* a = arr;
//    scanf("%s", arr);
//    printf("the result of output:\n");
//    int b = 0, c = 0, d = 0;
//    for (int i = 0; i < strlen(arr); i++)
//    {
//
//        if (a[i] >= '0' && a[i] <= '9')
//        {
//            b++;
//            if (b == 1)
//            {
//                d += a[i] - '0';
//            }
//            else
//            {
//                d = d * 10 + a[i] - '0';
//            }
//            c = 0;
//        }
//        if (a[i] < '0' || a[i] > '9')
//        {
//            c++;
//            if (c == 1 && i > 0)
//            {
//                b = 0;
//            }
//        }
//        if ((a[i + 1] < '0' || a[i + 1] > '9') && b != 0)
//        {
//            printf("%10d\n", d);
//            d = 0;
//        }
//    }
//}
