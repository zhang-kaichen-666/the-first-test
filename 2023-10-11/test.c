//#include<stdio.h>

//写一个程序读入一条句子中的所有单词，并计算该语句中所有单词的平均长度。假定句子以换行结束，单词间用空格分隔，并且每个单词长度不超过25个字符。
//
//** 提示信息："请输入一行字符："
//* *输出格式要求："总共有%d个单词，平均长度为%d\n"
//int main()
//{
//	char s[1000];
//	char* p = s;
//	int count = 1;
//	int  len = 0;
//	printf("请输入一行字符：");
//	gets(s);
//	for (;; p++)
//	{
//		if (*p == '\0')
//		{
//			break;
//		}
//		if (*p == ' ')
//		{
//			count++;
//		}
//		else
//		{
//			len++;
//		}
//	}
//	len = len / count;
//
//	printf("总共有%d个单词，平均长度为%d\n", count, len);
//	return 0;
//}

//用指针编程实现3X4的二维数组的元素读入以及求此二维数组的最大值及最大值下标
//请用以下函数实现：
//void Input(int* p, int m, int n);  /*数组元素读入函数*/
//int FindMax(int* p, int m, int n, int* pRow, int* pCol);  /*求最大值及下标函数*/
//
//若存在若干个相同最大元素，则按照第一次出现的最大值的下标输出。
//
//*** 输入提示信息："Please input your data:\n"
//* **输入格式要求：无格式要求
//* **输出格式要求："The maximum is %d, which is in row %d, colum %d\n"
//
//
//样例：
//
//Please input your data :
//**输入样例：
//3 5 2 7 1 6 12 11 4 10 8 9
//* *输出样例：
//The maximum is 12, which is in row 1, colum 2
//void Input(int* p, int m, int n)  /*数组元素读入函数*/
//{
//	for (int i = 0; i < m; i++)
//	{
//		for (int j = 0; j < n; j++)
//		{
//			scanf("%d", p);
//			p++;
//		}
//	}
//
//}
//int FindMax(int* p, int m, int n, int* pRow, int* pCol)  /*求最大值及下标函数*/
//{
//	int max = 0;
//	for (int i = 0; i < m; i++)
//	{
//		for (int j = 0; j < n; j++)
//		{
//			if (max < *p)
//			{
//				max = *p;
//				*pRow = i;
//				*pCol = j;
//			}
//			p++;
//		}
//	}
//	return max;
//}
//int main()
//{
//	int arr[3][4] = { 0 };
//	int row = 0;
//	int col = 0;
//	printf("Please input your data :\n");
//	Input(arr[0], 3, 4);
//
//	int max = FindMax(arr[0], 3, 4, &row, &col);
//	printf("The maximum is %d, which is in row %d, colum %d", max, row, col);
//	return 0;
//}

//请用指针数组编程实现按奥运会参赛国的国名在字典中的顺序对其入场次序进行排序。
//假设参赛国不超过150个，参赛国的国名不超过9个字符。
//下面程序中存在比较隐蔽的错误，请通过分析和调试程序，发现并改正程序中的错误。
//注意：
//（1）请将修改正确后的完整源程序拷贝粘贴到答题区内。
//（2）对于没有错误的语句，请不要修改，修改原本正确的语句也要扣分。
//（3）当且仅当错误全部改正，且程序运行结果调试正确，才给加5分。
//（4）改错时不能改变程序原有的意图，也不要改变代码的输入输出格式。
//（5）经教师手工核对后，如果未使用指针数组做函数参数编程，那么即使运行结果正确也不给分。
//#include  <stdio.h>
//#include<string.h>
//#define   max_len  10
//#define   n         150
//void sortstring(char* ptr[], int n);
//main()
//{
//    int    i, n;
//    char* pstr[n];
//    char str[n][max_len];
//    printf("how many countries?\n");
//    scanf("%d", &n);
//    getchar();
//    printf("input their names:\n");
//    for (i = 0; i < n; i++)
//    {
//        gets(str[i]);
//    }
//    for (i = 0; i < n; i++)
//    {
//        pstr[i] = str[i];
//    }
//
//    sortstring(pstr, n);
//    printf("sorted results:\n");
//    for (i = 0; i < n; i++)
//    {
//        puts(pstr[i]);
//    }
//}
//
//void sortstring(char* ptr[], int n)
//{
//    int    i, j;
//    char* temp = null;
//    for (i = 0; i < n - 1; i++)
//    {
//        for (j = i + 1; j < n; j++)
//        {
//            if (strcmp(ptr[j], ptr[i]) < 0)
//            {
//                temp = ptr[j];
//                ptr[j] = ptr[i];
//                ptr[i] = temp;
//            }
//        }
//    }
//}

//有一电文，已按下面规律译成密码。
//A->Z	a->z
//B->Y	b->y
//C->X	c->x
//…		…
//即第一个字母变成第26个字母，第i个字母变成第（26 - i + 1）个字母，非字母字符不变。要求编程将密码译回原文，并打印出密码和原文。
//程序的运行示例如下：
//请输入字符：hello, world!
//密码是：hello, world!
//原文是：svool, dliow!
//#include<stdio.h>
//#include<ctype.h>
//#include<string.h>
//int main()
//{
//	char str[100];
//	char str2[100];
//	int i = 0;
//	printf("请输入字符：");
//	gets(str);
//	printf("密码是：%s\n", str);
//
//	for (i = 0; i < strlen(str);i++)
//	{
//		if (isalpha(str[i]) != 0)
//		{
//			if (str[i] >= 97 && str[i] <= 122)//小写字母
//			{
//				str2[i] = 219 - str[i];
//			}
//			else//大写字母
//			{
//				str2[i] = 155 - str[i];
//			}
//		}
//		else
//		{
//			str2[i] = str[i];
//		}
//	}
//	str2[i] = '\0';
//
//	printf("原文是：%s\n", str2);
//	return 0;
//}

//从键盘任意输入一个字符，编程判断该字符是数字字符、英文字母、空格还是其他字符。
//** 输入格式要求：提示信息："Press a key and then press Enter:"
//* *输出格式要求："It is an English character!\n" "It is a digit character!\n"  "It is a space character!\n"  "It is other character!\n"
//程序运行示例1如下：
//Press a key and then press Enter : A
//It is an English character!
//程序运行示例2如下：
//Press a key and then press Enter : 2
//It is a digit character!
//程序运行示例3如下：
//Press a key and then press Enter :
//It is a space character!
//程序运行示例4如下：
//Press a key and then press Enter : #
//It is other character!
//#include<stdio.h>
//#include<ctype.h>
//int main()
//{
//	printf("Press a key and then press Enter:");
//	char c;
//	scanf("%c", &c);
//	if (isdigit(c))
//	{
//		printf("It is a digit character!\n");
//	}
//	else if (isalpha(c))
//	{
//		printf("It is an English character!\n");
//	}
//	else if (c == ' ')
//	{
//		printf("It is a space character!\n");
//	}
//	else
//	{
//		printf("It is other character!\n");
//	}
//	return 0;
//}

//求100～200间的全部素数（即质数），要求每行输出10个素数。
//下面程序中存在比较隐蔽的错误，
//请通过分析和调试程序，发现并改正程序中的错误。
//注意：请将修改正确后的完整源程序拷贝粘贴到答题区内。
//对于没有错误的语句，请不要修改，修改原本正确的语句也要扣分。
//当且仅当错误全部改正，且程序运行结果调试正确，才得满分。
//#include <stdio.h>
//#include<math.h>
//main()
//{
//    int n = 0;
//    int m, k, i;
//    for (m = 101; m <= 200; m += 2)
//    {
//        if (n % 10 == 0)
//        {
//            printf("\n");
//        }
//        k = sqrt(m);
//        for (i = 2; i <= k; i++)
//        {
//            if (m % i == 0)
//            {
//                break;
//            }
//        }
//        if (i == k + 1)
//        {
//            printf("%d ", m);
//            n++;
//        }
//    }
//}

//有100g药品，用天平称量，砝码只有1g, 2g, 5g三种，若要求加的砝码总数为30个，有几种不同的加法？用穷举法编程求解。
//* *输入提示信息和格式：无
//* *输出格式："%2d,%2d,%2d\n"
//注：（1）不能使用指针、结构体、共用体、文件、goto、枚举类型进行编程。
//（2）用纯C语言编程，所有变量必须在第一条可执行语句前定义。
//#include<stdio.h>
//int main()
//{
//	int n = 30;
//	int m = 30;
//	int l = 20;
//	while (n >= 0)
//	{
//		while (m >= 0)
//		{
//			while (l >= 0)
//			{
//				if ((n + 2 * m + 5 * l == 100) && (n + m + l == 30))
//				{
//					printf("%2d,%2d,%2d\n", n, m, l);
//				}
//				l--;
//			}
//			l = 20;
//			m--;
//		}
//		m = 50;
//		n--;
//	}
//
//	return 0;
//}

//梅森尼数
//形如2^ i - 1的素数，称为梅森尼数。编程计算并输出指数i在[2, n]中的所有梅森尼数，并统计这些梅森尼数的个数，其中n的值由键盘输入，
// 并且n的值不能大于50。其中，2 ^ i表示2的i次方，请不要使用pow(2, i)编程计算，应采用循环累乘求积的方式计算2 ^ i。
//提示：当i超过30以后，2 ^ i - 1的值会很大，不能用long型变量来存储，必须使用double类型来存储。对于double类型变量x（不是整型）不能执行求余运算，
// 即不能用x % i == 0来判断x是否能被i整除，可以使用x / i == (long long)(x / i)来判断x是否能被i整除。
//
//程序运行示例
//Input n :
//50↙
//2 ^ 2 - 1 = 3
//2 ^ 3 - 1 = 7
//2 ^ 5 - 1 = 31
//2 ^ 7 - 1 = 127
//2 ^ 13 - 1 = 8191
//2 ^ 17 - 1 = 131071
//2 ^ 19 - 1 = 524287
//2 ^ 31 - 1 = 2147483647
//count = 8
//
//输入提示信息："Input n:\n"
//输入格式: "%d"
//输出格式： "2^%d-1=%.0lf\n"
//"count=%d\n"
//#include<stdio.h>
//#include<math.h>
//int mypow(int x, int n)//返回x的n次方-1
//{
//	int ret = 1;
//	for (int i = 0; i < n; i++)
//	{
//		ret *= x;
//	}
//	return ret - 1;
//}
//int isprime(double x)//判断是否是素数
//{
//	int k = (int)sqrt(x);
//	int flag = 1;
//	if (x <= 1)
//	{
//		flag = 0;
//	}
//	for (int i = 2; i <= k; i++)
//	{
//		if (x / i == (long long)(x / i))
//		{
//			flag = 0;
//			break;
//		}
//
//	}
//	return flag;
//}
//
//int main()
//{
//	int i = 0;
//	int n = 0;
//	int count = 0;
//	double x = 1;
//	printf("Input n:\n");
//	scanf("%d", &n);
//	for (i = 2; i < n; i++)
//	{
//		x = mypow(2, i);
//		if (isprime(x) == 1)
//		{
//			printf("2^%d-1=%.0lf\n", i, x);
//			count++;
//		}
//	}
//	printf("count=%d\n", count);
//	return 0;
//
//}

//在数组元素中找最大值及其所在下标位置。
//#include <stdio.h>
//int FindMax(int num[], int n, int* pMaxPos);
//main()
//{
//	int num[10], maxValue, maxPos, i;
//	printf("Input %d numbers:\n", 10);
//	for (i = 0; i < 10; i++)
//	{
//		scanf("%d", &num[i]);
//	}
//	maxValue = FindMax(num, 10, &maxPos);
//	printf("Max=%d, Position=%d\n", maxValue, maxPos);
//}
//int FindMax(int num[], int n, int* pMaxPos)
//{
//	int i, max = num[0];
//	*pMaxPos = 0;
//	for (i = 1; i < n; i++)
//	{
//		if (num[i] >= max)
//		{
//			max = num[i];
//			*pMaxPos = i;
//		}
//	}
//	return max;
//}