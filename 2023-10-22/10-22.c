#define _CRT_SECURE_NO_WARNINGS 1
//下面程序的功能是求解爱因斯坦数学题。
// 爱因斯坦曾出过这样一道数学题：有一条长阶梯，
// 若每步跨2阶，最后剩下1阶；
// 若每步跨3阶，最后剩下2阶；
// 若每步跨5阶，最后剩下4阶；
// 若每步跨6阶，最后剩下5阶；只
// 有每步跨7阶，最后才正好1阶不剩。编程打印这条阶梯共有多少阶。找出其中错误，并改正之。
//#include <stdio.h>
//main()
//{
//    int  x = 0;
//    int find = 1;
//
//    x = 0;
//    do {
//        ++x;
//        if (x % 2 == 1 && x % 3 == 2 && x % 5 == 4 && x % 6 == 5 && x % 7 == 0)
//        {
//            find = 0;
//        }
//    } while (find);
//    printf("x=%d\n", x);
//}

//猴子吃桃问题。猴子第一天摘下若干个桃子，
// 当即吃了一半，还不过瘾，又多吃了一个。
// 第二天早上又将剩下的桃子吃掉一半，又多吃了一个。
// 以后每天早上都吃了前一天剩下的一半零一个。
// 到第10天早上再想吃时，见只剩一个桃子。求第一天共摘了多少桃子。
//** 输出格式要求："桃子总数=%d\n"
//程序运行示例如下：
//桃子总数 = xxxx
//#include<stdio.h>
//int main()
//{
//	int sum = 1;
//	for (int i = 0; i < 9; i++)
//	{
//		sum = (sum + 1) * 2;
//	}
//	printf("桃子总数=%d\n", sum);
//	return 0;
//}

//为了倡导居民节约用电，某省电力公司执行“阶梯电价”，
//安装一户一表的居民用户电价分为两个“阶梯”：
//月用电量50千瓦时以内的，电价为0.53元 / 千瓦时；
//月用电量超过50千瓦时，电价上调0.05元 / 千瓦时。
//编写程序，输入用户的月用电量（千瓦时），计算并输出该用户应支付的电费（元）。
//* *输入格式要求："%lf" 提示信息："请输入月用电量：\n"
//* *输出格式要求："应支付电费=%.2f\n"
//程序运行示例如下：
//请输入月用电量：32↙
//应支付电费 = 16.96
//#include<stdio.h>
//int main()
//{
//	double x = 0;
//	double cost = 0;
//	printf("请输入月用电量：\n");
//	scanf("%lf", &x);
//	if (x > 50)
//	{
//		cost = 0.58 * x;
//	}
//	else
//	{
//		cost = 0.53 * x;
//	}
//	printf("应支付电费=%.2f\n", cost);
//	return 0;
//}

//从键盘输入10个整数，编程统计其中奇数(大于0)的个数。
//要求输入：% d
//输出：the odd number is% d
//(注意输出语句末尾不需要输出换行符\n)
//#include<stdio.h>
//int main()
//{
//	int x = 10;
//	int count = 0;
//	int num = 0;
//	while (x--)
//	{
//		scanf("%d", &num);
//		if (num % 2 == 1)
//		{
//			count++;
//		}
//	}
//	printf("the odd number is% d", count);
//	return 0;
//}

//打印100~1000之间能同时被3、5、17整除的数。
//* *输出格式要求："%d\n"
//#include<stdio.h>
//int main()
//{
//	int x = 0;
//	for (x = 100; x <= 1000; x++)
//	{
//		if (x % 3 == 0 && x % 5 == 0 && x % 17 == 0)
//		{
//			printf("%d\n", x);
//		}
//	}
//	return 0;
//}

//矩阵乘法。编写一个C函数实现M行K列矩阵与K行N列的矩阵的乘积。设A为M行K列的矩阵，B为K行N列的矩阵，则C = A×B的积为M行N列的矩阵。
//矩阵乘法的规则是：设A[m, k]，B[k, n]，则C[m, n] = A[m, k]×B[k, n]，其中：
//C[i, j] = ∑kl = 1A[i, l]×B[l, j], (i = 1, 2, …, m   j = 1, 2, …, n)
//* *输出格式要求："\t%d" "array A=\n" "array B=\n" "array C=\n"
//程序运行示例如下：
//array A =
//1	2	3
//4	5	6
//array B =
//1	1
//0	2
//2	0
//array C =
//7	5
//16	14
//#include<stdio.h>
//int main()
//{
//	int a[2][3];
//	int b[3][2];
//	int c[2][2];
//	printf("array A=\n");
//	for (int i = 0; i < 2; i++)
//	{
//		for (int j = 0; j < 3; j++)
//		{
//			scanf("%d", &a[i][j]);
//		}
//	}
//	printf("array B=\n");
//	for (int i = 0; i < 3; i++)
//	{
//		for (int j = 0; j < 2; j++)
//		{
//			scanf("%d", &b[i][j]);
//		}
//	}
//	for (int i = 0; i < 2; i++)
//	{
//		for (int j = 0; j < 2; j++)
//		{
//			int sum = 0;
//			for (int z = 0; z < 3; z++)
//			{
//				sum += a[i][z] * b[z][j];
//			}
//			c[i][j] = sum;
//		}
//	}
//	printf("array C=\n");
//	for (int i = 0; i < 2; i++)
//	{
//		for (int j = 0; j < 2; j++)
//		{
//			printf("\t%d", c[i][j]);
//		}
//		printf("\n");
//	}
//	return 0;
//}

//要求：从键盘输入10个不同的整数存入一个数组中，用函数编程将其中的最大数与最小数位置互换，
// 分别输出互换之前最大值和最小值及其在数组中的位置, 以及互换后的数组
//函数原型：void MaxMinExchang(int a[], int n);
//输入提示信息："Input 10 numbers:"
//输入格式："%d"
//最大值输出提示信息："Max element position:%d,Max element:%d\n"
//最小值输出提示信息："Min element position:%d,Min element:%d\n"
//数组输出提示信息："Exchange results:"
//数组输出格式："%4d"
//程序运行结果示例：
//Input 10 numbers:0 2 7 4 9 11 5 47 6 97↙
//Max element position : 9, Max element : 97
//Min element position : 0, Min element : 0
//Exchange results : 97   2   7   4   9  11   5  47   6   0
//#include<stdio.h>
//void MaxMinExchang(int a[], int n)
//{
//	int maxindex = 0;
//	int minindex = 0;
//	int max = a[0];
//	int min = a[0];
//	for (int i = 0; i < n; i++)
//	{
//		if (max < a[i])
//		{
//			maxindex = i;
//			max = a[i];
//		}
//		if (min > a[i])
//		{
//			minindex = i;
//			min = a[i];
//		}
//	}
//	printf("Max element position:%d,Max element:%d\n", maxindex, max);
//	printf("Min element position:%d,Min element:%d\n", minindex, min);
//	a[maxindex] = min;
//	a[minindex] = max;
//	printf("Exchange results:");
//	for (int i = 0; i < 10; i++)
//	{
//		printf("%4d", a[i]);
//	}
//}
//int main()
//{
//	int arr[10];
//	printf("Input 10 numbers:");
//	for (int i = 0; i < 10; i++)
//	{
//		scanf("%d", &arr[i]);
//	}
//	MaxMinExchang(arr, 10);
//	return 0;
//}

//用指针做函数参数自己编程实现字符串连接函数strcat()的功能。
//下面程序中存在比较隐蔽的错误，请通过分析和调试程序，发现并改正程序中的错误。
//注意：请将修改正确后的完整源程序拷贝粘贴到答题区内。
//对于没有错误的语句，请不要修改，修改原本正确的语句也要扣分。
//当且仅当错误全部改正，且程序运行结果调试正确，才给加5分。
//经教师手工核对后，如果未用指针做函数参数编程，那么即使做对也不给分。
//改错时不能改变程序原有的意图，不能改变函数原型。
//#include <stdio.h>
//#define N  80
//void MyStrcat(char* dstStr, char* srcStr);
//main()
//{
//    char s[N];
//    char t[N];
//    printf("Input a string:\n");
//    gets(s);
//    printf("Input another string:\n");
//    gets(t);
//    MyStrcat(s, t);
//    printf("Concatenate results:%s\n", s);
//}
//void MyStrcat(char* dstStr, char* srcStr)
//{
//    while (*dstStr != '\0')
//    {
//        dstStr++;
//    }
//    while (*srcStr != '\0')
//    {
//        *dstStr++ = *srcStr++;
//    }
//    *dstStr = '\0';
//}