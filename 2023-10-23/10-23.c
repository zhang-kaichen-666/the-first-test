#define _CRT_SECURE_NO_WARNINGS 1

//编程计算圆的面积。
//要求：
//（1）圆的半径r的值为1到10（包括1和10）之间的正整数；
//（2）用符号常量定义PI为3.14；
//（3）当圆的面积小于50时输出圆的面积并对圆的面积求累加和，
//大于50时结束循环；
//（4）输出累加和的结果
//（5）不用数组编程
//* *要求输入提示信息为：无输入提示信息和输入数据
//* *要求输出格式为：
//（1）"area=%.2f\n"
//（2）"sum=%.2f\n"
//#include<stdio.h>
//#define PI 3.14
//int main()
//{
//	double sum = 0;
//	double area = 0;
//	for (int r = 1; r <= 50; r++)
//	{
//		if (sum < 50)
//		{
//			area = PI * r * r;
//			sum += area;
//			if (sum < 50)
//			{
//				printf("area=%.2f\n", area);
//			}
//		}
//		else
//		{
//			break;
//		}
//	}
//	sum -= area;
//	printf("sum=%.2f\n", sum);
//	return 0;
//}

//已知立方和不等式为 1^3 + 2^3 + … + m^3 < n 对指定的n值，试求满足上述立方和不等式的m的整数解。
//输入提示信息："Please enter n:"
//输入格式："%ld"
//输出格式："m<=%1d\n"
//#include<stdio.h>
//#include<math.h>
//int main()
//{
//    long n, m, sum = 0;
//    int i;
//    printf("Please enter n:");
//    scanf("%ld", &n);
//    for (i = 1;; i++)
//    {
//        sum += pow(i, 3);
//        if (sum >= n)
//        {
//            printf("m<=%1d\n", i - 1);
//            break;
//        }
//    }
//    return 0;
//}
