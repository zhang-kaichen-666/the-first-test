#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<stdbool.h>
typedef int QDataType;
// 链式结构：表示队列 
typedef struct QListNode
{
	struct QListNode* _next;
	QDataType _data;
}QNode;

// 队列的结构 
typedef struct Queue
{
	QNode* _front;
	QNode* _rear;
}Queue;

// 初始化队列 
void QueueInit(Queue* q);
// 队尾入队列 
void QueuePush(Queue* q, QDataType data);
// 队头出队列 
void QueuePop(Queue* q);
// 获取队列头部元素 
QDataType QueueFront(Queue* q);
// 获取队列队尾元素 
QDataType QueueBack(Queue* q);
// 获取队列中有效元素个数 
int QueueSize(Queue* q);
// 检测队列是否为空，如果为空返回非零结果，如果非空返回0 
int QueueEmpty(Queue* q);
// 销毁队列 
void QueueDestroy(Queue* q);

// 初始化队列 
void QueueInit(Queue* q)
{
	assert(q);
	q->_front = q->_rear = NULL;
}
// 队尾入队列 
void QueuePush(Queue* q, QDataType data)
{
	assert(q);
	if (q->_rear == NULL)
	{
		q->_front = q->_rear = (QNode*)malloc(sizeof(QNode));
		if (q->_rear == NULL)
		{
			perror("q->_rear malloc");
			return;
		}
		q->_rear->_data = data;
		q->_rear->_next = NULL;
	}
	else
	{
		QNode* newnode = (QNode*)malloc(sizeof(QNode));
		if (newnode == NULL)
		{
			perror("newnode malloc");
			return;
		}
		newnode->_data = data;
		newnode->_next = NULL;
		q->_rear->_next = newnode;
		q->_rear = newnode;
	}
}
// 队头出队列 
void QueuePop(Queue* q)
{
	assert(q);
	assert(q->_front);
	QNode* del = q->_front;
	q->_front = q->_front->_next;
	if (q->_front == NULL)
	{
		q->_rear = NULL;
		return;
	}
	free(del);
	del = NULL;
}
// 获取队列头部元素 
QDataType QueueFront(Queue* q)
{
	assert(q);
	assert(q->_front);
	return q->_front->_data;
}
// 获取队列队尾元素 
QDataType QueueBack(Queue* q)
{
	assert(q);
	assert(q->_rear);
	return q->_rear->_data;
}
// 获取队列中有效元素个数 
int QueueSize(Queue* q)
{
	assert(q);
	int count = 0;
	QNode* cur = q->_front;
	while (cur)
	{
		count++;
		cur = cur->_next;
	}
	return count;
}
// 检测队列是否为空，如果为空返回非零结果，如果非空返回0 
int QueueEmpty(Queue* q)
{
	assert(q);
	if (q->_front == NULL)
	{
		return 1;
	}
	return 0;
}
// 销毁队列 
void QueueDestroy(Queue* q)
{
	assert(q);
	while (q->_front != NULL)
	{
		QNode* del = q->_front;
		q->_front = q->_front->_next;
		free(del);
	}
	q->_rear = NULL;
}


typedef struct {
	Queue q1;
	Queue q2;
} MyStack;


MyStack* myStackCreate() {
	MyStack* st = (MyStack*)malloc(sizeof(MyStack));
	QueueInit(&st->q1);
	QueueInit(&st->q2);
	return st;
}

void myStackPush(MyStack* obj, int x) {
	Queue* nonequ = &obj->q1;
	Queue* qu = &obj->q2;
	if (QueueEmpty(&obj->q1))
	{
		nonequ = &obj->q2;
		qu = &obj->q1;
	}
	QueuePush(nonequ, x);
}

int myStackPop(MyStack* obj) {
	Queue* nonequ = &obj->q1;
	Queue* qu = &obj->q2;
	if (QueueEmpty(&obj->q1))
	{
		nonequ = &obj->q2;
		qu = &obj->q1;
	}
	while (QueueSize(nonequ) > 1)
	{
		QueuePush(qu, QueueFront(nonequ));
		QueuePop(nonequ);
	}
	int tmp = QueueFront(nonequ);
	QueuePop(nonequ);
	return tmp;
}

int myStackTop(MyStack* obj) {
	Queue* nonequ = &obj->q1;
	Queue* qu = &obj->q2;
	if (QueueEmpty(&obj->q1))
	{
		nonequ = &obj->q2;
		qu = &obj->q1;
	}
	return QueueFront(nonequ);
}

bool myStackEmpty(MyStack* obj) {
	if (QueueEmpty(&obj->q1) && QueueEmpty(&obj->q2))
	{
		return false;
	}
	return true;
}

void myStackFree(MyStack* obj) {
	free(&obj->q1);
	free(&obj->q2);
}

int main()
{
	MyStack* st = myStackCreate();
	myStackPush(st, 1);
	myStackPush(st, 2);
	myStackTop(st);
	myStackPop(st);
	myStackEmpty(st);
	return 0;
}