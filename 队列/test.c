#include"Queue.h"

void test()
{
	Queue q;
	QueueInit(&q);
	QueuePush(&q, 1);
	printf("size = %d ,frontdata = %d,backdata = %d\n", QueueSize(&q), QueueFront(&q), QueueBack(&q));
	QueuePop(&q);
	QueuePush(&q, 2);
	QueuePush(&q, 3);
	QueuePush(&q, 4);
	while (!QueueEmpty(&q))
	{
		printf("size = %d ,frontdata = %d,backdata = %d\n", QueueSize(&q), QueueFront(&q), QueueBack(&q));
		QueuePop(&q);
	}

	QueueDestroy(&q);
}
int main()
{
	test();
	return 0;
}