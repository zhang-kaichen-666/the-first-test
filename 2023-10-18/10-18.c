#define _CRT_SECURE_NO_WARNINGS 1
//利用一个字符数组作函数参数, 实现字符串（最大长度为80个字符 ）的逆序存放。
//要求如下：
//(1)在子函数Inverse中实现字符串的逆序存放。函数原型为：
//void Inverse(char str[]);
//(2)在主函数中
//从键盘输入字符串(使用gets函数)
//然后，调用Inverse函数，
//最后，输出逆序后的字符串。
//(3)** 输入提示信息："Input a string:\n"
//* *输出提示信息："Inversed results:\n"
//* *输出格式："%s\n"
//#include<stdio.h>
//#include<string.h>
//void Inverse(char str[])
//{
//	int left = 0;
//	int right = strlen(str) - 1;
//	while (left < right)
//	{
//		char tmp = str[right];
//		str[right] = str[left];
//		str[left] = tmp;
//		left++;
//		right--;
//	}
//}
//int main()
//{
//	char str[100];
//	printf("Input a string:\n");
//	gets(str);
//	Inverse(str);
//	printf("Inversed results:\n");
//	printf("%s\n", str);
//	return 0;
//}

//* 对输入的行和字符进行计数。在计算机中，一行是以一个回车符\n作为行结束标记的，
//这样在程序中可以通过搜索\n对行进行计数。在UNIX操作系统中，一般有ctrl + d作为文件结束标记，
//其字符码为 - 1.当输入ctrl + d时表示文件输入结束，停止计数。在C语言中，以EOF（End Of File）作为文件结束标志
//* *输出格式要求："chars=%d, lines=%d\n"
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	int lines = 0;
//	int chars = 0;
//	char ch;
//	while ((ch = getchar()) != EOF)
//	{
//		chars++;
//		if (ch == '\n')
//		{
//			lines++;
//		}
//	}
//	printf("chars=%d, lines=%d\n", chars, lines);
//	return 0;
//}

//将一个链表中元素值为x的结点删除。（链表数据域为整数，初始长为6个元素）
//程序运行示例如下：
//输入数组6个元素的值。
//11 22 33 44 55 66
//此链表各个结点的数据域为：11 22 33 44 55 66
//输入要删除的数据x: 33
//删除后链表各个结点的数据域为：11 22 44 55 66
//#include<stdio.h>
//#include<stdlib.h>
//struct LinkList
//{
//	int info;
//	struct LinkList* next;
//};
//typedef struct LinkList LinkList;
//LinkList* ListBuyNode(int x)
//{
//	LinkList* node = (LinkList*)malloc(sizeof(LinkList));
//	if (node == NULL)
//	{
//		perror("node malloc");
//	}
//	node->info = x;
//	node->next = NULL;
//	return node;
//}
//LinkList* CreatLinkList()//创建一个6个节点的链表
//{
//	LinkList* phead = ListBuyNode(1);
//	LinkList* pTail = phead;
//	int arr[6];
//	printf("输入数组6个元素的值。\n");
//	for (int i = 0; i < 6; i++)
//	{
//		scanf("%d", &arr[i]);
//	}
//	for (int i = 0; i < 6; i++)
//	{
//		LinkList* node = ListBuyNode(arr[i]);
//		pTail->next = node;
//		pTail = pTail->next;
//	}
//	return phead->next;
//}
//void PrintLinkList(LinkList* phead)
//{
//	LinkList* pcur = phead;
//	while (pcur)
//	{
//		printf("%d ", pcur->info);
//		pcur = pcur->next;
//	}
//	printf("\n");
//}
//void PopLinkListnode(LinkList** pphead)
//{
//	int x = 0;
//	LinkList* pcur = *pphead;
//	LinkList* prev = *pphead;
//	printf("输入要删除的数据x: ");
//	scanf("%d", &x);
//	while (pcur)
//	{
//		if (pcur->info == x)
//		{
//			if (pcur == *pphead)
//			{
//				*pphead = pcur->next;
//				free(pcur);
//				pcur = NULL;
//				return;
//			}
//			else
//			{
//				prev->next = pcur->next;
//				free(pcur);
//				pcur = NULL;
//				return;
//			}
//		}
//		prev = pcur;
//		pcur = pcur->next;
//		
//	}
//	printf("没找到对应的值");
//}
//int main()
//{
//	LinkList* phead = CreatLinkList();
//	printf("此链表各个结点的数据域为：");
//	PrintLinkList(phead);
//	PopLinkListnode(&phead);
//	printf("删除后链表各个结点的数据域为：");
//	PrintLinkList(phead);
//	return 0;
//}

//用函数编程实现输出 m~n之间所有素数，并求和，m和n由键盘输入。
//素数是只能被1和自身整除的大于1的正整数。 要求程序能对用户输入的数据进行正确性检查，满足条件：m和n都是大于1的正整数，并且m <= n。
//1. 要求编写函数InputNumber 实现用户输入一个正整数，并对数的合法性进行检查，如果读入不成功(例如：输入字符串)，
//或者数不合法即不是大于1的正整数，则重新输入，直到输入正确为止。
//函数原型：
//unsigned int InputNumber(char ch);
//形式参数：ch，用于生成输入提示信息。
//返回值：返回正确的输入值。
//输入提示信息：printf("Please input the number %c(>1):", ch);
//输入格式："%d"
//输入错误提示信息："The input must be an integer larger than 1!\n"
//提示：测试scanf的返回值来判断是否成功读入数据。
//2. 要求编写函数IsPrime判断自然数x是否为素数，如果x是素数则返回1，不是素数返回0。
//函数原型：
//int IsPrime(unsigned int n);
//返回值：如果x是素数则返回1，不是素数返回0。
//
//3. 要求编写函数PrimeSum 输出m到n之间所有素数并返回它们的和。
//函数原型：
//int PrimeSum(unsigned int m, unsigned int n);
//返回值：m到n之间所有素数的和。
//每个素数的输出格式："%d\n"
//#include<stdio.h>
//#include<math.h>
//unsigned int InputNumber(char ch)
//{
//    printf("Please input the number %c(>1):", ch);
//    int n = 0;
//    while ((scanf("%d", &n) != 1) || (n <= 1))
//    {
//        while (getchar() != '\n')
//        {
//            continue;
//        }
//        printf("The input must be an integer larger than 1!\n");
//        printf("Please input the number %c(>1):", ch);
//
//    }
//    return n;
//}
//int IsPrime(unsigned int n)
//{
//    if (n == 2)
//    {
//        return 1;
//    }
//    for (int i = 2; i <= sqrt(n); i++)
//    {
//        if (n % i == 0)
//        {
//            return 0;
//        }
//    }
//    return 1;
//}
//int PrimeSum(unsigned int m, unsigned int n)
//{
//    unsigned int sum = 0;
//    for (unsigned int i = m; i <= n; i++)
//    {
//        if (IsPrime(i))
//        {
//            printf("%d\n", i);
//            sum += i;
//        }
//    }
//    return sum;
//}
//int main()
//{
//    int m = 0, n = 0, sum = 0, i = 0;
//    do
//    {
//        m = InputNumber('m');
//        n = InputNumber('n');
//    } while (m > n && printf("n must be not smaller than m! Input again!\n"));   //保证m<=n
//    sum = PrimeSum(m, n);
//    printf("sum of prime numbers:%d", sum);
//    return 0;
//}

//班级共有 m个人，该班C语言的成绩存放在score（score为整数）数组中，计算该班成绩的平均分，
// 并将小于平均分的成绩存储在一个数组中，并打印该数组的值。
//要求：
//1.	请编写函数fun, 它的功能是：计算平均分，
// 并将低于平均分的成绩和相应的数组下标分别存在不同的数组中(打印语句放在主函数中)，声明如下：
//int fun(int score[], int m, int below_score[], int below_index[]);
//2.	请编写函数ReadScore，读入成绩，返回输入的有效人数，声明如下：
//int ReadScore(int score[]);
//3.	需要对数组越界做判断，如：在输入时，直接输入 - 1的情况，此时显示“there are no valid scores”，并终止程序。
//4.	班机最多有40人，用宏定义数组的所含最多的元素数量。
//
//输入：每一行输入一个人的成绩，直到输入值为负数时，结束成绩的输入，并将此时拥有的成绩数量，作为班机人数，如：
//45
//67
//98
//- 1
//输出：打印班机的总人数，低于平均分的，低于平均分的成绩及该成绩在输入时的序号，从1开始计数，如：
//the number of the class :3
//the number under the average score : 2
//the 1th score is : 45
//the 2th score is : 67
//#define NUMS_MAX 40
//#include<stdio.h>
//int ReadScore(int score[])
//{
//	int i = 0;
//	int flag = 0;
//	while (flag  == 0)
//	{
//		scanf("%d", &score[i]);
//		if (score[i] < 0)
//		{
//			flag = 1;
//			break;
//		}
//		i++;
//	}
//	return i;
//
//
//}
//int fun(int score[], int m, int below_score[], int below_index[])
//{
//	int average = 0;
//	int sum = 0;
//	int j = 0;
//	int count = 0;
//	for (int i = 0; i < m; i++)
//	{
//		sum += score[i];
//	}
//	average = sum / m;
//	for (int i = 0; i < m; i++)
//	{
//		if (score[i] < average)
//		{
//			below_score[j] = score[i];
//			below_index[j] = i;
//			j++;
//			count++;
//		}
//	}
//	return count;
//
//}
//int main()
//{
//	int score[NUMS_MAX];
//	int m = ReadScore(score);
//	int below_score[NUMS_MAX / 2];
//	int below_index[NUMS_MAX / 2];
//	printf("the number of the class:%d\n", m);
//	int count = fun(score, m, below_score, below_index);
//	printf("the number under the average score: %d\n", count);
//	for (int i = 0; i < count; i++)
//	{
//		printf("the %dth score is: %d\n", below_index[i] + 1, below_score[i]);
//	}
//	return 0;
//}

//输入字符串，包含：字母、数字、标点符号，以及空格字符，并将其逆序输出。
//例如，当输入字符串为“I am a student.”, 输出为“.tneduts a ma I”，假设字符数组最大长度为30。
//输入提示信息："Please Enter String1:\n"
//输入格式：gets()
//输出格式："Result is:\n%s\n"
#include<stdio.h>
#include<string.h>
#define STR_MAX 30
int main()
{
	char str[STR_MAX];
	printf("Please Enter String1:\n");
	gets(str);
	int left = 0;
	int right = strlen(str) - 1;
	while (left < right)
	{
		char tmp = str[left];
		str[left] = str[right];
		str[right] = tmp;
		left++;
		right--;
	}
	printf("Result is:\n%s\n", str);

	return 0;
}