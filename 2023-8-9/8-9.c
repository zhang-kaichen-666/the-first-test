#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>



//获取一个整数二进制序列中所有的偶数位和奇数位，分别打印出二进制序列
//int main()
//{
//	int n = 0;
//	printf("请输入一个整数:");
//	scanf("%d", &n);
//	printf("偶数位：");
//	for (int i = 31; i >= 1; i -= 2)
//	{
//		if ((n >> i) & 1)
//		{
//			printf("1 ");
//		}
//		else
//			printf("0 ");
//	}
//	printf("\n");
//	printf("奇数位：");
//	for (int i = 30; i >= 0; i -= 2)
//	{
//		if ((n >> i) & 1)
//		{
//			printf("1 ");
//		}
//		else
//			printf("0 ");
//	}
//	return 0;
//}


//编程实现：两个int（32位）整数m和n的二进制表达中，有多少个位(bit)不同？
//输入例子 :
//1999 2299
//输出例子 : 7
//int main()
//{
//    int x = 0;
//    int y = 0;
//    int i = 0;
//    int count = 0;
//    scanf("%d%d", &x, &y);
//    for (i = 31; i >= 0; i--)
//    {
//        if (((x >> i) & 1) != ((y >> i) & 1))
//        {
//            count++;
//        }
//    }
//    printf("%d\n", count);
//    return 0;
//}