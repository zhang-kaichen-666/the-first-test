//从键盘任意输入一个字符，编程判断该字符是数字字符、英文字母、空格还是其他字符。
//** 输入格式要求：提示信息："Press a key and then press Enter:"
//* *输出格式要求："It is an English character!\n" 
// "It is a digit character!\n"  
// "It is a space character!\n" 
//  "It is other character!\n"
//程序运行示例1如下：
//Press a key and then press Enter : A
//It is an English character!
//程序运行示例2如下：
//Press a key and then press Enter : 2
//It is a digit character!
//程序运行示例3如下：
//Press a key and then press Enter :
//It is a space character!
//程序运行示例4如下：
//Press a key and then press Enter : #
//It is other character!
//#include<stdio.h>
//#include<ctype.h>
//int main()
//{
//	char ch = '0';
//	printf("Press a key and then press Enter:");
//	scanf("%c", &ch);
//	if (isalpha(ch))
//	{
//		printf("It is an English character!\n");
//	}
//	else if (isdigit(ch))
//	{
//		printf("It is a digit character!\n");
//	}
//	else if (isspace(ch))
//	{
//		printf("It is a space character!\n");
//	}
//	else
//	{
//		printf("It is other character!\n");
//	}
//	return 0;
//}

//编程输出以下形式的乘法九九表：
//1 * 1 = 1
//2 * 1 = 2 2 * 2 = 4
//3 * 1 = 3 3 * 2 = 6 3 * 3 = 9
//4 * 1 = 4 4 * 2 = 8 4 * 3 = 12 4 * 4 = 16
//5 * 1 = 5 5 * 2 = 10 5 * 3 = 15 5 * 4 = 20 5 * 5 = 25
//6 * 1 = 6 6 * 2 = 12 6 * 3 = 18 6 * 4 = 24 6 * 5 = 30 6 * 6 = 36
//7 * 1 = 7 7 * 2 = 14 7 * 3 = 21 7 * 4 = 28 7 * 5 = 35 7 * 6 = 42 7 * 7 = 49
//8 * 1 = 8 8 * 2 = 16 8 * 3 = 24 8 * 4 = 32 8 * 5 = 40 8 * 6 = 48 8 * 7 = 56 8 * 8 = 64
//9 * 1 = 9 9 * 2 = 18 9 * 3 = 27 9 * 4 = 36 9 * 5 = 45 9 * 6 = 54 9 * 7 = 63 9 * 8 = 72 9 * 9 = 81
//
//* *输出格式要求："%1d*%1d=%2d "
//#include<stdio.h>
//int main()
//{
//	int i = 1;
//	int j = 1;
//	for (i = 1; i < 10; i++)
//	{
//		for (j = 1; j <= i; j++)
//		{
//			printf("%1d*%1d=%2d ", i, j, i * j);
//		}
//		printf("\n");
//	}
//	return 0;
//}

//一辆卡车违反了交通规则，撞人后逃逸。
// 现场有三人目击该事件，但都没有记住车号，只记住车号的一些特征。
// 甲说：车号的前两位数字是相同的；
// 乙说：车号的后两位数字是相同的，但与前两位不同；
// 丙是位数学家，他说：4位的车号正好是一个整数的平方。
// 请根据以上线索编程协助警方找出车号，以便尽快破案，抓住交通肇事犯。
//** 输出格式要求："k=%d, m=%d\n" (k为车号，k = m * m)
//#include<stdio.h>
//int main()
//{
//	int x = 0;//前两位的数字
//	int y = 0;//后两位的数字
//	int m = 0;//整数
//	again:
//	for (x = 0; x < 10; x++)
//	{
//		for (y = 0; y < 10; y++) {
//			int k = 1100 * x + 11 * y;
//			if (k == m * m && x != y)
//			{
//				printf("k=%d, m=%d\n", k, m);
//			}
//		}
//	}
//	if (m < 100)
//	{
//		m++;
//		goto again;
//	}
//	return 0;
//}

//用if - else语句编程根据输入的百分制成绩score，转换成相应的五分制成绩grade后输出。已知转换标准为：
//0 - 59    E
//60 - 69   D
//70 - 79   C
//80 - 89   B
//90 - 100  A
//* *输入格式要求："%d" 提示信息："Please enter score:"
//* *输出格式要求："Input error!\n" "%d——A\n"
//程序运行示例1如下：
//Please enter score : 15
//15——E
//程序运行示例2如下：
//Please enter score : 85
//85——B
//#include<stdio.h>
//int main()
//{
//	int score = 0;
//	printf("Please enter score:");
//	scanf("%d", &score);
//	if (score >= 0 && score < 60)
//	{
//		printf("%d——E\n", score);
//	}
//	else if (score >= 60 && score < 70)
//	{
//		printf("%d——D\n", score);
//	}	
//	else if (score >= 70 && score < 80)
//	{
//		printf("%d——C\n", score);
//	}	
//	else if (score >= 80 && score < 90)
//	{
//		printf("%d——B\n", score);
//	}	
//	else if (score >= 90 && score <= 100)
//	{
//		printf("%d——A\n", score);
//	}
//	else
//	{
//		printf("Input error!\n");
//	}
//	return 0;
//}

