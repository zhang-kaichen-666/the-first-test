#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>

//写一个函数打印arr数组的内容，不使用数组下标，使用指针。
//arr是一个整形一维数组。
//void print(int* arr, int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", *(arr + i));
//	}
//}
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 }; 
//	int * p = arr;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	print(p, sz);
//	return 0;
//}


//实现一个对整形数组的冒泡排序
//void sort_bubble(int* arr, int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz - 1; i++)
//	{
//		//一次冒泡排序
//		int j = 0;
//		int flag = 1;
//		for (j = 0; j < sz - 1; j++)
//		{
//			if (arr[j] > arr[j + 1])
//			{
//				int tmp = arr[j];
//				arr[j] = arr[j + 1];
//				arr[j + 1] = tmp;
//				flag = 0;
//			}
//		}
//		if (flag == 1)
//		{
//			break;
//		}
//	}
//}
//void print(int* arr, int sz)//打印整个数组
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", *(arr + i));
//	}
//}
//int main()
//{
//	int arr[10] = { 3,2,4,6,5,7,8,9,1,10 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	sort_bubble(arr,sz);
//	print(arr,sz);
//	return 0;
//}



//作业内容
//调整数组使奇数全部都位于偶数前面。
//题目：
//输入一个整数数组，实现一个函数，
//来调整该数组中数字的顺序使得数组中所有的奇数位于数组的前半部分，
//所有偶数位于数组的后半部分。
//void print(int* arr, int sz)//打印整个数组
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", *(arr + i));
//	}
//}
//sort(int* arr,int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz - 1; i++)
//	{
//		int j = 0;
//		int flag = 1;
//		for (j = 0; j < sz - 1; j++)
//		{
//			if (arr[j] % 2 == 0)
//			{
//				int tmp = arr[j];
//				arr[j] = arr[j + 1];
//				arr[j + 1] = tmp;
//				flag = 0;
//			}
//		}
//		if (flag == 1)
//		{
//			break;
//		}
//	}
//}
//int main()
//{
//	int arr[6] = { 1,2,3,4,5,6 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	sort(arr, sz);
//	print(arr, sz);
//	return 0;
//}


//写一个函数，判断一个字符串是否为另外一个字符串旋转之后的字符串。
//例如：给定s1 = AABCD和s2 = BCDAA，返回1
//给定s1 = abcd和s2 = ACBD，返回0.
//AABCD左旋一个字符得到ABCDA
//AABCD左旋两个字符得到BCDAA
//AABCD右旋一个字符得到DAABC
//int judge(char* str1, char* str2, size_t len)
//{
//	int i = 0;
//	for (i = 0; i <= len - 1; i++)
//	{
//
//		//一次旋转得到的字符串：
//		int j = 0;
//		for (j = 0; j < len - 1; j++)
//		{
//			char tmp = str1[j];
//			str1[j] = str1[j + 1];
//			str1[j + 1] = tmp;
//		}
//		if (strcmp(str1, str2) == 0)
//		{
//			return 1;
//		}
//	}
//	return 0;
//}
//int main()
//{
//	char str1[10000] = {0};
//	char str2[10000] = {0};
//	printf("请输入s1:");
//	scanf("%s", &str1);
//	printf("请输入s2:");
//	scanf("%s", &str2);
//	size_t len1 = strlen(str1);
//	size_t len2 = strlen(str2);
//	if (len1 != len2)
//	{
//		printf("不是。");
//	}
//	else
//	{
//		if (judge(str1, str2, len1) == 1)
//		{
//			printf("是。");
//		}
//		else if (judge(str1, str2, len1) == 0)
//		{
//			printf("不是。");
//		}
//	}
//	return 0;
//}
