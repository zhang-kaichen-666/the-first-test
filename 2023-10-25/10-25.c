#define _CRT_SECURE_NO_WARNINGS 1
//单词接龙
//阿泰和女友小菲用英语短信玩单词接龙游戏。
// 一人先写一个英文单词，然后另一个人回复一个英文单词，
// 要求回复单词的开头有若干个字母和上一个人所写单词的结尾若干个字母相同，
// 重合部分的长度不限。（如阿泰输入happy，小菲可以回复python，重合部分为py。）
// 现在，小菲刚刚回复了阿泰一个单词，
// 阿泰想知道这个单词与自己发过去的单词的重合部分是什么。
// 他们两人都是喜欢写长单词的英语大神，
// 阿泰觉得用肉眼找重合部分实在是太难了，所以请你编写程序来帮他找出重合部分。
//程序运行结果示例1：
//happy↙
//pythen↙
//py
//程序运行结果示例2：
//sun↙
//unknown↙
//un
//输入格式 : "%s%s"
//输出格式： "%s\n"
//#include<string.h>
//#include<stdio.h>
//int main()
//{
//	char str1[100];
//	char str2[100];
//	int i = 0;
//	int j = 0;
//	int count = 0;
//	scanf("%s%s", str1, str2);
//	for (i = 0; i < strlen(str1); i++)
//	{
//		if (str1[i] == str2[0])
//		{
//			break;
//		}
//	}
//	while (i < strlen(str1))
//	{
//		if (str1[i] == str2[j])
//		{
//			i++;
//			j++;
//			count++;
//		}
//		else
//		{
//			count = 0;
//			j = 0;
//		}
//	}
//	i = i - count;
//	for (; i < strlen(str1); i++)
//	{
//		printf("%c", str1[i]);
//	}
//}
