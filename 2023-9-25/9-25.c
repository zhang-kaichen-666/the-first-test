﻿#include<stdio.h>
#include<stddef.h>
#include<string.h>

//预处理详解
//1. 预定义符号
//int main()
//{
//	printf("%s\n", __FILE__);//文件名
//	printf("%d\n", __LINE__);//行号
//	printf("%s\n", __DATE__);
//	printf("%s\n", __TIME__);
//	//printf("%d\n", __STDC__);//未定义，说明vs是不支持 ANSI C 的
//	return 0;
//}

//2. #define定义常量
//#define M 100
//#define STR "hehe"
//int main()
//{
//	int a = M;
//	printf("%d\n", a);
//	printf("%s\n", STR);
//	return 0;
//}
//基本语法：#define name stuff
//例子:
//#define MAX 1000
//#define reg register //为 register这个关键字，创建⼀个简短的名字
//#define do_forever for(;;) //⽤更形象的符号来替换⼀种实现
//#define CASE break;case //在写case语句的时候⾃动把 break写上。
//// 如果定义的 stuff过⻓，可以分成⼏⾏写，除了最后⼀⾏外，每⾏的后⾯都加⼀个反斜杠(续⾏符)。
//#define DEBUG_PRINT printf("file:%s\tline:%d\t \
// date:%s\ttime:%s\n" ,\
// __FILE__,__LINE__ , \
// __DATE__,__TIME__ )

//3. #define定义宏(不要吝啬括号)
//#define name( parament-list ) stuff
//#define SQUARE(X) X*X//可以写多个参数   SQUARE和()必须连接在一块
////写成#define SQUARE(X) ((X)*(X))    这样可以解决问题
//int main()
//{
//	int a = 5;
//	printf("%d\n", SQUARE(a));//SQUARE直接替换为a*a，这里也会出现风险，如下
//	printf("%d\n", SQUARE(a+2));//这里会打印17，因为替换为a+2*a+2 == 17
//	return 0;
//}
//也可能出现以下问题
//#define DOUBLE(x) x+x
////最好写成#define DOUBLE(x) (x+x)
//int main()
//{
//	int a = 4;
//	printf("%d\n", 10 * DOUBLE(a));//会替换为10*a + a
//	return 0;
//}

//4. 带有副作⽤的宏参数
//#define MAX(a,b) ((a)>(b)?(a):(b))
////函数是传值进去，但是宏是替换进去
//int main()
//{
//	//int m = MAX(15, 9);
//	//printf("%d\n", m);
//	int a = 15;
//	int b = 9;
//
//	int m = MAX(a++, b++);//会导致a++ 两次
//	printf("%d\n", m);//16
//	printf("%d\n", a);//17
//	printf("%d\n", b);//10
//	return 0;
//}

//5. 宏替换的规则
//在程序中扩展#define定义符号和宏时，需要涉及⼏个步骤。
//1. 在调⽤宏时，⾸先对参数进⾏检查，看看是否包含任何由#define定义的符号。如果是，它们⾸先被替换。
//2. 替换⽂本随后被插⼊到程序中原来⽂本的位置。对于宏，参数名被他们的值所替换。
//3. 最后，再次对结果⽂件进⾏扫描，看看它是否包含任何由#define定义的符号。如果是，就重复上述处理过程。
//注意：
//1. 宏参数和#define 定义中可以出现其他#define定义的符号。但是对于宏，不能出现递归。
//2. 当预处理器搜索#define定义的符号的时候，字符串常量的内容并不被搜索。//"这里的符号不会被替换"

//6.宏与函数对比
//宏通常被应⽤于执⾏简单的运算。
//⽐如在两个数中找出较⼤的⼀个时，写成下⾯的宏，更有优势⼀些。
//#define MAX(a, b) ((a) > (b) ? (a) : (b))
//那为什么不⽤函数来完成这个任务？
//原因有⼆：
//1. ⽤于调⽤函数和从函数返回的代码可能⽐实际执⾏这个⼩型计算⼯作所需要的时间更多。所以宏⽐
//函数在程序的规模和速度⽅⾯更胜⼀筹。
//2. 更为重要的是函数的参数必须声明为特定的类型。所以函数只能在类型合适的表达式上使⽤。反之
//这个宏怎可以适⽤于整形、⻓整型、浮点型等可以⽤于 > 来⽐较的类型。宏是类型⽆关的。
//
//和函数相⽐宏的劣势：
//1. 每次使⽤宏的时候，⼀份宏定义的代码将插⼊到程序中。除⾮宏⽐较短，否则可能⼤幅度增加程序的⻓度。
//2. 宏是没法调试的。
//3. 宏由于类型⽆关，也就不够严谨。
//4. 宏可能会带来运算符优先级的问题，导致程容易出现错。
//宏有时候可以做函数做不到的事情。⽐如：宏的参数可以出现类型，但是函数做不到。
//
//一般，计算逻辑比较简单 -- 使用宏
//              比较复杂 -- 使用函数

//#和##
//#运算符将宏的⼀个参数转换为字符串字⾯量。它仅允许出现在带参数的宏的替换列表中。
//#运算符所执⾏的操作可以理解为”字符串化“。
//当我们有⼀个变量 int a = 10; 的时候，我们想打印出： the value of a is 10 .
//就可以写：
//#define PRINT(n) printf("the value of "#n " is %d", n);
//当我们按照下⾯的⽅式调⽤的时候：
//PRINT(a);//当我们把a替换到宏的体内时，就出现了#a，⽽#a就是转换为"a"，时⼀个字符串
//代码就会被预处理为：
//printf("the value of ""a" " is %d", a);
//可以再加一个参数
//变为
//#define PRINT(n,format) printf("the value of " #n " is "format"\n",n)
//int main()
//{
//	int a = 10;
//	PRINT(a, "%d");
//	float f = 3.14f;
//	PRINT(f, "%f");
//	return 0;
//}
//##运算符
//## 可以把位于它两边的符号合成⼀个符号，它允许宏定义从分离的⽂本⽚段创建标识符。 ## 被称
//为记号粘合
//这样的连接必须产⽣⼀个合法的标识符。否则其结果就是未定义的
//例：
//写⼀个函数求2个数的较⼤值的时候，不同的数据类型就得写不同的函数。
//int int_max(int x, int y)
//{
//	return x > y ? x : y;
//}
//float float_max(float x, float y)
//{
//	return x > y ? x:y;
//}
//但是这样写起来太繁琐了，现在我们这样写代码试试：
//宏定义
//GENERIC_MAX为通用的模板，这个模板可以定义函数
//     \ 为续行符
//#define GENERIC_MAX(type) \
//type type##_max(type x, type y)\
//{ \
// return (x>y?x:y); \
//}
//
//GENERIC_MAX(int)
//GENERIC_MAX(float)
//int main()
//{
//	int a = 10;
//	int b = 20;
//	int ret = int_max(a, b);
//	printf("%d\n", ret);
//  float fm = float_max(3.5f, 4.5f);
//  printf("%f\n", fm);
//	return 0;
//}

//8. 命名约定
//⼀般来讲函数的宏的使⽤语法很相似。所以语⾔本⾝没法帮我们区分⼆者。
//那我们平时的⼀个习惯是：
//把宏名全部⼤写
//函数名不要全部⼤写

//9. #undef
//这条指令⽤于移除⼀个宏定义。
//#undef NAME
//如果现存的⼀个名字需要被重新定义，那么它的旧名字⾸先要被移除
//例：
//#define M 100
//#undef M
//#define M 200

//10. 命令⾏定义
//许多C 的编译器提供了⼀种能⼒，允许在命令⾏中定义符号。⽤于启动编译过程。
//例如：当我们根据同⼀个源⽂件要编译出⼀个程序的不同版本的时候，这个特性有点⽤处。（假定某
//个程序中声明了⼀个某个⻓度的数组，如果机器内存有限，我们需要⼀个很⼩的数组，但是另外⼀个
//机器内存⼤些，我们需要⼀个数组能够⼤些。）
//vs不支持，在gcc下可以验证

//11. 条件编译
//在编译⼀个程序的时候我们如果要将⼀条语句（⼀组语句）编译或者放弃是很⽅便的。因为我们有条件编译指令。
//⽐如说：调试性的代码，删除可惜，保留⼜碍事，所以我们可以选择性的编译。
//满足条件就编译，不满足条件就放弃编译
//#define FLAG 3
//#define MAX 10
//int main()
//{
//#if FLAG==2//只有 FLAG == 3 的时候会打印 hehe
//	printf("hehe\n");
//#elif FLAG == 3
//	printf("haha\n");
//#else
//	printf("heihei\n")
//#endif//常量表达式由预处理器求值。
//		//注意：一定要是常量表达式，变量不可以
//
//#if defined(MAX)//判断是否被定义     等同于   #ifdef MAX
//	printf("hehe");
//#endif
//#if !defined(MAX)//与上边正好相反  等同于 #ifndef MAX
//	printf("666");
//#endif
//
//	//嵌套指令
//#if defined(OS_UNIX)
//#ifdef OPTION1
//	unix_version_option1();
//#endif
//#ifdef OPTION2
//	unix_version_option2();
//#endif
//#elif defined(OS_MSDOS)
//#ifdef OPTION2
//	msdos_version_option2();
//#endif
//#endif
//
//	return 0;
//}

//本地⽂件包含：（自己写的头文件）
//#include "filename"
//查找策略：先在源⽂件所在⽬录下查找，如果该头⽂件未找到，编译器就像查找库函数头⽂件⼀样在标准位置查找头⽂件。
//如果找不到就提⽰编译错误
//库⽂件包含（只会查找一次）
//#include <filename.h>
//查找头⽂件直接去标准路径下去查找，如果找不到就提⽰编译错误。
//这样是不是可以说，对于库⽂件也可以使⽤ " " 的形式包含？
//答案是肯定的，可以，但是这样做查找的效率就低些，当然这样也不容易区分是库⽂件还是本地⽂件了。

// 嵌套⽂件包含
//我们已经知道， #include 指令可以使另外⼀个⽂件被编译。就像它实际出现于 #include 指令的地⽅⼀样。
//这种替换的⽅式很简单：预处理器先删除这条指令，并⽤包含⽂件的内容替换。
//⼀个头⽂件被包含10次，那就实际被编译10次，如果重复包含，对编译的压⼒就⽐较⼤。
//如何解决头⽂件被重复引⼊的问题？答案：条件编译。
//每个头⽂件的开头写：
//#ifndef __TEST_H__
//#define __TEST_H__
////头⽂件的内容
//#endif //__TEST_H__
//或者
//#pragma once

//模拟实现offsetof宏（计算结构体成员偏移量）
//struct S
//{
//	char c1;
//	int i;
//	char c2;
//};
//#define MYoffsetof(type,mem) (size_t)&(((type*)0)->mem)//假设起始地址是0，找到成员地址，该地址即为成员偏移量
//int main()
//{
//	printf("%zd\n", (MYoffsetof(struct S, c1)));
//	printf("%zd\n", (MYoffsetof(struct S, i)));
//	printf("%zd\n", (MYoffsetof(struct S, c2)));
//	return 0;
//}

//C99后和C++中，引入了一个概念：内联函数inline
//内联函数：具有了函数的特点，也有和宏一样的在程序运行时展开。

#define N 4

#define Y(n) ((N+2)*n)
int main()
{
	int z = 0;
	z = 2 * (N + Y(5 + 1));
	printf("%d\n", z);
	return 0;
}