#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>

struct node;
typedef struct node* Pnode;
struct node {
	int info;
	Pnode link;
};
typedef struct node* llist;
typedef llist* pllist;
void init(pllist p, int n)//初始化链表
{
	int i;
	Pnode q, r;
	q = (Pnode)malloc(sizeof(struct node));
	*p = q;
	q->info = 1;
	q->link = q;
	for (i = 2; i < n + 1; i++)
	{
		r = (Pnode)malloc(sizeof(struct node));
		r->info = i;
		r->link = q->link;
		q->link = r;
		q = r;
	}
}
void print(pllist p)//打印链表
{
	Pnode p1,pre;
	p1 = *p;
	printf("%d ", p1->info);
	pre = p1;
	p1 = p1->link;
	while (p1 != *p)
	{
		printf("%d ", p1->info);
		p1 = p1->link;
	}
	printf("\n");
}
void josephus(pllist p, int s, int m)
{
	Pnode pre, p1;
	int i = 0;
	p1 = *p;
	//找到第s个元素
	if (s == 1)
	{
		pre = p1;
		p1 = p1->link;
		while (p1 != *p)
		{
			pre = p1;
			p1 = p1->link;
		}
	}
	else
	{
		for (i = 1; i < s; i++)
		{
			pre = p1;
			p1 = p1->link;
		}
	}

	//向后数m个结点
	while (p1 != p1->link)
	{
		for (i = 1; i < m; i++)
		{
			pre = p1;
			p1 = p1->link;
		}
		printf("kill the %d\n", p1->info);
		if (*p == p1)
		{
			*p = p1->link;
		}
		pre->link = p1->link;
		free(p1);
		p1 = pre->link;
	}
	printf("kill the %d\n", p1->info);
	*p = NULL;
	free(p1);
}
int main()
{
	llist jos;
	int n, s, m;
	printf("请输入n的值：");
	scanf("%d", &n);
	printf("请输入s的值：");
	scanf("%d", &s);
	printf("请输入m的值：");
	scanf("%d", &m);
	init(&jos, n);
	printf("原始链表为：");
	print(&jos);
	josephus(&jos, s, m);
	return 0;
}