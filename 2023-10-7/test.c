#include"SeqList.h"

void SLtest() 
{
	SL sl;
	//初始化
	SLInit(&sl);
	//SLPopBack(&sl);//空表时不能删除，会触发assert(!SLIsEmpty(ps))
	//顺序表的具体操作
	SLPushBack(&sl, 1);
	SLPushBack(&sl, 2);
	SLPushBack(&sl, 3);
	SLPushBack(&sl, 4);
	SLPrint(&sl);
	printf("\n");

	SLPushFront(&sl, 5);
	SLPushFront(&sl, 6);
	SLPushFront(&sl, 7);
	SLPrint(&sl);
	printf("\n");

	SLPopBack(&sl);
	SLPrint(&sl);
	SLPopBack(&sl);
	SLPrint(&sl);
	SLPopBack(&sl);
	SLPrint(&sl);
	printf("\n");

	SLPopFront(&sl);
	SLPrint(&sl);
	SLPopFront(&sl);
	SLPrint(&sl);
	SLPopFront(&sl);
	SLPrint(&sl);
	printf("\n");

	//
	SLInsert(&sl,1,11);
	SLPrint(&sl);
	SLInsert(&sl,2,12);
	SLPrint(&sl);

	SLErase(&sl, 1);
	SLPrint(&sl);

	if (SLFind(&sl, 11))
	{
		printf("找到了");
	}
	else if (!SLFind(&sl, 11))
	{
		printf("没找到");
	}
	//销毁
	SLDestroy(&sl);
}
int main() 
{
	SLtest();
	return 0;
}
