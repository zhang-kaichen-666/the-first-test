#include<stdio.h>
#include<stdbool.h>

typedef int SLDataType;//改成char类型或其他类型时较方便
struct SeqList
{
	SLDataType* a;
	int size;//顺序表中有效的数据个数
	int capacity;//顺序表当前的空间大小
};
typedef struct SeqList SL;

bool SLIsEmpty(SL* ps);//判空
void SLCheckCapacity(SL* ps);//检查空间容量是否足够
//对顺序表进行初始化
void SLInit(SL* ps);
//销毁
void SLDestroy(SL* ps);
//头部/尾部 插入/删除
void SLPushBack(SL* ps, SLDataType x);//尾插
void SLPushFront(SL* ps, SLDataType x);//头插
void SLPopBack(SL* ps);//尾删
void SLPopFront(SL* ps);//头删
void SLPrint(SL* ps);//打印

//在指定位置之前插入
void SLInsert(SL* ps, int pos, SLDataType x);
//删除指定位置的数据
void SLErase(SL* ps, int pos);

bool SLFind(SL* ps, SLDataType x);

