#define _CRT_SECURE_NO_WARNINGS 1
//下面程序的功能是交换变量a和b中的值。 找出其中的错误并改正之。
//#include <stdio.h>                           
//main()
//{
//    int  a, b, * p, * q, t;
//    p = &a;
//    q = &b;
//    printf("请输入变量a和b的值:");
//    scanf("%d%d", p, q);
//    t = *p;
//    *p = *q;
//    *q = t;
//    printf("交换后a和b的值：a=%d b=%d\n", a, b);
//}

//用数组实现输入10整数，将下标为偶数的元素求和。
//输入提示："input 10 numbers:"
//输入格式："%d"
//输出格式："sum=%d\n"
//#include<stdio.h>
//int main()
//{
//	int arr[10],sum = 0;
//	printf("input 10 numbers:");
//	for (int i = 0; i < 10; i++)
//	{
//		scanf("%d", &arr[i]);
//		if (i % 2 == 0)
//		{
//			sum += arr[i];
//		}
//	}
//	printf("sum=%d\n", sum);
//	return 0;
//}

//百万富翁的换钱计划
//有一天，一位百万富翁遇到一个陌生人，陌生人找他谈一个换钱的计划，
// 陌生人对百万富翁说：“我每天给你10万元，而你第一天只需给我1分钱，
// 第二天我仍给你10万元，你给我2分钱，第三天我仍给你10万元，你给我4分钱……。
// 你每天给我的钱是前一天的两倍，直到满一个月（30天）为止”，
// 百万富翁很高兴，欣然接受了这个契约。
// 请编程计算在这一个月中陌生人总计给百万富翁多少钱，百万富翁总计给陌生人多少钱。
//输入格式 : 无
//输出格式：
//输出百万富翁给陌生人的钱： "to Stranger: %.2lf yuan\n"
//输出陌生人给百万富翁的钱： "to Richman: %.2lf yuan\n"
//#include<stdio.h>
//int main()
//{
//	double tostranger = 0, torichman = 0,dailymoney = 0.01;
//	int day = 1;
//	for (day = 1; day <= 30; day++)
//	{
//		torichman += 100000;
//		tostranger += dailymoney;
//		dailymoney *= 2;
//	}
//	printf("to Stranger: %.2lf yuan\n", tostranger);
//	printf("to Richman: %.2lf yuan\n", torichman);
//	return 0;
//}

//编程实现显示用户输入的月份（不包括闰年的月份）拥有的天数。
//** 输入格式要求："%d" 提示信息："Input a month:" "The number of days is %d\n"
//* *输出格式要求："%d"
//程序的运行示例如下：
//Input a month : 13
//Input a month : 2
//The number of days is 28
//#include<stdio.h>
//int main()
//{
//	int monthday[12] = { 31,28,31,30,31,30,31,31,30,31,30,31 }, day, month;
//	end1:
//	printf("Input a month:");
//	scanf("%d", &month);
//	if (month > 12 || month <= 0)
//		goto end1;
//	day = monthday[month - 1];
//	printf("The number of days is %d\n",day);
//
//	return 0;
//}

//假设对折一张厚度为0.1mm的纸，请问要对折多少次才能使纸的厚度从地球到达月球（假设地球到月球的距离为30万km）？
//* *输出格式要求："%d"
//#include<stdio.h>
//int main()
//{
//	double thickness = 0.1 * 0.001,len = 30*10000*1000;
//	int count = 0;
//	while (thickness < len)
//	{
//		count++;
//		thickness *= 2;
//	}
//	printf("%d", count);
//	return 0;
//}

//从键盘输入某班学生某门课的成绩（已知每班人数最多不超过40人，具体人数由键盘输入，成绩为整数），试编程计算其平均分。
//** 输入格式要求："%d" 提示信息："Input n:" "Average score is %d\n" "Input score:"
//* *输出格式要求："%d"
//程序的运行示例如下：
//Input n : 3
//Input score : 80 100 60
//Average score is 80
//#include<stdio.h>
//int main()
//{
//	int n,score[40],average = 0;
//	printf("Input n:");
//	scanf("%d", &n);
//	printf("Input score:");
//	for (int i = 0; i < n; i++)
//	{
//		scanf("%d", &score[i]);
//		average += score[i];
//	}
//	average /= n;
//	printf("Average score is %d\n", average);
//	return 0;
//}

//用数组实现输入20整数，将其中数组元素为正整数求和。
//输入提示："input 20 numbers:"
//输入格式："%d"
//输出格式："sum=%d\n"
//#include<stdio.h>
//int main()
//{
//	int arr[20],sum = 0;
//	printf("input 20 numbers:");
//	for (int i = 0; i < 20; i++)
//	{
//		scanf("%d", &arr[i]);
//		if (arr[i] > 0)
//		{
//			sum += arr[i];
//		}
//	}
//	printf("sum=%d\n", sum);
//	return 0;
//}

//编程打印200~300之间所有素数。
//要求判断一个数是否是素数用函数实现，打印结果请在主函数实现。
//判断一个数是否是素数函数原型为 :
//int fun(int m);
//参数说明：
//参  数：m是要进行判断的数；
//返回值：若此数是素数返回值为1；否则返回值为0
//**** 输入提示信息格式: 无
//* ***输入数据格式要求 : 无
//* ***输出数据格式要求 : "%d\n"
//注：不能使用指针、结构体、共用体、文件、goto、枚举类型进行编程。
//#include<stdio.h>
//#include<math.h>
//int fun(int m)
//{
//	if (m == 2)
//	{
//		return 1;
//	}
//	for (int i = 2; i <= sqrt(m); i++)
//	{
//		if (m % i == 0)
//		{
//			return 0;
//		}
//	}
//	return 1;
//
//
//}
//int main()
//{
//	for (int i = 200; i <= 300; i++)
//	{
//		if (fun(i))
//		{
//			printf("%d\n",i);
//		}
//	}
//	return 0;
//}

//输入一行字符，统计其中的英文字符、数字字符、空格和其他字符的个数。
//输入提示信息："Please input a  string:"
//输入字符串用gets()
//输出提示信息和格式：
//"English character:  %d\n"
//"digit character:  %d\n"
//"space:  %d\n"
//"other character:  %d\n"
//#include<stdio.h>
//#include<ctype.h>
//#include<string.h>
//int main()
//{
//	char str[100];
//	int e=0, d=0, s=0, l=0;
//	printf("Please input a  string:");
//	gets(str);
//	for (int i = 0; i < strlen(str); i++)
//	{
//		if (isspace(str[i]))
//		{
//			s++;
//		}
//		else if (isdigit(str[i]))
//		{
//			d++;
//		}
//		else if (isalpha(str[i]))
//		{
//			e++;
//		}
//		else
//		{
//			l++;
//		}
//	}
//	printf("English character:  %d\n", e);
//	printf("digit character:  %d\n", d);
//	printf("space:  %d\n", s);
//	printf("other character:  %d\n", l);
//
//	return 0;
//}

//在主函数中从键盘输入某班学生某门课程的成绩(已知班级人数最多不超过40人，具体人数由键盘输入),
//  试编程计算其平均分，并计算出成绩低于平均分的学生的人数。
//要求：调用函数aver()，计算n名学生的平均成绩返回给主函数，然后在主函数中输出学生的平均成绩。
//函数原型：float aver(int score[], int n);
//***输入提示信息：无
//*** 输入格式：输入学生人数用"%d"，输入学生成绩用"%d"
//* **输出平均成绩提示信息和格式："Average score is %10.2f\n"
//* **输出平均分以下的学生人数提示信息和格式:"The number of students in less than average %d\n"
//注：（1）不能使用指针、结构体、共用体、文件、goto、枚举类型进行编程。
//（2）用纯C语言编程，所有变量必须在第一条可执行语句前定义。
//#include<stdio.h>
//float aver(int score[], int n)
//{
//	float sum = 0;
//	for (int i = 0; i < n; i++)
//	{
//		sum += score[i];
//	}
//	return sum / (float)n;
//}
//int main()
//{
//	int n,arr[40],count = 0;
//	float average;
//	scanf("%d", &n);
//	for (int i = 0; i < n; i++)
//	{
//		scanf("%d", &arr[i]);
//	}
//	average = aver(arr, n);
//	printf("Average score is %10.2f\n",average);
//	for (int i = 0; i < n; i++)
//	{
//		if (arr[i] < average)
//		{
//			count++;
//		}
//	}
//	printf("The number of students in less than average %d\n", count);
//	return 0;
//}

//不使用数学函数，自己编写一个函数myPOW，计算整数x的n次方，然后在主函数中调用myPOW。
//要求必须用函数编程，否则不给分。
//函数原型为double myPOW(int x, int n);
//输入提示信息："Enter x and n\n"
//输入格式："%d%d"
//输出格式："mypow(%d,%d) = %.2f\n"
//运行示例：
//Enter x and n
//2 8↙
//mypow(2, 8) = 256.00
//注：
//各函数中的变量声明写在所有可执行语句之前。
//不能使用指针、结构体、共用体、文件、goto、枚举类型进行编程。
//#include<stdio.h>
//double myPOW(int x, int n)
//{
//	double tmp = x;
//	while (--n)
//	{
//		tmp *= x;
//	}
//	return tmp;
//}
//int main()
//{
//	int x,n;
//	printf("Enter x and n\n");
//	scanf("%d%d", &x, &n);
//	printf("mypow(%d,%d) = %.2f\n",x,n, myPOW(x, n));
//	return 0;
//}

//爱因斯坦数学题。爱因斯坦曾出过这样一道数学题：
//有一条长阶梯，若每步跨2阶，则最后剩下1阶，
//若每步跨3阶，则最后剩下2阶，
//若每步跨5阶，则最后剩下4阶，
//若每步跨6阶，则最后剩下5阶，
//只有每步跨7阶，最后才正好1阶不剩。
//请问，这条阶梯共有多少阶？
//#include <stdio.h>
//int main()
//{
//    int x=0, find=0;
//    while (!find)
//    {
//        if (x % 2 == 1 && x % 3 == 2 && x % 5 == 4 && x % 6 == 5 && x % 7 == 0)
//        {
//            printf("x=%d\n",x); 
//                find = 1;
//        }
//        x++;
//    }
//    return 0;
//}

//编程计算分段函数：
//当x < 0时，y = 2x + 1
//	当x = 0时，y = x
//	当x>0时，y = 1 / x
//	从键盘输入一个实数x，打印输出y值。
//	* ***输入提示信息要求: "Please input x:\n"
//	* ***输入数据格式要求 : "%f"
//	* ***输出数据格式要求 : "y=%f"
//#include<stdio.h>
//int main()
//{
//	float x, y;
//	printf("Please input x:\n");
//	scanf("%f", &x);
//	if (x < 0)
//	{
//		y = 2 * x + 1;
//	}
//	else if (x == 0)
//	{
//		y = x;
//	}
//	else
//	{
//		y = 1 / x;
//	}
//	printf("y=%f", y);
//	return 0;
//}

//已知三角形的三边长为a，b，c，计算三角形面积的公式为：
//area = sqrt(s(s - a)(s - b)(s - c)), s = 1 / 2(a + b + c)
//其中，a，b，c为浮点数，sqrt为开平方计算。
//要求编写程序，从键盘输入a，b，c的值，计算并输出三角形的面积。
//注意：不用判断输入的边长值是否满足三角形要求，
//只需进行简单计算即可。
//* *要求输入提示信息格式为："Input a,b,c:"；
//* *要求输入数据格式："%f,%f,%f"
//* *输出格式要求："area=%.2f\n"
//#include<stdio.h>
//#include<math.h>
//int main()
//{
//	float a, b, c,area,s;
//	printf("Input a,b,c:");
//	scanf("%f,%f,%f", &a, &b, &c);
//	s = 0.5 * (a + b + c);
//	area = sqrt(s * (s - a) * (s - b) * (s - c));
//	printf("area=%.2f\n",area);
//	return 0;
//}

//一个n位正整数如果等于它的n个数字的n次方和，该数称为n位自方幂数。四位自方幂数称为玫瑰花数。求所有的四位玫瑰花数。
//** 输出格式要求："四位玫瑰花数有:" "%d\t"
//#include<stdio.h>
//#include<math.h>
//int is(int x)
//{
//	int x1 = x % 10;
//	int x2 = x / 10 % 10;
//	int x3 = x / 100 % 10;
//	int x4 = x / 1000 % 10;
//	if (pow(x1, 4) + pow(x2, 4) + pow(x3, 4) + pow(x4, 4) == x)
//	{
//		return 1;
//	}
//	return 0;
//}
//int main()
//{
//	printf("四位玫瑰花数有:");
//	for (int i = 1000; i <= 9999; i++)
//	{
//		if (is(i))
//		{
//			printf("%d\t", i);
//		}
//	}
//	return 0;
//}

//给定一维数组a，求a中各元素的平均值。
//a[10] = { 1.0,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9 }
//**输出格式要求："The average of array is %f\n"
//#include<stdio.h>
//int main()
//{
//	float a[10] = { 1.0,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9 };
//	float sum = 0,average;
//	for (int i = 0; i < 10; i++)
//	{
//		sum += a[i];
//	}
//	average = sum / 10;
//	printf("The average of array is %f\n", average);
//	return 0;
//}

//编程实现以下图形打印。要打印的图形行数（n）从键盘读入。
//*** 输入提示信息** ："Input n:\n"
//* **输入数据格式 * *："%d"
//* **输出数据格式 * *：
//若输入的n值为4，则图形打印结果为：
//*
//***
//*****
//*******
//若输入的n值为5，则图形打印结果为：
//*
//***
//*****
//*******
//*********
//#include<stdio.h>
//int main()
//{
//	int n;
//	printf("Input n:\n");
//	scanf("%d", &n);
//	for (int i = 1; i <= n; i++)
//	{
//		for (int j = 0; j < 2 * i - 1; j++)
//		{
//			printf("*");
//		}
//		printf("\n");
//	}
//	return 0;
//}

//打印输出如下图所示的下三角形乘法九九表
//1
//2	4
//3	6	9
//4	8	12	16
//5	10	15	20	25
//6	12	18	24	30	36
//7	14	21	28	35	42	49
//8	16	24	32	40	48	56	64
//9	18	27	36	45	54	63	72	81
//* ***输入提示信息格式: "Input n:\n"
//* ***输入数据格式要求 : "%d"
//* ***输出数据格式要求 : "%4d"
//注：
//1)输入的n值用来控制需要打印的行数；
//2）若输入4，则打印结果为：
//1
//2	4
//3	6	9
//4	8	12	16
//#include<stdio.h>
//int main()
//{
//	int n;
//	printf("Input n:\n");
//	scanf("%d", &n);
//	for (int i = 1; i <= n; i++)
//	{
//		for (int j = 1; j <= i; j++)
//		{
//			printf("%4d", i * j);
//		}
//		printf("\n");
//	}
//	return 0;
//}

//输入一个三位正整数，判断其是否为水仙花数。例如：例如：1 ^ 3 + 5 ^ 3 + 3 ^ 3 = 153
//输入格式：  "%d"
//假设输入的数必为三位整数，若输入的为负数，则输出:"Input error"
//如是水仙花数则输出：  "Yes", 否则输出 "No"
//#include<stdio.h>
//#include<math.h>
//int is(int x)
//{
//	int x1 = x % 10;
//	int x2 = x / 10 % 10;
//	int x3 = x / 100 % 10;
//	if (pow(x1, 3) + pow(x2, 3) + pow(x3, 3) == x)
//	{
//		return 1;
//	}
//	return 0;
//}
//int main()
//{
//	int x;
//	scanf("%d", &x);
//	if (x < 0)
//	{
//		printf("Input error");
//		return 0;
//	}
//	if (is(x))
//	{
//		printf("Yes");
//	}
//	else
//	{
//		printf("No");
//	}
//	return 0;
//}
//

//编程打印以下图案。
//******
// ******
//  ******
//   ******
//#include<stdio.h>
//int main()
//{
//	for (int i = 0; i < 4; i++)
//	{
//		for (int j = 1; j <= i; j++)
//		{
//			printf(" ");
//		}
//		for (int j = 0; j < 6; j++)
//		{
//			printf("*");
//		}
//		printf("\n");
//	}
//	return 0;
//}

//从键盘输入一个字符串a，将字符串a复制到字符串b中，
// 再输出字符串b，即编程实现字符串处理函数strcpy()的功能，
// 但要求不能使用字符串处理函数strcpy()。
//** 输入提示信息："Input a string:"
//* *输入格式要求：用gets()输入字符串
//* *输出格式要求："The copy is:"
//程序的运行示例如下：
//Input a string : Hello China
//The copy is : Hello China
//#include<stdio.h>
//void mystrcpy(char* a, char* b)
//{
//	while (*a != '\0')
//	{
//		*b++ = *a++;
//	}
//	*b = '\0';
//}
//int main()
//{
//	char a[100], b[100];
//	printf("Input a string:");
//	gets(a);
//	mystrcpy(a, b);
//	printf("The copy is:%s",b);
//	return 0;
//}

//用字符指针作函数参数编程实现字符串逆序存放功能。
//输入提示信息："Input a string:"
//输入字符串用gets()
//输出提示信息："Inversed results:%s\n"
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char str[100];
//	int start = 0, tail;
//	printf("Input a string:");
//	gets(str);
//	tail = strlen(str) - 1;
//	while (start < tail)
//	{
//		char tmp = str[start];
//		str[start++] = str[tail];
//		str[tail--] = tmp;
//	}
//	printf("Inversed results:%s\n", str);
//	return 0;
//}

//求输入两个数的和、差、积、商和余数（如果是浮点数，则需将其强转为整型后计算余数）。请改正程序中的错误，使它能得出正确的结果。
//#include <stdio.h>
//
//main()
//{
//    float a, b;
//    float sum, minus, product, quotient;
//    int remainder;
//
//    printf("\n请输入两个数:\n");
//    scanf("%f %f", &a, &b);
//    sum = a + b;
//    minus = a - b;
//    product = a * b;
//    quotient = a / b;
//    remainder = (int)a % (int)b;
//    printf("和为:%.2f\n", sum);
//    printf("差为:%.2f\n", minus);
//    printf("积为:%.2f\n", product);
//    printf("商为:%.2f\n", quotient);
//    printf("余数为:%d\n", remainder);
//}

//编程计算：1！ + 3！ + ... + （2n - 1）！。其中n值在主函数中由键盘输入，阶层计算调用fac函数，计算结果在主函数打印。fac函数原型如下：
//long fac(int n);
//其中，n为要计算的阶层，返回值为n的阶层。
//** 输入提示信息** ："Input n:"
//* *输入数据格式 * *："%d"
//* *输出数据格式 * *："sum=%ld\n"
//注:
//（1）不允许使用全局变量；
//（2）不允许使用go to 语句；
//（3）请按题目指定要求编写程序，未按题目要求编写程序，即使程序运行结果正确，人工批阅后记为0分。
//#include<stdio.h>
//long fac(int n)
//{
//	long ret = 1;
//	for (int i = 1; i <= n; i++)
//	{
//		ret *= i;
//	}
//	return ret;
//}
//int main()
//{
//	int n;
//	long sum = 0;
//	printf("Input n:");
//	scanf("%d", &n);
//	for (int i = 1; i < 2 * n; i += 2)
//	{
//		sum += fac(i);
//	}
//	printf("sum=%ld\n", sum);
//	return 0;
//}