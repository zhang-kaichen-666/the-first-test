#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<math.h>


//实现一个函数is_prime，判断一个数是不是素数。
//利用上面实现的is_prime函数，打印100到200之间的素数。
//void is_prime(int a,int b)
//{
//	int i = 0;
//	int j = 0;
//	int flag = 0;
//	for (i = a; i < b; i++)
//	{
//		for (j = 2; j < sqrt(i); j++)
//		{
//			if (i % j == 0)
//			{
//				flag = 1;
//				break;
//			}
//
//			
//		}
//		if (flag == 0)
//		{
//			printf("%d ", i);
//		}
//		flag = 0;
//	}
//}
//int main()
//{	is_prime(100, 200);
//	return 0;
//}


//实现函数判断year是不是润年。
//void is_leapyear(int year)
//{
//	if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0)
//	{
//		printf("%d是闰年", year);
//	}
//	else
//		printf("%d不是闰年", year);
//}
//int main()
//{
//	int year = 0;
//	printf("请输入一个年份：");
//	scanf("%d", &year);
//	is_leapyear(year);
//	return 0;
//}


//创建一个整形数组，完成对数组的操作
//
//实现函数init() 初始化数组为全0
//实现print()  打印数组的每个元素
//实现reverse()  函数完成数组元素的逆置。
//要求：自己设计以上函数的参数，返回值。
//void init(int arr[], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		arr[i] = 0;
//	}
//}
//void print(int arr[], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	printf("\n");
//}
//void reverse(int arr[], int sz)
//{
//	int i = 0;
//	int j = sz - 1;
//	int temp = 0;
//	while (i < j)
//	{
//		temp = arr[i];
//		arr[i] = arr[j];
//		arr[j] = temp;
//		i++;
//		j--;
//	}
//}
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	print(arr, sz);
//	reverse(arr, sz);
//	print(arr, sz);
//	init(arr, sz);
//	print(arr, sz);
//
//	return 0;
//}


//实现一个函数，打印乘法口诀表，口诀表的行数和列数自己指定
//如：输入9，输出9 * 9口诀表，输出12，输出12 * 12的乘法口诀表
//void mul_table(int a)
//{
//	int i = 0;
//	int j = 0;
//	for (i = 1; i <= a; i++)
//	{
//		for (j = 1; j <= a; j++)
//		{
//			printf("%d * %d = %d  ", j, i, i * j);
//			if (i == j)
//			{
//				printf("\n");
//				break;
//			}
//		}
//	}
//}
//int main()
//{
//	int a = 0;
//	printf("请输入口诀表的行数:");
//	scanf("%d", &a);
//	mul_table(a);
//	return 0;
//}
