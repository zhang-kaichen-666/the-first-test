#define _CRT_SECURE_NO_WARNINGS 1
//第7章实验任务4：
//任意输入一个整数m，若m不是素数，则从小到大输出其所有不包括1和自身的因子；否则输出“没有因子，是素数”的相关提示信息。
//输入提示信息："Please enter a number:"
//输入格式："%d"
//输出格式：
//有因子时："%d\n"
//无因子时："It is a prime number.No divisor!\n"
//输入的整数小于2时输出："It is not a prime number.No divisor!\n"
//#include<stdio.h>
//int main()
//{
//	int m,flag = 0;
//	printf("Please enter a number:");
//	scanf("%d", &m);
//	if (m < 2)
//	{
//		printf("It is not a prime number.No divisor!\n");
//	}
//	else
//	{
//		for (int i = 2; i < m; i++)
//		{
//			if (m % i == 0)
//			{
//				printf("%d\n", i);
//				flag = 1;
//			}
//		}
//		if (flag == 0)
//		{
//			printf("It is a prime number.No divisor!\n");
//		}
//	}
//	return 0;
//}

//计算m!+ (m + 1)!+ … + n!.
//输入提示信息："Please input m,n"
//输入格式："%d,%d"
//输出提示信息及格式："\nsum = %ld\n"
//#include<stdio.h>
//#include<math.h>
//int fact(int x)
//{
//	int ret = 1;
//	for (int i = 1; i <= x; i++)
//	{
//		ret *= i;
//	}
//	return ret;
//}
//int main()
//{
//	int m, n,sum = 0;
//	printf("Please input m,n");
//	scanf("%d,%d", &m, &n);
//	for (int i = m; i <= n; i++)
//	{
//		sum += fact(i);
//	}
//	printf("\nsum = %ld\n", sum);
//	return 0;
//}