#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>


//有一个数字矩阵，矩阵的每行从左到右是递增的，矩阵从上到下是递增的，请编写程序在这样的矩阵中查找某个数字是否存在。
//要求：时间复杂度小于O(N);
//int find(int (*p)[5],int c,int r,int key)
//{
//	int x = 0;
//	int y = 0;
//	while (x < r && y < c)
//	{
//		if (*(*(p + y) + x) == key)
//		{
//			return 1;
//		}
//		else if (*(*(p + y) + x) > key)
//		{
//			return 0;
//		}
//		else
//		{
//			if (x == r - 1)
//			{
//				x = 0;
//				y++;
//			}
//			else
//			{
//				x++;
//			}
//		}
//
//	}
//	return 0;
//}
//int main()
//{
//	int key = 0;
//	printf("请输入您要查找的值：");
//	scanf("%d", &key);
//	int arr[3][5] = { {1,2,3,4,5} ,{2,3,4,5,6},{3,4,5,6,7} };
//	if (find(arr, 3, 5, key) == 1)
//	{
//		printf("可以找到。");
//	}
//	else
//	{
//		printf("找不到。");
//	}
//	return 0;
//}


//日本某地发生了一件谋杀案，警察通过排查确定杀人凶手必为4个嫌疑犯的一个。
//以下为4个嫌疑犯的供词:
//A说：不是我。
//B说：是C。
//C说：是D。
//D说：C在胡说
//已知3个人说了真话，1个人说的是假话。
//现在请根据这些信息，写一个程序来确定到底谁是凶手。
//int main()
//{
//	char killer;
//	for (killer = 'A';killer <= 'D'; killer++)
//	{
//		if ((killer != 'A') + (killer == 'C') + (killer == 'D') + (killer != 'D') == 3)
//		{
//			printf("%c是凶手。\n", killer);
//		}
//	}
//	return 0;
//}


//打印杨辉三角
//1
//1 1
//1 2 1
//1 3 3 1
//……
//int main()
//{
//	int line = 0;
//	printf("请输入想要输出的行数:");
//	scanf("%d", &line);
//	int arr[100][100];
//	int i = 0;
//	int j = 0;
//	for (i = 0; i < line; i++)
//	{
//		for (j = 0; j <= i; j++)
//		{
//			if (j == 0 || i == j)
//			{
//				arr[i][j] = 1;
//			}
//			else
//			{
//				arr[i][j] = arr[i - 1][j - 1] + arr[i - 1][j];
//			}
//		}
//	}
//	int z = 0;
//	
//	for (i = 0; i < line; i++)
//	{
//		int flag = 1;
//		for (j = 0; j <= i; j++)
//		{
//			while (flag == 1)
//			{
//				for (z = 0; z < line - 1 - i; z++)
//				{
//					printf(" ");
//				}
//				flag = 0;
//			}
//			printf("%d ", arr[i][j]);
//		}
//		printf("\n");
//	}
//	return 0;
//}