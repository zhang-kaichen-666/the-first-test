#include<stdio.h>
#include<assert.h>

//模拟实现strncat
//char * my_strncat(char* dest, const char* src, size_t num)
//{
//	char* ret = dest;
//	while (*dest)
//	{
//		dest++;
//	}
//	while (num)
//	{
//		num--;
//		*dest++ = *src++;
//	}
//	return ret;
//}
//int main()
//{
//	char str1[20] = "abc";
//	char str2[] = "def";
//	my_strncat(str1, str2, 2);
//	printf("%s\n", str1);
//	return 0;
//}

//模拟实现strncpy
//char* my_strncpy(char* dest, const char* src, size_t num)
//{
//	assert(dest && src);
//	char* ret = dest;
//	while (num)
//	{
//		*dest++ = *src++;
//		num--;
//	}
//	return ret;
//
//}
//int main()
//{
//	char arr1[10] = "xxxxxxxx";
//	char arr2[] = "abc";
//	my_strncpy(arr1, arr2, 5);
//	printf("%s\n", arr1);
//	return 0;
//}

//模拟实现strcmp函数
//int my_strcmp(const char* str1, const char* str2)
//{
//	assert(str1 && str2);
//	while (*str1 == *str2)
//	{
//		if (*str1 == '\0')
//		{
//			return 0;
//		}
//		str1++;
//		str2++;
//	}
//	return *str1 - *str2;
//}
//int main()
//{
//	int ret = my_strcmp("abcdef", "abc");
//	printf("%d\n", ret);
//	return 0;
//}