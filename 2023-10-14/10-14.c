#define _CRT_SECURE_NO_WARNINGS 1
//请按给定的函数原型编程实现将字符数组中的字符串的第m个字符开始的n个字符逆序存放。
//要求在主函数读入字符串，且逆序存放后的字符串也在主函数打印。函数原型：
//void  inverse(char  str[], int m, int n);
//输入要求: 输入的一行字符串，应包含字母，数字以及空格字符
//友情提示 :
//在执行输入字符串的函数之前, 请用getchar(); 把输入缓冲区中的换行符读出!!!
//****输入提示信息和格式要求为：
//"input m,n:"
//"%d,%d"
//"input the string:"
//* ***输出格式为:"the inverse string:%s"
//m = 4,n = 3
//hello worl
//hel olworl
//0123456789
#include<stdio.h>
#include<stdlib.h>
void  inverse(char  str[], int m, int n)
{
	int left = m - 1;
	int right = m + n - 2;
	while (left < right)
	{
		char tmp = str[left];
		str[left] = str[right];
		str[right] = tmp;
		left++;
		right--;
	}

}
int main()
{
	char str[100];
	int m = 0;
	int n = 0;
	printf("input m,n:");
	scanf("%d,%d", &m, &n);
	getchar();
	printf("input the string:");
	gets(str);
	inverse(str, m, n);
	printf("the inverse string:%s",str);
	return 0;
}