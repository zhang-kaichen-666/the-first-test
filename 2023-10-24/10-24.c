#define _CRT_SECURE_NO_WARNINGS 1
//编程统计候选人的得票数。设有3个候选人zhang、li、wang（候选人姓名不区分大小写），
//10个选民，选民每次输入一个得票的候选人的名字，
//若选民输错候选人姓名，则按废票处理。
//选民投票结束后程序自动显示各候选人的得票结果和废票信息。
//要求用结构体数组candidate表示3个候选人的姓名和得票结果。
//例如：
//Input vote 1:li
//Input vote 2 : li
//Input vote 3 : Zhang
//Input vote 4 : wang
//Input vote 5 : zhang
//Input vote 6 : Wang
//Input vote 7 : Zhang
//Input vote 8 : wan
//Input vote 9 : li
//Input vote 10 : lii
//Election results :
//li:3
//zhang : 3
//wang : 2
//Wrong election : 2
//输入格式：
//"Input vote %d:"
//"%s"
//输出格式：
//"Election results:\n"
//"%8s:%d\n"
//"Wrong election:%d\n"
//#include<stdio.h>
//#include<string.h>
//#include<ctype.h>
//#include<stdlib.h>
//struct candidate
//{
//	char* str1;
//	char* str2;
//	char* str3;
//	int count1;
//	int count2;
//	int count3;
//	int Wrong_election;
//};
//void belower(char* str)
//{
//	for (int i = 0; i < strlen(str); i++)
//	{
//		if (isupper(str[i]))
//		{
//			str[i] += 32;
//		}
//	}
//}
//int main()
//{
//	struct candidate* pcandidate = (struct candidate*)malloc(sizeof(struct candidate));
//	pcandidate->count1 = 0;
//	pcandidate->count2 = 0;
//	pcandidate->count3 = 0;
//	pcandidate->Wrong_election = 0;
//	pcandidate->str1 = "zhang";
//	pcandidate->str2 = "li";
//	pcandidate->str3 = "wang";
//
//	char str[10][5];
//	for (int i = 0; i < 10; i++)
//	{
//		printf("Input vote %d:", i + 1);
//		scanf("%s", str[i]);
//		belower(str[i]);
//		if (strcmp(str[i], pcandidate->str1) == 0)
//		{
//			pcandidate->count1++;
//		}
//		else if (strcmp(str[i], pcandidate->str2) == 0)
//		{
//			pcandidate->count2++;
//		}
//		else if (strcmp(str[i], pcandidate->str3) == 0)
//		{
//			pcandidate->count3++;
//		}
//		else
//		{
//			pcandidate->Wrong_election++;
//		}
//	}
//	printf("Election results:\n");
//	printf("%8s:%d\n", pcandidate->str2, pcandidate->count2);
//	printf("%8s:%d\n", pcandidate->str1, pcandidate->count1);
//	printf("%8s:%d\n", pcandidate->str3, pcandidate->count3);
//	printf("Wrong election:%d\n", pcandidate->Wrong_election);
//	return 0;
//}
//li li Li wang zhang Wang Zhang wang li lii

//搬砖问题。36块砖，36人搬，男搬4，女搬3，两个小孩抬一块砖，要求一次搬完，
// 问男人、女人和小孩各需多少人？请用穷举法编程求解。
//*** 输入提示信息*** ：无
//*** 输入数据格式*** ：无
//*** 输出数据格式*** ："men=%d,women=%d,children=%d\n"
//注：不允许使用goto语句
//#include<stdio.h>
//int main()
//{
//	int men = 0;
//	int women = 0;
//	int children = 0;
//	while (men < 9)
//	{
//		while (women < 12)
//		{
//			while (children < 72)
//			{
//				if (men * 4 + women * 3 + children * 0.5 == 36 && men + women + children == 36)
//				{
//					printf("men=%d,women=%d,children=%d\n", men, women, children);
//				}
//				children++;
//			}
//			children = 0;
//			women++;
//		}
//		women = 0;
//		men++;
//	}
//	return 0;
//}

//小华有5本新书，要借给A、B、C本位同学，
//每人只能借一本书，
//编程输出小华有多少种不同的借书方法。
//（提示，可将小华五本书编号为1，2，3，4，5）
//输出格式要求为：printf("%d,%d,%d\n", );
//#include<stdio.h>
//int main()
//{
//	for (int a = 1; a <= 5; a++)
//	{
//		for (int b = 1; b <= 5; b++)
//		{
//			for (int c = 1; c <= 5; c++)
//			{
//				if (a != b && a != c && b != c)
//				{
//					printf("%d,%d,%d\n", a, b, c);
//				}
//			}
//		}
//	}
//	return 0;
//}

//编写程序计算浮点数的绝对值。
//要求从键盘输入任意的浮点数，然后计算并输出其绝对值。
//注意：不允许使用math.h中的标准库函数进行编程。
//*** 输入数据格式*** ："%f"
//* **输出数据格式 * **："%f"
//#include<stdio.h>
//int main()
//{
//	float x = 0;
//	printf("Please enter the number:");
//	scanf("%f", &x);
//	if (x < 0)
//		x = -x;
//	printf("%f", x);
//	return 0;
//}