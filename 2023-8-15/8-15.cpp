#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>

//将课堂上所讲使用函数指针数组实现转移表的代码，自己实践后，并提交代码到答案窗口。
int Add(int x, int y)
{
	return x + y;
}
int Sub(int x, int y)
{
	return x - y;
}
int Mul(int x, int y)
{
	return x * y;
}
int Div(int x, int y)
{
	return x / y;
}
int main(void)
{
	int x = 0;
	int y = 0;
	int input = 0;
	int ret = 0;
	int (*p[5])(int x, int y) = { 0,Add,Sub,Mul,Div };
	do
	{
		printf("*************************\n");
		printf(" 1:add             2:sub \n");
		printf(" 3:mul             4:div \n");
		printf(" 0:exit                  \n");
		printf("*************************\n");
		printf("请选择：");
		scanf("%d", &input);
		if (input >= 1 && input <= 4)
		{
			printf("请输入两个操作数；");
			scanf("%d%d", &x, &y);
			ret = (*p[input])(x, y);
			printf("结果是：%d\n", ret);
		}
		else if (input == 0)
		{
			printf("退出计算器。\n");
		}
		else
		{
			printf("输入错误，请重新输入。\n");
		}

	} while (input);
	return 0;
}


//练习使用库函数，qsort排序各种类型的数据
//int cmp(const void* a,const void* b)
//{
//	return *(int*)a - *(int*)b;
//}
//void print(int* arr, int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", *(arr + i));
//	}
//}
//int main()
//{
//	int arr[10] = { 5,4,6,7,8,9,1,2,3,10 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	qsort(arr, sz, sizeof(int), cmp);
//	print(arr, sz);
//	return 0;
//}


//一个数组中只有两个数字是出现一次，其他所有数字都出现了两次。
//编写一个函数找出这两个只出现一次的数字。
//例如：
//有数组的元素是：1，2，3，4，5，1，2，3，4，6
//只有5和6只出现1次，要找出5和6。
//void single_find(int* arr, size_t sz)
//{
//	int i = 0;
//	int tmp = 0;
//	for (i = 0; i < sz; i++)
//	{
//		tmp ^= arr[i];
//	}
//	int flag = 0;
//	while (((tmp >> flag) & 1) == 0)
//	{
//		flag++;
//	}
//	int ret_1 = 0;
//	int ret_2 = 0;
//	for (i = 0; i < sz; i++)
//	{
//		if (((arr[i] >> flag) & 1) == 1)
//		{
//			ret_1 ^= arr[i];
//		}
//		else
//		{
//			ret_2 ^= arr[i];
//		}
//	}
//	printf("两个数分别是：%d和%d\n", ret_1, ret_2);
//}
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,1,2,3,4,6 };
//	size_t sz = sizeof(arr) / sizeof(arr[0]);
//	single_find(arr, sz);
//	return 0;
//}


//KiKi想获得某年某月有多少天，请帮他编程实现。输入年份和月份，计算这一年这个月有多少天。
//int main()
//{
//    int arr[12] = { 31,28,31,30,31,30,31,31,30,31,30,31 };
//    int year = 0;
//    int month = 0;
//    while (scanf("%d %d", &year, &month) != EOF)
//    {
//        if ((((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)) && month == 2)
//        {
//            printf("%d\n", arr[month - 1] + 1);
//        }
//        else {
//            printf("%d\n", arr[month - 1]);
//        }
//    }
//    return 0;
//}