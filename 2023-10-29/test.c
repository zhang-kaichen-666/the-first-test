#include"SeqList.h"

test()
{
	SL sl;
	SLInit(&sl);

	SLPushBack(&sl, 1);
	SLPushBack(&sl, 2);
	SLPushBack(&sl, 3);
	SLPushBack(&sl, 4);
	SLPushBack(&sl, 5);
	SLPrint(&sl);//1 2 3 4 5

	SLPushFront(&sl, 6);
	SLPushFront(&sl, 7);
	SLPushFront(&sl, 8);
	SLPrint(&sl);//8 7 6 1 2 3 4 5

	SLPopBack(&sl);
	SLPopBack(&sl);
	SLPrint(&sl);//8 7 6 1 2 3

	SLPopFront(&sl);
	SLPopFront(&sl);
	SLPrint(&sl);//6 1 2 3

	SLInsert(&sl, 2, 4);
	SLPrint(&sl);//6 1 4 2 3

	SLErase(&sl,1);
	SLPrint(&sl);//6 4 2 3

	int pos = SLFind(&sl, 2);
	SLErase(&sl, pos);
	SLPrint(&sl);//6 4 3

	SLDestroy(&sl);

}
int main()
{
	test();
	return 0;
}