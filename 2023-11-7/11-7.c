#define _CRT_SECURE_NO_WARNINGS 1
//输出如下图所示的下三角形乘法九九表。
//1	2	3	4	5	6	7	8	9
//------------------------------------------------------------------
//1
//2	4
//3	6	9
//4	8	12	16
//…
//9	18	27	36	45	54	63	72	81
//* *输出格式要求："\n-----------------------------\n"  "%4d"
//程序运行示例如下：
//1   2   3   4   5   6   7   8   9
//---------------------------- -
//1
//2   4
//3   6   9
//4   8  12  16
//5  10  15  20  25
//6  12  18  24  30  36
//7  14  21  28  35  42  49
//8  16  24  32  40  48  56  64
//9  18  27  36  45  54  63  72  81
//#include<stdio.h>
//int main()
//{
//	printf("   1   2   3   4   5   6   7   8   9");
//	printf("\n-----------------------------\n");
//	int i = 1, j = 1;
//	for (i = 1; i < 10; i++)
//	{
//		for (j = 1; j <= i; j++)
//		{
//			printf("%4d", i * j);
//		}
//		printf("\n");
//	}
//	return 0;
//}

//从键盘输入任意10个整数存入数组中，求出其中最大值和最小值的和,
//并输出这个和。
//** 要求输入提示信息为：无输入提示信息
//** 要求输入格式为："%d"
//* *要求输出格式为："sum=%3d"
//#include<stdio.h>
//int main()
//{
//	int arr[10];
//	int max ,min;
//	for (int i = 0; i < 10; i++)
//	{
//		scanf("%d", &arr[i]);
//		if (i == 0)
//		{
//			max = min = arr[i];
//		}
//		if (max < arr[i])
//		{
//			max = arr[i];
//		}
//		if (min > arr[i])
//		{
//			min = arr[i];
//		}
//	}
//	printf("sum=%3d", max + min);
//	return 0;
//}

//编写程序实现以下功能：从键盘输入一个大写英文字母，
// 将该字母转换成小写字母后，打印输出转换后的小写字母及其所对应的ASC码值。
//** 输入提示信息** ："Enter a capital letter:\n"
//* *输入数据格式 * *："%c"
//* *输出数据格式 * *："%c,%d\n"
//友情提示：从键盘输入一个字符可用scanf也可用getchar
//#include<stdio.h>
//int main()
//{
//	char c;
//	printf("Enter a capital letter:\n");
//	scanf("%c", &c);
//	c += 32;
//	printf("%c,%d\n", c, c);
//	return 0;
//}

//编程打印以下图案。
//******
// ******
//  ******
//   ******
//#include<stdio.h>
//int main()
//{
//	int k = 6;
//	for (int i = 0; i < 4; i++)
//	{
//		int j = i;
//		while (j--)
//		{
//			printf(" ");
//		}
//		while (k--)
//		{
//			printf("*");
//		}
//		k = 6;
//		printf("\n");
//	}
//	return 0;
//}

//编程计算: 1!+ 3!+ 5!+ ... + (2n - 1)!，要求阶乘计算调用fun函数实现，
//数据输入及打印结果在主函数实现。阶乘计算fun函数原型为 :
//long fun(int n);
//参数说明:
//参  数 : n是要进行阶乘计算的数；
//返回值 : 数n的阶乘
//* ***输入提示信息 : "Input n:\n"
//* ***输入数据格式要求 : "%d"
//* ***输出数据格式要求 : "sum=%ld\n" (格式转换说明 % ld用于输出长整型数值)
//#include<stdio.h>
//long fun(int n)
//{
//	long ret = 1;
//	for (int i = 1; i <= n; i++)
//	{
//		ret *= i;
//	}
//	return ret;
//}
//int main()
//{
//	int i = 1,n,sum = 0;
//	printf("Input n:\n");
//	scanf("%d", &n);
//	for (; i <= 2 * n - 1; i += 2)
//	{
//		sum += fun(i);
//	}
//	printf("sum=%ld\n", sum);
//	return 0;
//}

//下面程序的功能是删除字符串中第一次出现的a字符。
//#include <stdio.h>
//#include <string.h>
//void fun(char* x, int n, int*t)
//{   int i, k = 0;
//    x[n] = '\0';
//    while (x[k] != 'a') k++;
//    if (k == n) { *t = 0;}
//    else
//    {
//        for (i = k; i < n; i++)
//            x[i] = x[i + 1];
//        x[i] = '\0';
//    }
//}
//
//main()
//{
//    char x[20];
//    int len, t;
//    gets(x);
//    puts(x);
//    len = strlen(x);
//    fun(x, len, &t);
//    if (t == 0) printf("Not exist!\n");
//    else    puts(x);
//}
//运行样例1：
//what a pity!↙
//what a pity!
//wht a pity!

//程序：数列求和
//编写一个程序对用户输入的整数数列进行求和计算。
//** 输入格式要求："%d" 提示信息："This program sums a series of integers.\n" "Enter integers (0 to terminate):"
//* *输出格式要求："The sum is: %d\n"
//下面显示的是用户可见的内容：
//This program sums a series of integers.
//Enter integers(0 to terminate) : 8 23 71 5 0
//The sum is : 107
//#include<stdio.h>
//int main()
//{
//	int arr[100];
//	int i = 0,sum = 0;
//	printf("This program sums a series of integers.\n");
//	printf("Enter integers (0 to terminate):");
//	while (1)
//	{
//		scanf("%d", &arr[i]);
//		if (arr[i] == 0)
//		{
//			break;
//		}
//		sum += arr[i];
//		i++;
//	}
//	printf("The sum is: %d\n", sum);
//	return 0;
//}

//如果一个正整数等于其各个数字的立方和，则该数称为阿姆斯特朗数（亦称为自恋性数）。
//如407 = 4 ^ 3 + 0 ^ 3 + 7 ^ 3就是一个阿姆斯特朗数。试编程求1000内的所有3位数的阿姆斯特朗数。
//* *输出格式要求："There are following Armstrong number smaller than 1000:\n" " %d "
//程序运行示例如下：
//There are following Armstrong number smaller than 1000:
//153  370  371  407
//#include<stdio.h>
//#include<math.h>
//int Armnumber(int n)
//{
//	int sum = 0;
//	for (int i = 0; i < 3; i++)
//	{
//		int tmp = n / (int)pow(10, i) % 10;
//		sum += pow(tmp, 3);
//	}
//	if (sum == n)
//	{
//		return 1;
//	}
//	return 0;
//
//}
//int main()
//{
//	printf("There are following Armstrong number smaller than 1000:\n");
//	for (int i = 100; i <= 1000; i++)
//	{
//		if (Armnumber(i))
//		{
//			printf(" %d ", i);
//		}
//	}
//	return 0;
//}

//从键盘输入任意输入一个字符，编程判断该字符是数字字符、英文字符还是其他字符.
//要求输入：Please input a char:
//输出：如果是数字，则输出：It is a digit character!
//如果是英文字母，则输出：It is an English character!
//如果是其他字符，则输出：It is other character!
//(注意输出语句末尾不需要输出换行符\n)
//#include<stdio.h>
//#include<ctype.h>
//int main()
//{
//	char ch;
//	printf("Please input a char:");
//	scanf("%c", &ch);
//	if (isdigit(ch))
//	{
//		printf("It is a digit character!");
//	}
//	else if (isalpha(ch))
//	{
//		printf("It is an English character!");
//	}
//	else
//	{
//		printf("It is other character!");
//	}
//	return 0;
//}

//字符串逆序
//用字符数组作函数参数编程，利用一个数组实现字符串（允许输入带空格的字符串）的逆序存放。要求如下：
//（1）在主函数中从键盘输入字符串，字符串的最大长度为80个字符。
//调用Inverse()函数将字符串逆序存放，然后在主函数中输出逆序后的字符串。
//（2）在子函数Inverse()中实现字符串的逆序存放。函数原型为：
//void Inverse(char str[]);
//程序运行结果示例1：
//Input a string :
//abcde↙
//Inversed results :
//edcba
//程序运行结果示例2：
//Input a string :
//hello↙
//Inversed results :
//olleh
//输入格式 : 用gets()输入字符串
//输出格式：
//输入提示信息："Input a string:\n"
//输出提示信息："Inversed results:\n"
//用puts()输出字符串
//#include<stdio.h>
//#include<string.h>
//void Inverse(char str[])
//{
//	int start = 0;
//	int len = strlen(str) - 1;
//	while (start < len)
//	{
//		str[start] = str[start] ^ str[len];
//		str[len] = str[start] ^ str[len];
//		str[start] = str[start] ^ str[len];
//		start++;
//		len--;
//	}
//}
//int main()
//{
//	char str[80];
//	printf("Input a string:\n");
//	gets(str);
//	Inverse(str);
//	printf("Inversed results:\n");
//	puts(str);
//	return 0;
//}

//利用数组计算fibonacci数列的前10个数，即1, 1, 2, 3, 5, ……，并按每行打印5个数的格式输出。
//** 输出格式要求："%6d"
//#include<stdio.h>
//int fib(int n)
//{
//	if (n == 1 || n == 2)
//	{
//		return 1;
//	}
//	else
//	{
//		return fib(n - 1) + fib(n - 2);
//	}
//}
//int main()
//{
//	for (int i = 1; i <= 10; i++)
//	{
//		printf("%6d",fib(i));
//		if (i == 5)
//		{
//			printf("\n");
//		}
//	}
//	return 0;
//}

//写一个函数测试某个整数值是否落在某个范围之内。函数原型如下：
//int range_test(int val, int low, int high);
//其中val是要测试的值，low是范围的最小值，high是范围的最大值。
//如果落在指定的范围内，函数返回1，否则返回0。编写main函数调用它并进行测试。
//** 输入格式要求："%d%d%d" 提示信息："请输入数值、下界和上界：\n"
//* *输出格式要求："函数测试输出为%d！\n" （输出为：1或者0）
//#include<stdio.h>
//int range_test(int val, int low, int high)
//{
//	if (val >= low && val <= high)
//	{
//		return 1;
//	}
//	return 0;
//}
//int main()
//{
//	int val,low,high;
//	printf("请输入数值、下界和上界：\n");
//	scanf("%d%d%d", &val, &low, &high);
//	printf("函数测试输出为%d！\n", range_test(val, low, high));
//	return 0;
//}

//程序的Squeeze函数的功能是删除字符串s中所出现的与变量c相同的字符。
//** 输入输出格式要求：无输入输出提示信息，要求可以输入带空格的字符串；
//按原题要求，先输入一个字符串s，回车换行后再输入字符c（c为任意字符）。
//#include<stdio.h>
//#include<string.h>
//void Squeeze(char* str,char c)
//{
//	int i = 0;
//	while (str[i] != '\0')
//	{
//		if (str[i] == c)
//		{
//			int j = i;
//			while (str[j] != '\0')
//			{
//				str[j] = str[j + 1];
//				j++;
//			}
//		}
//		else
//		{
//			i++;
//		}
//	}
//}
//int main()
//{
//	char c;
//	char str[100];
//	gets(str);
//	scanf("%c", &c);
//	Squeeze(str, c);
//	printf("%s", str);
//	return 0;
//}

//编程判断输入的一个字符串是否是“回文”。所谓“回文”字符串就是左读和右读都一样的字符串。例如: "abcba"就是一个回文字符串。
//
//输入提示信息："Input a string:\n"
//输入格式：gets()
//判断是回文的输出提示信息："This string is a plalindrome."
//判断不是回文的输出提示信息："This string is not a plalindrome."
//
//程序运行示例1：
//Input a string :
//abcba↙
//This string is a plalindrome.
//
//程序运行示例2：
//Input a string :
//friend↙
//This string is not a plalindrome.
//#include<stdio.h>
//#include<string.h>
//int ispl(char* str)
//{
//	int start = 0;
//	int len = strlen(str) - 1;
//	while (start < len)
//	{
//		if (str[start] != str[len])
//		{
//			return 0;
//		}
//		start++;
//		len--;
//	}
//	return 1;
//}
//int main()
//{
//	char str[100];
//	printf("Input a string:\n");
//	gets(str);
//	if (ispl(str))
//	{
//		printf("This string is a plalindrome.");
//	}
//	else
//	{
//		printf("This string is not a plalindrome.");
//	}
//	return 0;
//}

//编程输入一个整数m（int型），输出该整数的位数n。
//** 输入格式要求："%d"
//* *输出格式要求："n=%d"
//程序运行示例1如下：
//345
//n = 3
//程序运行示例2如下：
//0
//n = 1
//#include<stdio.h>
//int main()
//{
//	int a,count = 0;
//	scanf("%d", &a);
//	if (a == 0)
//	{
//		count = 1;
//	}
//	while (a)
//	{
//		a /= 10;
//		count++;
//	}
//	printf("n=%d", count);
//	return 0;
//}