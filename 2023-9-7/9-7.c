﻿#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
#include<assert.h>

//strcmp函数:用来比较两个字符串大小(按字典序比较)(按位依次比较)
//前<后，返回小于零的数
//前>后，返回大于零的数
//前==后,返回0
//int main()
//{
//	int ret = strcmp("abcdef", "abq");
//	printf("%d\n", ret);//返回-1
//	return 0;
//}

//模拟实现strcmp函数
//int my_strcmp(const char* s1, const char* s2)
//{
//	assert(s1 != NULL);
//	assert(s2 != NULL);
//	while (*s1 == *s2)
//	{
//		if (*s1 == '\0')
//		{
//			return 0;
//		}
//		s1++;
//		s2++;
//	}
//	//1.if (*s1 > *s2)
//	//	  return 1;
//	//  else
//	//	  return -1;
//	//2.return *s1 - *s2;
//}
//int main()
//{
//	int ret = my_strcmp("abcdef", "abc");
//	printf("%d\n", ret);
//	return 0;
//}

//strncpy函数   strncat函数和strncmp函数同理
//int main()
//{
//	char arr1[10] = "xxxxxxxx";
//	char arr2[] = "ab";
//	strncpy(arr1, arr2, 5);
//	printf("%s\n", arr1);
//	return 0;
//}

//strtok函数:返回类型为char*   char * strtok(char *str,const char * sep)
//strtok函数找到str中的下⼀个标记，并将其⽤ \0 结尾，返回⼀个指向这个标记的指针。
//注意:strtok函数会改变被操作的字符串，所以在使⽤strtok函数切分的字符串⼀般都是临时拷⻉的内容并且可修改。
//strtok函数的第⼀个参数不为 NULL ，函数将找到str中第⼀个标记，strtok函数将保存它在字符串中的位置。
//strtok函数的第⼀个参数为 NULL ，函数将在同⼀个字符串中被保存的位置开始，查找下⼀个标记。
//如果字符串中不存在更多的标记，则返回 NULL 指针。
//int main()
//{
//	char arr[] = "zpengwei@yeah.net";//@ 和.称为分隔符
//	char buf[30] = { 0 };
//	strcpy(buf, arr);
//	char* p = "@.";
//	char * s = strtok(buf, p);
//	printf("%s\n", s);//打印出来的是zpengwei
//
//	s = strtok(NULL, p);
//	printf("%s\n", s);//yeah
//
//	s = strtok(NULL, p);
//	printf("%s\n", s);//net
//
//	return 0;
//
// }
//以上只是举例子，下面是正常写法
//int main()
//{
//	char arr[] = "zpengwei@yeah.net@xxxx.yyyy.zzzz";
//	char buf[60] = { 0 };
//	strcpy(buf, arr);
//	char* p = "@.";
//	char* r = NULL;
//	for (r = strtok(buf, p); r != NULL; r = strtok(NULL, p))
//	{
//		printf("%s\n", r);
//	}
//	return 0;
//}

//strstr函数:在字符串中查找另一个字符串，返回str2在str1中第一次出现的位置，如果没有出现，返回空指针
//int main()
//{
//	char arr1[] = "abcdefabcdef";
//	char arr2[] = "def";
//	char * ret = strstr(arr1, arr2);
//	if (ret != NULL)
//	{
//		printf("%s\n", ret);//打印出defabcdef
//	}
//	else
//		printf("找不到\n");
//	return 0;
//}

//模拟实现strstr函数
//const char* my_strstr(const char* str1, const char* str2)
//{
//	assert(str1);
//	assert(str2);
//	const char* cp = str1;
//	const char* s1 = str1;
//	const char* s2 = str2;
//	if (*str2 == '\0')//如果子串是空字符串，直接返回str1
//		return str1;
//	while (*cp)
//	{
//		s1 = cp;
//		s2 = str2;
//		while (*s1 == *s2 && *s1 & *s2)
//		{
//			s1++;
//			s2++;
//
//		}
//		if (*s2 == '\0')
//			return cp;
//		cp++;
//	}
//	return NULL;
//}
//int main()
//{
//	char arr1[] = "abcdefabcdef";
//	char arr2[] = "def";
//	char * ret = my_strstr(arr1, arr2);
//	if (ret != NULL)
//	{
//		printf("%s\n", ret);//打印出defabcdef
//	}
//	else
//		printf("找不到\n");
//	return 0;
//}

//strerror函数:返回一个错误码所对应的错误信息字符串的起始地址      const char * strerror(int i)
//int main()
//{
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		char* ret = strerror(i);
//		printf("%d,%s\n", i, ret);
//	}
//	return 0;
//}
//当库函数调用失败时，会将错误码记录到errno这个变量中
//errno是一个c语言的全局变量

//内存函数(针对内存块):
//1.memcpy函数:内存拷贝     void * memcpy(void * destination,const void * source,size_t num)
//内存之间如果有重叠，要拷贝，要使用memmove函数
//int main()
//{
//	int arr1[10] = { 0 };
//	int arr2[] = { 1,2,3,4,5,6,7,8 };
//	memcpy(arr1, arr2, 20);
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", arr1[i]);
//	}//拷贝了20个字节，也就是5个整型
//
//	char arr3[] = "xxxxxxxxx";
//	char arr4[] = "yyyyyyyyy";
//	memcpy(arr3, arr4, 3);
//	for (i = 0; i < 10; i++)
//	{
//		printf("%c ", arr3[i]);
//	}//拷贝了3个字节，就是三个y
//	return 0;
//}

//模拟实现memcpy
//void * my_memcpy(void* dest, const void* src, size_t num)//返回目标空间的起始地址
//{
//	void* ret = dest;
//	assert(dest && src);
//	while (num--)
//	{
//		//拷贝一个字节
//		*(char*)dest = *(char*)src;
//		dest = (char*)dest + 1;
//		src = (char*)src + 1;
//	}
//}
//int main()
//{
//	int arr1[10] = { 0 };
//	int arr2[] = { 1,2,3,4,5,6,7,8 };
//	my_memcpy(arr1, arr2, 20);
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", arr1[i]);
//	}//拷贝了20个字节，也就是5个整型
// 
//	char arr3[] = "xxxxxxxxx";
//	char arr4[] = "yyyyyyyyy";
//	my_memcpy(arr3, arr4, 3);
//	for (i = 0; i < 10; i++)
//	{
//		printf("%c ", arr3[i]);
//	}//拷贝了3个字节，就是三个y
//	return 0;
//}