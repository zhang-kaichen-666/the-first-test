#include"SeqList.h"
#include<stdio.h>

seqtest()
{
	SL s1;
	SLInit(&s1);

	SLPushBack(&s1,1);
	SLPushBack(&s1,2);
	SLPushBack(&s1,5);
	SLPrint(&s1);

	SLPushFront(&s1, 3);
	SLPrint(&s1);

	SLPopBack(&s1);
	SLPrint(&s1);

	SLPopFront(&s1);
	SLPrint(&s1);

	SLInsert(&s1, 1, 4);
	SLPrint(&s1);

	SLErase(&s1, 0);
	SLPrint(&s1);

	printf("%d\n", SLFind(&s1, 2));
	SLDestroy(&s1);
}
int main()
{
	seqtest();
	return 0;
}