#define _CRT_SECURE_NO_WARNINGS 1
//某班期末考试科目为数学（MT）、英语（EN）和物理（PH），有最多不超过40人参加考试。
// 请编程计算：（1）每个学生的总分和平均分；（2）每门课程的总分和平均分。
//** 输入格式要求："%d" "%ld"(学号)提示信息：
// "Input the total number of the students(n<40):" "Input student’s ID and score as: MT  EN  PH:\n"
//* *输出格式要求："Counting Result:\n" "Student’s ID\t  MT \t  EN \t  PH \t SUM \t AVER\n" "%12ld\t"(打印学号)"%4d\t"（打印每门课成绩）"%4d\t%5.1f\n"（打印总分和平均分） "SumofCourse \t" "%4d\t"(打印每门课的总分)"\nAverofCourse\t" "%4.1f\t"(每门课的平均分)
//程序的运行示例如下：
//Input the total number of the students(n <= 40) :4
//Input student’s ID and score as : MT  EN  PH :
//070310122  97  87  92
//070310123  92  91  90
//070310124  90  81  82
//070310125  73  65  80
//Counting Result :
//Student’s ID    MT	EN	PH	SUM	AVER
//070310122	97	87	92	276	92.0
//070310123	92	91	90	273	91.0
//070310124	90	81	82	253	84.3
//070310125	73	65	80	218	72.7
//SumofCourse	352	324	344
//AverofCourse	88.0	81.0	86.0
//#include <stdio.h>
//#include <stdlib.h>
//
//#define   MAX_LEN  9                	/* 字符串最大长度 */
//#define   STU_NUM 40                       /* 最多的学生人数 */
//
//void  ReadScore(long num[], int score[][3], int n);//录入信息
//void  Course(int Coursesum[3], float Courseaver[3], int score[][3], int n);//计算课程总分与平均分
//void  Student(int Studentsum[STU_NUM], float Studentaver[STU_NUM], int score[][3], int n);//计算学生总分与平均分
//
//int main()
//{
//    int n, i, k, g, t, score[STU_NUM][3], Coursesum[3], Studentsum[STU_NUM];
//    long num[STU_NUM];
//    float Courseaver[3], Studentaver[STU_NUM];
//    printf("Input the total number of the students(n<40):");
//    scanf("%d", &n);
//    ReadScore(num, score, n);
//    Course(Coursesum, Courseaver, score, n);
//    Student(Studentsum, Studentaver, score, n);
//    printf("Counting Result:\n");
//    printf("Student’s ID\t  MT \t  EN \t  PH \t SUM \t AVER\n");
//    for (i = 0; i < n; i++)
//    {
//        printf("%12ld\t", num[i]);
//        for (k = 0; k < 3; k++)
//        {
//            printf("%4d\t", score[i][k]);
//        }
//        printf("%4d\t%5.1f\n", Studentsum[i], Studentaver[i]);
//    }
//    printf("SumofCourse \t");
//    for (t = 0; t < 3; t++)
//    {
//        printf("%4d\t", Coursesum[t]);
//    }
//    printf("\nAverofCourse\t");
//    for (g = 0; g < 3; g++)
//    {
//        printf("%4.1f\t", Courseaver[g]);
//    }
//    return 0;
//}
//
//void  ReadScore(long num[], int score[][3], int n)
//{
//    int i, k;
//    printf("Input student’s ID and score as: MT  EN  PH:\n");
//    for (i = 0; i < n; i++)
//    {
//        scanf("%ld", &num[i]);
//        getchar();
//        for (k = 0; k < 3; k++)
//        {
//            scanf("%d", &score[i][k]);
//            getchar();
//        }
//    }
//}
//
//void  Course(int Coursesum[3], float Courseaver[3], int score[][3], int n)
//{
//    int s, i, k;
//    for (k = 0; k < 3; k++)
//    {
//        s = 0;
//        for (i = 0; i < n; i++)
//        {
//            s += score[i][k];
//        }
//        Coursesum[k] = s;
//        Courseaver[k] = s / n;
//    }
//}
//
//void  Student(int Studentsum[STU_NUM], float Studentaver[STU_NUM], int score[][3], int n)
//{
//    int i, k, s;
//    for (i = 0; i < n; i++)
//    {
//        s = 0;
//        for (k = 0; k < 3; k++)
//        {
//            s += score[i][k];
//        }
//        Studentsum[i] = s;
//        Studentaver[i] = (float)s / 3;
//    }
//}

//对输入的字符串进行长度验证，保证输入的字符串的长度在指定的范围内，
//如果不在指定的范围内，则一直提示用户输入，直到输入合法长度的字符串为止。
//程序的示例运行如下：
//请输入一个字符串(长度为[3..5]个字符)：a
//请输入一个字符串(长度为[3..5]个字符)：ab
//请输入一个字符串(长度为[3..5]个字符)：abcdef
//请输入一个字符串(长度为[3..5]个字符)：abc
//你输入的字符串为：abc
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	printf("请输入一个字符串(长度为[3..5]个字符)：\n");
//	char str[100];
//	gets(str);
//	while (strlen(str) > 5 || strlen(str) < 3)
//	{
//		printf("请输入一个字符串(长度为[3..5]个字符)：\n");
//		gets(str);
//	}
//	printf("你输入的字符串为：%s", str);
//	return 0;
//}

//编写一个简单的23 根火柴游戏程序，实现人跟计算机玩这个游戏的程序。
//为了方便程序自动评测，假设计算机移动的火柴数不是随机的，而是将剩余的火柴根数减1后对4求余来计算，如果计算结果为0，则取走1根。
//游戏规则是：
//1、两个游戏者开始拥有23 根火柴棒；
//2、每个游戏者轮流移走1 根、2根或3根火柴；
//3、谁取走最后一根火柴为失败者。
//程序一次运行示例如下：
//这里是23根火柴游戏！！
//注意：最大移动火柴数目为三根
//请输入移动的火柴数目：
//3
//您移动的火柴数目为：3
//您移动后剩下的火柴数目为：20
//计算机移动的火柴数目为：3
//计算机移动后剩下的火柴数目为：17
//请输入移动的火柴数目：
//2
//您移动的火柴数目为：2
//您移动后剩下的火柴数目为：15
//计算机移动的火柴数目为：2
//计算机移动后剩下的火柴数目为：13
//请输入移动的火柴数目：
//3
//您移动的火柴数目为：3
//您移动后剩下的火柴数目为：10
//计算机移动的火柴数目为：1
//计算机移动后剩下的火柴数目为：9
//请输入移动的火柴数目：
//1
//您移动的火柴数目为：1
//您移动后剩下的火柴数目为：8
//计算机移动的火柴数目为：3
//计算机移动后剩下的火柴数目为：5
//请输入移动的火柴数目：
//2
//您移动的火柴数目为：2
//您移动后剩下的火柴数目为：3
//计算机移动的火柴数目为：2
//计算机移动后剩下的火柴数目为：1
//请输入移动的火柴数目：
//3
//您移动的火柴数目为：3
//您移动后剩下的火柴数目为： - 2
//对不起！您输了！
//
//补充说明：
//如果输入的火柴数超过3，则输出"对不起！您输入了不合适的数目，请点击任意键重新输入！\n"，如果玩家赢了，则输出"恭喜您！您赢了！ \n"
//#include<stdio.h>
//int main()
//{
//	printf("这里是23根火柴游戏！！\n");
//	printf("注意：最大移动火柴数目为三根\n");
//	int m = 0;//人输入
//	int n = 0;//计算机
//	int sum = 23;
//	while (sum > 0)
//	{
//		flag:
//		printf("请输入移动的火柴数目：\n");
//		scanf("%d", &m);
//		if (m > 3)
//		{
//			printf("对不起！您输入了不合适的数目，请点击任意键重新输入！\n");
//			getch();
//			goto flag;
//		}
//		printf("您移动的火柴数目为：%d\n", m);
//		sum -= m;
//		printf("您移动后剩下的火柴数目为：%d\n", sum);
//		if (sum <= 0)
//		{
//			printf("对不起！您输了！");
//			break;
//		}
//		n = (sum - 1) % 4;
//		if (n == 0)
//		{
//			n = 1;
//		}
//		printf("计算机移动的火柴数目为：%d\n", n);
//		sum -= n;
//		printf("计算机移动后剩下的火柴数目为：%d\n", sum);
//		if (sum <= 0)
//		{
//			printf("恭喜您！您赢了！ \n");
//			break;
//		}
//	}
//	return 0;
//}

//某人三天打渔两天晒网，假设他从1990年1月1日开始打渔三天，然后晒网两天，请编程回答任意的一天他在打渔还是晒网。
//A boy works for 3 days while has a 2 days off.If he is working on 1st, Jan, 1990,
//then for a date entered from the keyboard, please write a program to determine what the boy is doing, working or resting ?
//Examples of input and output :
//1)Input:
//1990-01-05
//Output:
//He is having a rest.
//2)Input:
//1990-01-07
//Output:
//He is working.
//3)Input:
//1990-01-33
//Output:
//Invalid input.
//***输入数据格式***："%4d-%2d-%2d"
//***输出数据格式***："Invalid input."或"He is having a rest." 或"He is working."
//#include<stdio.h>
//int main()
//{
//    int m_days[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
//    int year, month, day, sumdays = 0;
//
//    scanf("%4d-%2d-%2d", &year, &month, &day);
//    // 检测非法输入
//    if (year < 1990 || month < 1 || month > 12 || day < 1 || day > m_days[month])
//    {
//        printf("Invalid input.\n");
//        return 0;
//    }
//    // 加上 每年的基本天数
//    sumdays += (year - 1990) * 365;
//
//    // 计算有多少个闰年 就额外加多少天
//    // 1988 年是闰年，每过四年是一个闰年
//    sumdays += ((year + 1 - 1990) / 4);
//
//    // 累加每月天数，其实，数组可以存成 {0 , 31, 59, 90 .....} 这样免去累加
//    for (int i = 1; i < month; i++) {
//        sumdays += m_days[i];
//    }
//    sumdays += day;
//    if (sumdays % 5 > 3 || sumdays % 5 == 0)
//    {
//        printf("He is having a rest.");
//    }
//    else
//    {
//        printf("He is working.");
//    }
//}

//* 输出正六边型。编写程序输出边长为N的空心正六边型，其边由‘* ’组成。
//** 输入格式要求："%d"  提示信息："Enter length:"
//* *输出格式要求："%c"
//程序运行示例如下：
//Enter length : 5
//    *****
//   *     *
//  *       *
// *         *
//*           *
// *         *
//  *       *
//   *     *
//    *****
//#include<stdio.h>
//int main()
//{
//	int sidelen = 0;
//	printf("Enter length:");
//	scanf("%d", &sidelen);
//	for (int i = 0; i < sidelen; i++)//上半个
//	{
//		if (i == 0)//第一行
//		{
//			for (int j = 0; j < sidelen - 1; j++)
//			{
//				printf("%c", ' ');
//			}
//			for (int j = 0; j < sidelen; j++)
//			{
//				printf("%c", '*');
//			}
//			printf("\n");
//		}
//
//		else
//		{
//			for (int j = 0; j < sidelen - 1 - i; j++)
//			{
//				printf("%c",' ');
//			}
//			printf("%c", '*');
//			for (int j = 0; j < sidelen + (i - 1) * 2; j++)
//			{
//				printf("%c", ' ');
//			}
//			printf("%c",'*');
//			printf("\n");
//		}
//	}
//	for (int i = 0; i < sidelen - 1; i++)
//	{
//		if (i == sidelen - 2)//最后一行
//		{
//			for (int j = 0; j < sidelen - 1; j++)
//			{
//				printf("%c", ' ');
//			}
//			for (int j = 0; j < sidelen; j++)
//			{
//				printf("%c", '*');
//			}
//			printf("\n");
//		}
//
//		else
//		{
//			for (int j = 0; j < i + 1; j++)
//			{
//				printf("%c", ' ');
//			}
//			printf("%c", '*');
//			for (int j = 0; j < 2 * sidelen - 1 - 2 * i + (sidelen - 5); j++)
//			{
//				printf("%c", ' ');
//			}
//			printf("%c", '*');
//			printf("\n");
//		}
//	}
//
//	return 0;
//}

//写一个函数，将一个字符串中的元音字母复制到另一个字符串，然后输出。
//程序的运行示例如下：
//提示信息："\n输入字符串："
//字符串中的元音字母是eoo
//* **输入数据格式 * **：使用gets()
//* **输出数据格式 * **："\n字符串中的元音字母是%s"
//程序运行示例：
//输入字符串：Hello, world!↙
//Hello, world!
//字符串中的元音字母是eoo
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char str1[100];
//	char str2[100];
//	int j = 0;
//	printf("\n输入字符串：");
//	gets(str1);
//	for (int i = 0; i < strlen(str1); i++)
//	{
//		if (str1[i] == 65 || str1[i] == 69 || str1[i] == 73 || str1[i] == 79 || str1[i] == 85 || str1[i] == 97 || str1[i] == 101 || str1[i] == 105 || str1[i] == 111 || str1[i] == 117)
//		{
//			str2[j++] = str1[i];
//		}
//	}
//	str2[j] = '\0';
//	printf("%s", str1);
//	printf("\n字符串中的元音字母是%s", str2);
//	return 0;
//}

//写一个函数，输入一行字符，将此字符串中最长的单词输出。
//程序的运行示例如下：
//输入一行文本：I am a student.
//
//最长的单词是：student
//#include<stdio.h>
//#include<string.h>
//#include<ctype.h>
//void findlongword(char* str, char* str2)
//{
//	int max = 0;
//	int maxindex = 0;
//	int i = 0;
//	int count = 0;
//	while (str[i] != '\0')
//	{
//
//		if (isalpha(str[i]))
//		{
//			count++;
//			i++;
//		}
//		else
//		{
//			count = 0;
//			i++;
//		}
//		if (max < count)
//		{
//			max = count;
//			maxindex = i - count;
//		}
//	}
//	int j = 0;
//	for (j = 0; j < max; j++)
//	{
//		str2[j] = str[maxindex + j];
//	}
//	str2[j] = '\0';
//}
//int main()
//{
//	char str[100];
//	char str2[100];
//	printf("输入一行文本：\n");
//	gets(str);
//
//	findlongword(str,str2);
//
//	printf("\n最长的单词是：%s", str2);
//	return 0;
//}

//在主函数中从键盘输入某班学生某门课程的成绩(已知班级人数最多不超过40人，具体人数由键盘输入),
//试编程计算其平均分，并计算出成绩高于平均分的学生的人数。
//要求：调用函数aver()，计算n名学生的平均成绩返回给主函数，然后在主函数中输出学生的平均成绩。
//函数原型：float aver(int score[], int n);
//***输入提示信息：无
//*** 输入格式：输入学生人数用"%d"，输入学生成绩用"%d"
//* **输出平均成绩提示信息和格式："Average score is %10.2f\n"
//* **输出平均分以上的学生人数提示信息和格式:"The number of students in more than average %d\n"
//注：（1）不能使用指针、结构体、共用体、文件、goto、枚举类型进行编程。
//（2）用纯C语言编程，所有变量必须在第一条可执行语句前定义。
//#include<stdio.h>
//float aver(int score[], int n)
//{
//	int sum = 0;
//	for (int i = 0; i < n; i++)
//	{
//		sum += score[i];
//	}
//	return (float)sum / n;
//}
//int main()
//{
//	int n = 0;
//	int j = 0;
//	int count = 0;
//	int score[40];
//	scanf("%d", &n);
//	for (int i = 0; i < n; i++)
//	{
//		scanf("%d", &score[i]);
//	}
//	float average = aver(score, n);
//	printf("Average score is %10.2f\n",average);
//	while (j < n)
//	{
//		if (score[j] > average)
//		{
//			count++;
//		}
//		j++;
//	}
//	printf("The number of students in more than average %d\n", count);
//
//	return 0;
//}

//程序改错。
//以下程序的功能是统计字符数。判断一个由’0’ ~‘9’这10个字符组成的字符串中哪个字符出现的次数最多。
//输入数据：第一行是测试数据的组数m，每组测试数据占1行，每行数据不超过1000个字符且非空。
//输出要求：m行，每行对应一组输入，包括出现次数最多的字符和该字符出现的次数。
//如果有多个字符出现的次数相同且最多，那么输出ASCII码最小的那一个。
//#include <stdio.h>
//#include <string.h>
//main()
//{
//    int  cases, sum[10], i, max;
//    char str[1000];
//    scanf("%d", &cases);
//    getchar();
//    while (cases > 0)
//    {
//        gets(str);
//        for (i = 0; i < 10; i++)
//            sum[i] = 0;
//        for (i = 0; i < strlen(str); i++)
//            sum[str[i] - '0']++;
//        max = 0;
//        for (i = 0; i < 10; i++)
//            if (sum[i] > sum[max]) max = i;
//        printf("%c %d\n", max + '0', sum[max]);
//        cases--;
//    }
//}

//写一个程序显示如下的金字塔树：
//           
//   *
//  ***
//   *
//  ***
// *****
//   *
//  ***
// *****
//*******
//   |
//===V===
//
//树的特点是由一系列逐渐增加的层构成。上面显示的树有3层，最后一行的=数量分别左右各3个。例如，两层时显示下面的结果：
//  *
// ***
//  *
// ***
//*****
//  |
//==V==
//
//要求程序从键盘输入层数（最后一行的=数量随着树的层数增加）。
//
//**输入格式要求："%d" 提示信息："请输入树的层数："
//#include<stdio.h>
//void plant(int n,int m)
//{
//	int x = m;
//	for (int i = 0; i < n; i++)
//	{
//		while (x)
//		{
//			printf(" ");
//			x--;
//		}
//		for (int j = 0; j < n - i - 1; j++)
//		{
//			printf(" ");
//		}
//		for (int j = 0; j < 2 * i + 1; j++)
//		{
//			printf("*");
//		}
//		printf("\n");
//		x = m;
//	}
//}
//int main()
//{
//	int x = 0;
//	printf("请输入树的层数：");
//	scanf("%d", &x);
//	
//	for (int i = 2; i < x + 2; i++)
//	{
//		plant(i,x + 1 - i);
//	}
//
//	for (int i = 0; i < x; i++)//树根
//	{
//		printf(" ");
//	}
//	printf("|\n");
//	for (int i = 0; i < x;i++)
//	{
//		printf("=");
//	}
//	printf("V");
//	for (int i = 0; i < x; i++)
//	{
//		printf("=");
//	}
//	return 0;
//}