#define _CRT_SECURE_NO_WARNINGS 1

//编写一个程序，将一个字符串s2插入到字符串s1中，其起始插入位置为n。
//** 输入格式要求："%d"  使用gets()接收字符串s1和s2  提示信息："main string:" "sub string:" "site of beginning:(<=%d)"
//* *输出格式要求："After instert:%s\n"
//程序运行示例如下：
//main string : hello, world!
//sub string : c
//site of beginning : (<= 12)6
//After instert : hello, cworld!
//#include <stdio.h>
//#include <string.h>
//int main()
//{
//    char s1[80], s2[80];
//    char s3[80];
//    int n;
//    printf("main string:");
//    gets(s1);
//    printf("sub string:");
//    gets(s2);
//    printf("site of beginning:(<=%d)", strlen(s1));
//    scanf("%d", &n);
//    int i, j = 0, k;
//    for (i = 0; i < n; i++)
//    {
//        s3[i] = s1[i];
//    }
//    for (; s2[j] != '\0'; i++, j++)
//    {
//        s3[i] = s2[j];
//    }
//    for (k = n; s1[k] != '\0'; i++, k++)
//    {
//        s3[i] = s1[k];
//    }
//    s3[i] = '\0';
//    printf("After instert:%s\n", s3);
//}

//由二个平方三位数获得三个平方二位数。已知两个平方三位数abc和xyz，其中a、b、c、x、y、z未必是不同的；而ax、by、cz是三个平方二位数。请编程求三位数abc和xyz。
//
//** 输出格式要求："%d and %d\n"
//* *输出提示信息："The possible perfect squares combinations are:\n"
//
//程序运行示例如下：
//The possible perfect squares combinations are :
//abc     xyz
//400 and 900
//841 and 196
//#include<stdio.h>
//int issqrt(int x)//是平方数返回1，否则返回0
//{
//	int i = 0;
//	int flag = 0;
//	for (i = 0; i < 32; i++)
//	{
//		if (i * i == x)
//		{
//			flag = 1;
//			return flag;
//		}
//	}
//	return flag;
//}
//int sucess(int x1, int x2)//验证这两个平方三位数组成的三个两位数是否为平方两位数，是，返回1，不是，返回0.
//{
//	if (issqrt(x1 / 100 * 10 + x2 / 100))
//	{
//		if (issqrt(x1 / 10 % 10 * 10 + x2 / 10 % 10))
//		{
//			if (issqrt(x1 % 10 * 10 + x2 % 10))
//			{
//				return 1;
//			}
//		}
//	}
//	return 0;
//}
//int main()
//{
//	int x1 = 0;
//	int x2 = 0;
//	printf("The possible perfect squares combinations are:\n");
//	for (x1 = 100; x1 < 1000; x1++)
//	{
//		if (issqrt(x1))
//		{
//			for (x2 = 100; x2 < 1000; x2++)
//			{
//				if (issqrt(x2))
//				{
//					if (sucess(x1, x2))
//					{
//						printf("%d and %d\n",x1,x2);
//					}
//				}
//			}
//		}
//	}
//	return 0;
//}

//自己定义一个函数，用字符数组作为函数参数的类型，
//以实现和函数strlen相同的功能，即在主函数中任意输入
//一个字符串（长度不超过80个字符），调用该函数计算
//输入字符串的实际长度，然后打印计算结果。
//要求按如下函数原型编程实现计算字符串长度的功能。
//int Mystrlen(char str[]);
//要求必须按照题目要求和用函数编程，否则不给分。
//** 要求输入提示信息格式为："Enter a string:"
//* *输出格式为："The length of the string is:%d\n"
//#include<stdio.h>
//int Mystrlen(char str[])
//{
//	int count = 0;
//	while (*str != '\0')
//	{
//		count++;
//		str++;
//	}
//	return count;
//}
//int main()
//{
//	char str[80];
//	printf("Enter a string:");
//	gets(str);
//	printf("The length of the string is:%d\n", Mystrlen(str));
//	return 0;
//}

//输入3个数x, y, z，按从小到大顺序排序后输出。
//要求：利用指针方法实现两数互换，函数原型为：void swap(int* p1, int* p2);
//输入提示：printf("please input 3 number x,y,z");
//输出格式：printf("the sorted numbers are:%d,%d,%d\n", );
//#include<stdio.h>
//void swap(int* p1, int* p2)
//{
//	*p1 = *p1 ^ *p2;
//	*p2 = *p1 ^ *p2;
//	*p1 = *p1 ^ *p2;
//
//}
//int main()
//{
//	int x = 0;
//	int y = 0;
//	int z = 0;
//	printf("please input 3 number x,y,z");
//	scanf("%d,%d,%d", &x, &y, &z);
//	if (x > y)
//	{
//		swap(&x, &y);
//	}
//	if (y > z)
//	{
//		swap(&y, &z);
//	}
//	if (x > y)
//	{
//		swap(&x, &y);
//	}
//	printf("the sorted numbers are:%d,%d,%d\n", x, y, z);
//	return 0;
//}

