//编程计算圆的面积。
//要求：
//（1）圆的半径r的值为1到10（包括1和10）之间的正整数；
//（2）用符号常量定义PI为3.14；
//（3）当圆的面积小于50时输出圆的面积并对圆的面积求累加和，
//大于50时结束循环；
//（4）输出累加和的结果
//（5）不用数组编程
//* *要求输入提示信息为：无输入提示信息和输入数据
//* *要求输出格式为：
//（1）"area=%.2f\n"
//（2）"sum=%.2f\n"
//#define PI 3.14
//#include<stdio.h>
//int main()
//{
//	double sum = 0;
//	double area = 0;
//	for (int r = 1; r <= 10; r++)
//	{
//		if (PI * r * r < 50)
//		{
//			area = PI * r * r;
//			sum += area;
//			printf("area=%.2f\n",area);
//		}
//		else
//		{
//			printf("sum=%.2f\n",sum);
//			break;
//		}
//	}
//	return 0;
//}

//编程计算分段：
//y = x               x < 1
//    y = 2x - 1            1 <= x < 10
//    y = 3x - 11           x >= 10
//    从键盘输入一个单精度实数x，打印出y值。
//    * *输入提示信息格式要求为："Please input x:"；
//    * *输入格式要求为："%f"
//    * *输出格式要求为"y = %.2f\n"。
//#include<stdio.h>
//int main()
//{
//	float x,y;
//	printf("Please input x:");
//	scanf("%f", &x);
//	if (x < 1)
//	{
//		y = x;
//	}
//	else if (x >= 1 && x < 10)
//	{
//		y = 2 * x - 1;
//	}
//	else if (x >= 10)
//	{
//		y = 3 * x - 11;
//	}
//	printf("y = %.2f\n", y);
//	return 0;
//}

//利用pi / 2 = (2 / 1) * (2 / 3) * (4 / 3) * (4 / 5) * (6 / 5) * (6 / 7)*...前100项之积，编程计算pi的值。
//为保证计算精度，请用double类型计算。
//* *输出格式要求："pi = %f\n"
//#include<stdio.h>
//int main()
//{
//	double pi = 2;
//	for (double i = 2; i < 101; i += 2)
//	{
//		pi = pi * (i / (i - 1)) * (i / (i + 1));
//	}
//	printf("pi = %f\n", pi);
//	return 0;
//}

//鸡兔同笼，共有98个头，386只脚，请用穷举法编程计算鸡、兔各多少只。
//** 输入提示信息格式要求：无输入数据
//** 输出格式要求："x = %d,y = %d\n"
//#include<stdio.h>
//int main()
//{
//	int x = 0;//鸡
//	int y = 0;//兔
//	for (x = 0; x < 98; x++)
//	{
//		for (y = 0; y < 98; y++)
//		{
//			if (x + y == 98 && 2 * x + 4 * y == 386)
//			{
//				printf("x = %d,y = %d\n",x,y);
//			}
//		}
//	}
//	return 0;
//}