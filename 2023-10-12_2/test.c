#define _CRT_SECURE_NO_WARNINGS 1
//计算1 - 1 / 2 + 1 / 3 - 1 / 4 + ... + 1 / 99 - 1 / 100 + ...，直到最后一项的绝对值小于1e - 4为止。
//* *输出格式要求："sum = %f\n"
//#include<stdio.h>
//int main()
//{
//	int i = 2;
//	int j = 3;
//	float sum = 1;
//	float minuend = 1.0 / i;
//	float addend = 1.0 / j;
//	while (1)
//	{
//		sum -= minuend;
//		i += 2;
//		minuend = 1.0 / i;
//		if (minuend < 1e-4)
//			break;
//		sum += addend;
//		j += 2;
//		addend = 1.0 / j;
//		if (addend < 1e-4)
//			break;
//	}
//
//	printf("sum = %f\n", sum);
//	return 0;
//}

//从键盘输入6位仅由数字0~9组成的密码。用户每输入一个密码并按回车键后，程序给出判断。
// 如果是数字，则原样输出该数字，并提示用户目前已经输入了几位密码，同时继续输入下一位密码；
// 否则，程序提示"error"，并让用户继续输入下一位密码。直到用户输入的全部密码为止。
//* *输入提示信息 * *："Input your password:\n"   //输入提示信息放在循环体外
//* *输入数据格式 * *："%c"
//* *输出提示信息 * *：
//若输入的是0~9之间的数字，则输出为："%c, you have enter %d-bits number\n"
//若输入的不是0~9间的数字，则输出为："error\n"
//1、输入提示信息放在循环体外；
//2、若第1次输入数据为1：
//则输出数据格式为："1, you have enter 1-bits number"
//3、若第3次输入数据为6：
//则输出数据格式为："6, you have enter 3-bits number"
//4、若输入数据为d：
//则输出数据格式为："error\n"
//#include<stdio.h>
//#include<ctype.h>
//int main()
//{
//	int i = 0;
//	char str[6];
//	printf("Input your password:\n");
//	while (str[5] > 57 || str[5] < 48)
//	{
//		scanf("%c", &str[i]);
//		getchar();
//		if (isdigit(str[i]))
//		{
//			printf("%c, you have enter %d-bits number\n", str[i], i + 1);
//			i++;
//		}
//		else
//		{
//			str[5] = 58;
//			printf("error\n");
//		}
//	}
//	return 0;
//}