#pragma once
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<assert.h>
#include<stdbool.h>
#include"Contact.h"

typedef PeoInfo SLDataType;
typedef struct SListNode
{
	SLDataType data;
	struct SListNode* next;
}SLNode;

void SLPrint(SLNode* phead);

void SLPushBack(SLNode** pphead, SLDataType x);

void SLPushFront(SLNode** pphead, SLDataType x);

void SLPopBack(SLNode** pphead);

void SLPopFront(SLNode** pphead);

//在指定位置之前插入
void SLInsert(SLNode** pphead, SLNode* pos, SLDataType x);
//指定位置之后插入数据
void SLInsertAfter(SLNode* pos, SLDataType x);

//找节点(这里传一级指针实际上就可以，因为不改变头节点，但是这里要写二级指针，因为要保持接口一致性)
SLNode* SLFind(SLNode** pphead, SLDataType x);

//删除pos位置的节点
void SLErase(SLNode** pphead, SLNode* pos);
//删除pos之后的节点
void SLEraseAfter(SLNode* pos);

//销毁链表
void SLDesTroy(SLNode** pphead);