#include"Contact.h"
#include"SList.h"

//初始化通讯录
void InitContact(contact** con);

//添加通讯录数据
void AddContact(contact** con)
{
	PeoInfo peoinfo;
	printf("请输入用户姓名：\n");
	scanf("%s", peoinfo.name);
	printf("请输入用户性别：\n");
	scanf("%s", peoinfo.sex);
	printf("请输入用户年龄：\n");
	scanf("%d", &peoinfo.age);
	printf("请输入用户电话：\n");
	scanf("%s", peoinfo.tel);
	printf("请输入用户地址：\n");
	scanf("%s", peoinfo.addr);
	SLPushBack(con, peoinfo);

}
contact* FindByName(contact* con, char name[]) {
	contact * cur = con;
	while (cur)
	{
		if (strcmp(cur->data.name, name) == 0)
		{
			return cur;

		}
		cur = cur->next;
	}
	return NULL;
	
}
//删除通讯录数据
void DelContact(contact** con)
{
	char name[NAME_MAX];
	printf("请输入您要删除的联系人姓名：\n");
	scanf("%s", name);
	contact* pos = FindByName(*con,name);
	if (pos == NULL)
	{
		printf("您要删除的联系人不存在！\n");
		return;
	}
	SLErase(con, pos);
	printf("删除成功。\n");

}
//展示通讯录数据
void ShowContact(contact* con)
{
	printf("%s %s %s %s %s\n", "姓名", "性别", "年龄", "电话", "地址");
	contact* cur = con;
	while (cur)
	{
		printf("%s %s %d %s %s\n", cur->data.name, cur->data.sex, cur->data.age, cur->data.tel, cur->data.addr);
		cur = cur->next;
	}
}
//查找通讯录数据
void FindContact(contact* con)
{
	char name[NAME_MAX];
	printf("请输入您要查找的联系人姓名:\n");
	scanf("%s", name);
	contact* pos = FindByName(con, name);
	if (pos == NULL)
	{
		printf("您要查找的联系人不存在！\n");
		return;
	}
	printf("查找成功。\n");
	printf("%s %s %s %s %s\n", "姓名", "性别", "年龄", "电话", "地址");
	printf("%s %s %d %s %s\n", pos->data.name, pos->data.sex, pos->data.age, pos->data.tel, pos->data.addr);
}
//修改通讯录数据
void ModifyContact(contact** con)
{
	char name[NAME_MAX];
	printf("请输入您要修改的联系人姓名:\n");
	scanf("%s", name);
	contact* pos = FindByName(*con, name);
	if (pos == NULL)
	{
		printf("您要修改的联系人不存在！\n");
		return;
	}
	printf("请输入用户姓名：\n");
	scanf("%s", pos->data.name);
	printf("请输入用户性别：\n");
	scanf("%s", pos->data.sex);
	printf("请输入用户年龄：\n");
	scanf("%d", &pos->data.age);
	printf("请输入用户电话：\n");
	scanf("%s", pos->data.tel);
	printf("请输入用户地址：\n");
	scanf("%s", pos->data.addr);
	printf("修改成功。\n");
}
//销毁通讯录数据
void DestroyContact(contact** con)
{
	SLDesTroy(con);
}