#define _CRT_SECURE_NO_WARNINGS 1
#include"Contact.h"
#include"SList.h"

void SLtest()
{
	contact* con = NULL;
	AddContact(&con);
	AddContact(&con);
	AddContact(&con);
	ShowContact(con);

	DelContact(&con);
	ShowContact(con);

	FindContact(con);

	ModifyContact(&con);
	ShowContact(con);

	DestroyContact(&con);
	
}
int main()
{
	SLtest();
	return 0;
}