#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<math.h>

//int main()
//{
//	int fir = 20;
//	int sum = 20;
//	while (fir != 0)
//	{
//		fir /= 2;
//		sum += fir;
//	}
//	printf("一共可以喝 %d 瓶", sum);
//	return 0;
//}


//int main()
//{
//	//上菱形
//	for (int n = 1; n <= 7; n++)
//	{
//		for (int i = 7 - n; i > 0; i--)
//		{
//			printf(" ");
//		}
//		for (int j = 1; j <= 2 * n - 1; j++)
//		{
//			printf("*");
//		}
//		printf("\n");
//
//	}
//	//下菱形
//	for (int n = 1; n <= 6; n++)
//	{
//		for (int i = 1; i <= n; i++)
//		{
//			printf(" ");
//		}
//		for (int j = 13 - 2 * n; j >= 1; j--)
//		{
//			printf("*");
//		}
//		printf("\n");
//	}
//	return 0;
//}


//求出0～100000之间的所有“水仙花数”并输出。
//
//“水仙花数”是指一个n位数，其各位数字的n次方之和确好等于该数本身，如 : 153＝1 ^ 3＋5 ^ 3＋3 ^ 3，则153是一个“水仙花数”。
//int main()
//{
//	int n = 0;
//	for (n = 0; n <= 100000; n++)
//	{
//		int m = n;
//		int sum = 0;
//		int count = 0;
//		while (n > 0)
//		{
//			count += 1;
//			n /= 10;
//		}//求出总共有多少位数
//		n = m;
//		while (n > 0)
//		{
//			sum += pow((n % 10), count);
//			n /= 10;
//		}//计算各位数字的n次方之和
//		if (sum == m)
//		{
//			printf("%d\n",m);
//		}//验证是否相等
//		n = m;
//	}
//	return 0;
//}


//求Sn=a+aa+aaa+aaaa+aaaaa的前5项之和，其中a是一个数字，
//例如：2 + 22 + 222 + 2222 + 22222
//int main()
//{
//	int a = 0;
//	scanf("%d", &a);
//	int sum = a;
//	int Sn = a;
//	for(int i = 1; i < 5; i++)
//	{
//		sum = pow(10,i) * a + sum;
//		Sn += sum;
//	}
//	printf("%d", Sn);
//	return 0;
//}
