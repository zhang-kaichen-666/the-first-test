#define _CRT_SECURE_NO_WARNINGS 1

//将一个链表按逆序排列，即将链头当链尾，链尾当链头。
//程序的运行示例如下：
//请输入链表（非数表示结束）
//结点值：3
//结点值：4
//结点值：5
//结点值：6
//结点值：7
//结点值：end
//原来表：
//3   4   5   6   7
//
//反转表：
//7   6   5   4   3
//#include <stdio.h>
//#include <stdlib.h>
//
//typedef struct node {
//    int data;
//    struct node* next;
//}Node;
//
//Node* CreatList(void)
//{
//    int va1, i;
//    Node* phead, * p, * q;
//    phead = (Node*)malloc(sizeof(Node));
//    q = phead;
//    q->next = NULL; 
//    printf("请输入链表（非数表示结束）\n");
//    while (1)
//    {
//        printf("结点值：");
//        if (scanf("%d", &va1) == 1)
//        {
//            p = (Node*)malloc(sizeof(Node));
//            p->data = va1;
//            q->next = p;
//            p->next = NULL;
//            q = p;
//        }
//        else
//        {
//            break;
//        }
//    }
//    
//    return phead;
//}
//void ShowList(Node* phead)
//{
//    Node* p;
//    p = phead->next;
//    while (p)
//    {
//        printf("   %d", p->data);
//        p = p->next;
//    }
//    printf("\n");
//}
//Node* ReverseList(Node* phead)
//{
//    Node* p, * q;
//    p = phead->next;
//    phead->next = NULL;
//    while (p)
//    {
//        q = p;
//        p = p->next;
//        q->next = phead->next;
//        phead->next = q;
//    }
//    return phead;
//}
//int main(void)
//{
//    Node* phead;
//    phead = CreatList();
//    printf("原来表：\n");
//    ShowList(phead);
//    phead = ReverseList(phead);
//    printf("\n");
//    printf("反转表：\n");
//    ShowList(phead);
//    return 0;
//}

//编程计算a % 1 + aa % 2 + aaa % 3 + ... + aa...a % n(最后一项是n个a对n求余)
//的值，然后输出这个值。
//（要求存储累加项及总和的变量定义为长整型, a与n定义为整型），
//其中n和a的值由键盘输入。
//** 要求：
//** 输入提示信息格式为："Enter n,a:\n"
//* *输入格式为："%d,%d"
//* *输出格式为："Sum=%ld\n"
//* *请严格按照以上要求输入输出，除了以上指定输出结果外，
//不允许有其他临时输出
//#include<stdio.h>
//int main()
//{
//	int a = 0;
//	int n = 0;
//	long tmp = 0;
//	long sum = 0;
//	printf("Enter n,a:\n");
//	scanf("%d,%d", &n, &a);
//	for (int i = 0; i < n; i++)
//	{
//		int j = i;
//		long tmp2 = a;
//		while (j)
//		{
//			tmp2 = tmp2 * 10 + a;
//			j--;
//			
//		}
//		tmp = tmp2 % (i+1);
//		sum += tmp;
//	}
//	printf("Sum=%ld\n", sum);
//	return 0;
//}

//题目：去除字符串中的空格并分解单词。
//要求：在main()中调用下述各函数，完成字符串中去除空格并分解单词的功能。
//函数原型如下：
//函数一：单词内是否包含数字 int IsNumIn(char word[])
//函数二：单词内是否包含空格 int IsSpcIn(char word[])
//函数三：去掉单词的前后空格，tab键和换行符 Trim(char oldWord[], char newWord[])
//函数四：单词内部有空格，分解成多个单词 Seg(char words[], char wArray[][100]) 假设单词内部只有一个空格，没有两个连续空格的情况发生。
//要求利用gets函数来获得用户输入的字符串，不能用scanf来获得用户输入的字符串
//参考输入，输出
//参考输入
//ros2e
//参考输出
//error
//参考输入
//︺︺hello︺（ ︺代表一个空格）
//参考输出
//hello
//参考输入
//︺︺hello world︺
//参考输出
//hello
//world
#include<stdio.h>
#include<ctype.h>
#include<string.h>
int IsNumIn(char word[])//有数字返回0，没有数字返回1
{
	int len = strlen(word);
	for (int i = 0; i < len; i++)
	{
		if (isdigit(word[i]) != 0)
		{
			return 0;
		}
	}
	return 1;
}
int IsSpcIn(char word[])//有空格返回0，没空格返回1
{
	int len = strlen(word);
	for (int i = 0; i < len; i++)
	{
		if (isspace(word[i]) != 0)
		{
			return 0;
		}
	}
	return 1;
}
void Trim(char oldWord[], char newWord[])//去掉单词的前后空格，tab键和换行符
{
	int len = strlen(oldWord);
	int j = 0;
	for (int i = 0; i < len; i++)
	{
		if (isspace(oldWord[i]) == 0)
		{
			newWord[j] = oldWord[i];
			j++;
		}
	}
	newWord[j] = '\0';
}
void Seg(char words[], char wArray[][100])
{
	int i = 0;
	int j = 0;
	int count = 0;
	int len = strlen(words);
	while (isspace(words[i]))
	{
		i++;
		count++;
	}
	while (isspace(words[i]) == 0)
	{
		wArray[0][i - count] = words[i];
		i++;
	}
	wArray[0][i - count] = '\0';
	while (i < len)
	{
		wArray[1][j++] = words[i++];
	}
	wArray[1][j] = '\0';
}
int main()
{
	char word[100];
	char new_word_1[2][100];
	gets(word);
	if (IsNumIn(word) == 0)//有数字
	{
		printf("error");
	}
	else if(IsSpcIn(word))//没空格
	{
		printf("%s", word);
	}
	else
	{
		Seg(word, new_word_1);
		Trim(new_word_1[1], new_word_1[1]);
		printf("%s\n", new_word_1[0]);
		printf("%s\n", new_word_1[1]);
	}



	return 0;
}