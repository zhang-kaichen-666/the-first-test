#define _CRT_SECURE_NO_WARNINGS 1
//下面程序用于将6个数按输入时顺序的逆序进行排列。找出其中错误，并改正之。
//#include<stdio.h>
//void Swap(int* x, int* y)
//{
//    int temp;
//    temp = *x;
//    *x = *y;
//    *y = temp;
//}
//void Sort(int* p, int m)
//{
//    int i;
//    int* p1, * p2;
//    for (i = 0; i < m / 2; i++)
//    {
//        p1 = p + i;
//        p2 = p + (m - 1 - i);
//        Swap(p1, p2);
//    }
//}
//int main()
//{
//    int i;
//    int* p, num[6];
//    for (i = 0; i < 6; i++)
//        scanf("%d", &num[i]);
//    p = num;
//    Sort(p, 6);
//    for (i = 0; i < 6; i++)
//        printf("%d ", num[i]);
//    return 0;
//}

//输入5个国名，编程找出并输出按字典顺序排在最前面的国名。
//提示：所谓字典顺序就是将字符串按由小到大的顺序排列，因此找出按字典顺序排在最前面的国名指的就是最小的字符串。
//程序的运行结果示例：
//Input five countries' names:
//America↙
//China↙
//Japan↙
//England↙
//Sweden↙
//The minimum is : America
//
//输入格式 : 国名输入用gets()函数。（字符串长度最大限制为30）
//输出格式：
//输入提示信息："Input five countries' names:\n"
//输出提示信息："The minimum is:%s\n"
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char str[5][100];
//	printf("Input five countries' names:\n");
//	for (int i = 0; i < 5; i++)
//	{
//		gets(str[i]);
//	}
//	char* ret = str[0];
//	for (int i = 1; i < 5; i++)
//	{
//		if (strcmp(ret, str[i]) > 0)
//		{
//			ret = str[i];
//		}
//	}
//	printf("The minimum is:%s\n",ret);
//	return 0;
//}

//有关输入输出问题。
//输入为：
//1↙
//2↙
//a↙
//b↙
//运行结果为：1, 2, a, b, 123.300000, 65535
//请改正程序中的错误，使它能得出正确的结果。
//#include<stdio.h>
//int main()
//{
//	double b;
//	int a = 65535;
//	char c, d;
//	int f, g;
//
//	b = (1234.0 - 1) / 10;
//	scanf("%c", &c);
//	scanf("%c", &d);
//	scanf("%c\t", &f);
//	scanf("%c", &g);
//	printf("%c,%c,%c,%c,%f,%d", c, d, f, g, b, a);
//	return 0;
//}