#define _CRT_SECURE_NO_WARNINGS 1
//从键盘输入一个长度为N（N为10）的整型数组，
// 而后将数组中小于零的元素移动到数组的前端，
// 大于零的元素移动到数组的后端，
// 等于零的元素留在数组中间。
// 比如原始数组为：2 - 5 - 89 75 0 - 89 0 93 48 0，
// 经过处理后的数组为： - 5 - 89 - 89 0 0 0 75 93 48 2。
// 由于不要求数组有序，所以不允许用排序方法。
//要求按如下函数原型编写程序：
//void Rearrange(int a[], int n);
//**输入格式要求："%d" 提示信息："Input %d integer number\n"
//* *输出格式要求："%5d"
//程序运行示例如下：
//Input 10 integer number
//2 -5 -89 75 0 -89 0 93 48 0↙
//- 5 - 89 - 89    0    0    0    2   75   93   48
//#include<stdio.h>
//void Rearrange(int a[], int n)
//{
//	int b[10];
//	int j = 0;
//	for (int i = 0; i < 10; i++)
//	{
//		if (a[i] < 0)
//		{
//			b[j++] = a[i];
//		}
//	}
//	for (int i = 0; i < 10; i++)
//	{
//		if (a[i] == 0)
//		{
//			b[j++] = a[i];
//		}
//	}	
//	for (int i = 0; i < 10; i++)
//	{
//		if (a[i] > 0)
//		{
//			b[j++] = a[i];
//		}
//	}
//	for (int i = 0; i < 10; i++)
//	{
//		a[i] = b[i];
//	}
//}
//int main()
//{
//	int arr[10];
//	printf("Input %d integer number\n",10);
//	for (int i = 0; i < 10; i++)
//	{
//		scanf("%d", &arr[i]);
//	}
//	Rearrange(arr, 10);
//	for (int i = 0; i < 10; i++)
//	{
//		printf("%5d", arr[i]);
//	}
//	return 0;
//}

//利用一个字符数组作函数参数, 实现字符串（最大长度为80个字符 ）的逆序存放。
//要求如下：
//(1)在子函数Inverse中实现字符串的逆序存放。函数原型为：
//void Inverse(char str[]);
//(2)在主函数中
//从键盘输入字符串(使用gets函数)
//然后，调用Inverse函数，
//最后，输出逆序后的字符串。
//(3)** 输入提示信息："Input a string:\n"
//* *输出提示信息："Inversed results:\n"
//* *输出格式："%s\n"
//注：不能使用指针、结构体、共用体、文件、goto、枚举类型进行编程。
//#include<stdio.h>
//#include<string.h>
//void Inverse(char str[])
//{
//	int front = 0;
//	int last = strlen(str) - 1;
//	while (front < last)
//	{
//		char tmp = str[front];
//		str[front++] = str[last];
//		str[last--] = tmp;
//	}
//}
//int main()
//{
//	char str[80];
//	printf("Input a string:\n");
//	gets(str);
//	Inverse(str);
//	printf("Inversed results:\n");
//	printf("%s\n",str);
//	return 0;
//}

//从键盘任意输入一个4位整数, 编程计算并输出它的逆序数.例如:输入1234,
//  分离出千位1、百位2、十位3和个位4，
// 然后计算4 * 1000 + 3 * 100 + 2 * 10 + 1 = 4321，并输出4321。
//* *要求输入提示信息为 : 无
//* *要求输入格式为 : "%d"
//* *要求输出格式为："%d"
//程序运行示例如下：
//1234   此处为输入
//4321   此处为输出
//#include<stdio.h>
//int main()
//{
//	int x = 0;
//	scanf("%d", &x);
//	int x1 = x % 10;
//	int x2 = x / 10 % 10;
//	int x3 = x / 100 % 10;
//	int x4 = x / 1000 % 10;
//	printf("%d", 1000 * x1 + 100 * x2 + 10 * x3 + x4);
//	return 0;
//}