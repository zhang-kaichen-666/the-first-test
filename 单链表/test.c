#include"SList.h"
#include<stdio.h>
#include<stdlib.h>

void sltest()
{
	SLNode* plist = NULL;
	SLPushBack(&plist, 1);
	SLPushBack(&plist, 2);
	SLPushBack(&plist, 3);
	SLPushBack(&plist, 4);
	//SLPushFront(&plist, 3);
	SLPrint(plist);

	//SLPopBack(&plist);
	//SLPrint(plist);
	//SLPopFront(&plist);
	//SLPrint(plist);
	SLNode* find = SLFind(&plist, 1);
	SLInsert(&plist, find, 11);
	SLPrint(plist);
	SLInsertAfter(find, 100);
	SLPrint(plist);
	SLEraseAfter(find);
	SLPrint(plist);
	SLErase(&plist, find);
	SLPrint(plist);

	SLDesTroy(&plist);
	SLPrint(plist);

}
int main()
 {
	sltest();
	return 0;
}