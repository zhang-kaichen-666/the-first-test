#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

//在一个整型数组中，只有一个数字出现一次，其他数组都是成对出现的，请找出那个只出现一次的数字。
//例如：
//数组中有：1 2 3 4 5 1 2 3 4，只有5出现一次，其他数字都出现2次，找出5
//int main()
//{
//	int arr[9] = { 1,2,3,4,5,1,2,3,4 };
//	for (int i = 0; i < 9; i++)
//	{
//		int flag = 0;
//		int flag2 = 1;
//		for (int j = 0; j < 9; j++)
//		{
//			flag = 0;
//			if (i == j)
//			{
//				continue;
//			}
//			flag = flag ^ arr[i] ^ arr[j];
//			if (flag == 0)
//			{
//				flag2 = 0;
//				break;
//			}
//		}
//		if (flag2 == 1)
//		{
//			printf("%d\n", arr[i]);
//		}
//	}
//	return 0;
//}


//标准答案
//#include <stdio.h>
//
//int find_single_dog(int arr[], int sz)
//{
//    int ret = 0;
//    int i = 0;
//    for (i = 0; i < sz; i++)
//    {
//        ret ^= arr[i];
//    }
//    return ret;
//}
//int main()
//{
//    int arr[] = { 1,2,3,4,5,1,2,3,4 };
//    int sz = sizeof(arr) / sizeof(arr[0]);
//    int dog = find_single_dog(arr, sz);
//    printf("%d\n", dog);
//
//
//    return 0;
//}


//写一个函数返回参数二进制中 1 的个数。
//比如： 15    0000 1111    4 个 1
//int react(int n)
//{
//	int count = 0;
//	while (n)
//	{
//		n = n & (n - 1);
//		count++;
//	}
//	return count;
//}
//int main()
//{
//	int n = 0;
//	printf("请输入一个数字：");
//	scanf("%d", &n);
//	printf("%d\n", react(n));
//	return 0;
//}


//不允许创建临时变量，交换两个整数的内容
//int main()
//{
//	int a = 0;
//	int b = 0;
//	printf("请输入两个整数：");
//	scanf("%d%d", &a, &b);
//	a = a ^ b;
//	b = a ^ b;
//	a = a ^ b;
//	printf("交换后a = %d  b = %d\n", a, b);
//	return 0;
//}