#define _CRT_SECURE_NO_WARNINGS 1
//输入：工资数，小时数（整数，空格分隔），
//输出：工资 / 小时数（精确到小数点后2位），
//并根据四舍五入取整，然后将取整的数平方后计算一共有几位，后三位分别是什么。
//程序的运行示例如下：
//2345 2    （输入）
//1172.50   （本行及以下为输出）
//1173
//7
//9 2 9
//* **输入数据格式 * **："%d %d"
//* **输出数据格式 * **："%.2f\n"
//"%d\n"
//"%d\n"
//"%d %d %d\n"
//#include<stdio.h>
//#include<math.h>
//int main()
//{
//	int salary = 0;
//	int hours = 0;
//	scanf("%d %d", &salary, &hours);
//	double x = (double)salary / (double)hours;
//	printf("%.2f\n", x);
//	if (x - salary / hours >= 0.5)
//	{
//		x = salary / hours + 1;
//	}
//	else
//	{
//		x = salary / hours;
//	}
//	int y = (int)x;
//	printf("%d\n", y);
//	int powy = (int)pow(y, 2);
//	int count = 0;
//	while (powy)
//	{
//		powy /= 10;
//		count++;
//	}
//	printf("%d\n", count);
//	powy = (int)pow(y, 2);
//	int num1, num2, num3;
//	num1 = powy % 10;
//	num2 = (powy % 100 - num1) / 10;
//	num3 = (powy % 1000 - num2 * 10 - num1) / 100;
//	printf("%d %d %d\n", num3, num2,num1);
//	return 0;
//}

//有一篇文章，共有三行文字，每行有80个字符。要求分别统计出其中英文大写字母、小写字母、数字、空格、以及其它字符的个数。
//程序的运行示例如下：
//请输入第0行：
//helloworld
//请输入第1行：
//12345gogogo
//请输入第2行：
//end bye bye.
//helloworld
//12345gogogo
//end bye bye.
//大写字母数：0
//小写字母数：25
//数字个数  ：5
//空格个数  ：2
//其它字符  ：1
//* **输入提示信息 * **："\n请输入第%d行：\n"
//* **输入数据格式 * **：使用gets()
//* **输出数据格式 * **："%s\n"
//"大写字母数：%d\n"
//"小写字母数：%d\n"
//"数字个数  ：%d\n"
//"空格个数  ：%d\n"
//"其它字符  ：%d\n"
//#include<stdio.h>
//#include<string.h>
//#include<ctype.h>
//int main()
//{
//	char str[3][80];
//	int upperchar = 0;
//	int lowerchar = 0;
//	int digitchar = 0;
//	int spacechar = 0;
//	int elsechar = 0;
//	for (int i = 0; i < 3; i++)
//	{
//		printf("\n请输入第%d行：\n", i);
//		gets(str[i]);
//	}
//	for (int i = 0; i < 3; i++)
//	{
//		for (int j = 0; j < strlen(str[i]); j++)
//		{
//			if (isupper(str[i][j]))
//			{
//				upperchar++;
//			}
//			else if (islower(str[i][j]))
//			{
//				lowerchar++;
//			}
//			else if (isdigit(str[i][j]))
//			{
//				digitchar++;
//			}
//			else if (isspace(str[i][j]))
//			{
//				spacechar++;
//			}
//			else
//			{
//				elsechar++;
//			}
//		}
//	}
//	for (int i = 0; i < 3; i++)
//	{
//		printf("%s\n", str[i]);
//	}
//	printf("大写字母数：%d\n",upperchar);
//	printf("小写字母数：%d\n",lowerchar);
//	printf("数字个数  ：%d\n",digitchar);
//	printf("空格个数  ：%d\n",spacechar);
//	printf("其它字符  ：%d\n",elsechar);
//	return 0;
//}

//写一个程序输入一个4乘5的数组，查找所有小于0的数组元素及其所在的行列下标。
//** 输入格式要求："%lf" 提示信息："请输入一个4乘5的矩阵：\n" "请输入a[%d][%d]："
//* *输出格式要求："第%d行%d列的元素%.2f小于0\n" （数组由第0行，第0列
//#include<stdio.h>
//int main()
//{
//	double arr[4][5];
//	int line[20];
//	int col[20];
//	int a = 0;
//	printf("请输入一个4乘5的矩阵：\n");
//	for (int i = 0; i < 4; i++)
//	{
//		for (int j = 0; j < 5; j++)
//		{
//			printf("请输入a[%d][%d]：", i, j);
//			scanf("%lf", &arr[i][j]);
//			if (arr[i][j] < 0)
//			{
//				line[a] = i;
//				col[a++] = j;
//			}
//		}
//	}
//	line[a] = -1;
//	a = 0;
//	while (line[a] >= 0)
//	{
//		printf("第%d行%d列的元素%.2f小于0\n", line[a], col[a], arr[line[a]][col[a]]);
//		a++;
//	}
//	return 0;
//}

//编写程序，输入一个以回车符结束的字符串（少于80个字符），
// 过滤去所有的非十六进制字符后，组成一个新字符串（十六进制形式）
// ，然后将其转换为十进制数后输出。
//** 输入提示信息："请输入十六进制字符串："
//* *输出格式要求："十六进制串%s的值=十进制%.0f"
//程序运行示例如下：
//请输入十六进制字符串：P12aZ
//十六进制串12a的值 = 十进制298
//#include<stdio.h>
//#include<string.h>
//#include<stdlib.h>
//int main()
//{
//	char str1[80];
//	char str2[80];
//	char* end;
//	int j = 0;
//	printf("请输入十六进制字符串：");
//	gets(str1);
//	for (int i = 0; i < strlen(str1); i++)
//	{
//		if ((str1[i] <= 'F' && str1[i] >= 'A') || (str1[i] <= 'f' && str1[i] >= 'a') || (str1[i] >= '0' && str1[i] <= '9'))
//		{
//			str2[j++] = str1[i];
//		}
//	}
//	str2[j] = '\0';
//	float sum = (float)strtol(str2, &end, 16);
//	printf("十六进制串%s的值=十进制%.0f", str2, sum);
//}