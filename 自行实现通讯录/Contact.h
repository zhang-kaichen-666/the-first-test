#pragma once
#define NAME_MAX 100
#define SEX_MAX 4
#define TEL_MAX 11
#define ADDR_MAX 100
//前置声明
typedef struct SeqList contact;
//用户数据
typedef struct PersonInfo
{
	char name[NAME_MAX];
	char sex[SEX_MAX];
	int age;
	char tel[TEL_MAX];
	char addr[ADDR_MAX];

}PeoInfo;

//初始化通讯录
void InitContact(contact* pcon);
//添加通讯录数据
void AddContact(contact* pcon);
//删除通讯录数据
void DelContact(contact* pcon); 
//展示通讯录数据
void ShowContact(contact* pcon);
//查找通讯录数据
void FindContact(contact* pcon);
//修改通讯录数据
void ModifyContact(contact* pcon);
//销毁通讯录数据
void DestroyContact(contact* pcon);