#include"Contact.h"
#include"SeqList.h"
#include<stdio.h>
#include<string.h>


//初始化通讯录
void InitContact(contact* pcon)
{
	SLInit(pcon);
}
//添加通讯录数据
void AddContact(contact* pcon)
{
	PeoInfo info;
	printf("请输入联系人姓名:\n");
	scanf("%s", info.name);
	printf("请输入联系人的性别:\n");
	scanf("%s", info.sex);
	printf("请输入联系人的年龄:\n");
	scanf("%d", &info.age);
	printf("请输入联系人的电话:\n");
	scanf("%s", info.tel);
	printf("请输入联系人的住址:\n");
	scanf("%s", info.addr);
	SLPushBack(pcon, info);
}
//查找联系人是否存在
int FindByName(contact* pcon, char name[])
{
	for (int i = 0; i < pcon->size; i++)
	{
		if (strcmp(pcon->a[i].name, name) == 0)
		{
			return i;
		}
	}
	return -1;
}
//删除通讯录数据
void DelContact(contact* pcon)
{
	printf("请输入要删除的用户名称:\n");
	char name[NAME_MAX];
	scanf("%s", name);
	int findname = FindByName(pcon, name);
	if (findname < 0)
	{
		printf("您要删除的联系人不存在。\n");
		return;
	}
	SLErase(pcon, findname);
}
//展示通讯录数据
void ShowContact(contact* pcon)
{
	printf("%s %s %s %s %s\n", "姓名", "性别", "年龄", "电话", "住址");
	for (int i = 0; i < pcon->size; i++)
	{
		printf("%-4s %-4s %-4d %-4s %-4s\n", pcon->a[i].name, pcon->a[i].sex, pcon->a[i].age, pcon->a[i].tel, pcon->a[i].addr);

	}
}
//查找通讯录数据
void FindContact(contact* pcon)
{
	char name[NAME_MAX];
	printf("请输入要查找的用户名称：\n");
	scanf("%s", name);
	int findname = FindByName(pcon, name);
	if (findname < 0)
	{
		printf("您要查找的联系人不存在。\n");
		return;
	}
	printf("%-4s %-4s %-4d %-4s %-4s\n", pcon->a[findname].name, pcon->a[findname].sex, pcon->a[findname].age, pcon->a[findname].tel, pcon->a[findname].addr);
}
//修改通讯录数据
void ModifyContact(contact* pcon)
{
	char name[NAME_MAX];
	printf("请输入要修改的用户名称：\n");
	scanf("%s", name);
	int findname = FindByName(pcon, name);
	if (findname < 0)
	{
		printf("您要修改的联系人不存在，\n");
		return;
	}
	printf("请输入新的用户名称：\n");
	scanf("%s", pcon->a[findname].name);
	printf("请输入新的用户性别：\n");
	scanf("%s", pcon->a[findname].sex);
	printf("请输入新的用户年龄：\n");
	scanf("%d", &pcon->a[findname].age);
	printf("请输入新的用户电话：\n");
	scanf("%s", pcon->a[findname].tel);
	printf("请输入新的用户地址：\n");
	scanf("%s", pcon->a[findname].addr);

	printf("修改成功！\n");

}
//销毁通讯录数据
void DestroyContact(contact* pcon)
{
	SLDestroy(pcon);
}

