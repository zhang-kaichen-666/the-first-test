#include"Contact.h"
#include"SeqList.h"


void contacttest()
{
	contact con;
	InitContact(&con);

	AddContact(&con);
	AddContact(&con);
	AddContact(&con);
	ShowContact(&con);

	DelContact(&con);
	ShowContact(&con);

	FindContact(&con);

	ModifyContact(&con);
	ShowContact(&con);

	DestroyContact(&con);
}
int main()
{
	contacttest();
	return 0;
}