#include"List.h"

void ListTest()
{
	//LTNode* plist = NULL;
	//LTInit(&plist);
	LTNode* plist = LTInit2();
	LTPushBack(plist, 1);
	LTPushBack(plist, 2);
	LTPushBack(plist, 3);
	LTPushBack(plist, 4);
	LTPrint(plist);//1 2 3 4

	LTPushFront(plist, 5);
	LTPushFront(plist, 6);
	LTPushFront(plist, 7);
	LTPrint(plist);//7 6 5 1 2 3 4

	LTPopBack(plist);
	LTPopBack(plist);
	LTPrint(plist);//7 6 5 1 2 

	LTPopFront(plist);
	LTPopFront(plist);
	LTPrint(plist);//5 1 2

	LTNode* find = LTFind(plist, 1);
	LTInsert(find, 11);
	LTPrint(plist);//5 1 11 2

	LTErase(find);
	LTPrint(plist);//5 11 2

	//LTDestroy2(&plist);

	LTDestroy(plist);//传一级指针的时候要手动将plist置为NULL
	/*free(plist);*/   //不能再free()，因为已经在 LTDestroy(plist) 中被free掉了。
	plist = NULL;
}
int main()
{
	ListTest();
	return 0;
}