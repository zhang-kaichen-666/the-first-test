﻿#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>

typedef int LTDataType;
typedef struct ListNode
{
	LTDataType data;
	struct ListNode* next;
	struct ListNode* prev;
}LTNode;

void LTInit(LTNode** pphead);//前提：要传入一个头节点

LTNode* LTInit2();//不需要传入参数，返回一个头节点
void LTDestroy(LTNode* phead);
void LTDestroy2(LTNode** pphead);

LTNode* ListBuyNode(LTDataType x);

//哨兵位的下一个节点是链表的第一个节点
//哨兵位的前一个节点是链表的最后一个节点
void LTPushBack(LTNode* phead,LTDataType x);
void LTPushFront(LTNode* phead, LTDataType x);

void LTPopBack(LTNode* phead);
void LTPopFront(LTNode* phead);

//在pos位置之后插⼊数据
void LTInsert(LTNode* pos, LTDataType x);
void LTErase(LTNode* pos);
LTNode* LTFind(LTNode* phead, LTDataType x);

void LTPrint(LTNode* phead);