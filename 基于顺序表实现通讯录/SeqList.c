#include"SeqList.h"
#include<stdlib.h>
#include<assert.h>

//void SLInit(SL s) //不能这么写    因为s是sl的临时拷贝，s的改变不会影响sl
//{
//	s.a = NULL;
//	s.size = s.capaciy = 0;
//}
void SLInit(SL* ps)
{
	ps->a = NULL;
	ps->size = ps->capacity = 0;
}

void SLDestroy(SL* ps)
{
	if (ps->a)
	{
		free(ps->a);
	}
	ps->a = NULL;
	ps->size = ps->capacity = 0;
}

//判断顺序表是否有足够的空间,如果空间不足就申请空间
void SLCheckCapacity(SL* ps)
{
	// 1)有足够的空间就可以直接插入,      ps->size有效的数据个数 = 最后一个数据的下一个位置

	// 2)空间不够，需要扩容
	//每次增加一个数据，就申请一块空间，假如现在要插入100个数据，则需要申请100次。如果频繁的扩容，会降低程序的性能。扩容次数越少越好。
	//增容：一般以2倍或者1.5倍进行扩容        4 -> 8 -> 16 ->...
	if (ps->size == ps->capacity)
	{
		//空间不足以再插入一个数据
		int newCapcity = ps->capacity == 0 ? 4 : 2 * ps->capacity;
		SLDataType* tmp = (SLDataType*)realloc(ps->a, newCapcity * sizeof(SLDataType));//如果直接用p->a接受，失败
		if (tmp == NULL)
		{
			perror("realloc fail!\n");
			return;
		}
		ps->a = tmp;
		ps->capacity = newCapcity;
	}
}

//void SLPrint(SL* ps)
//{
//	int i = 0;
//	for (i = 0; i < ps->size; i++)
//	{
//		printf("%d ", ps->a[i]);
//	}
//	printf("\n");
//}

bool SLIsEmpty(SL* ps)
{
	assert(ps);
	return ps->size == 0;
}

//尾插
void SLPushBack(SL* ps, SLDataType x)
{
	// (1)柔和的方式
	//if (ps == NULL)
	//{
	//	return;
	//}
	//(2)暴力的方式
	assert(ps);
	SLCheckCapacity(ps);
	//直接插入数据
	ps->a[ps->size++] = x;
}

//头插
void SLPushFront(SL* ps, SLDataType x)
{
	//1)空间不够，扩容
	//2)空间足够，历史数据后移一位
	assert(ps);
	SLCheckCapacity(ps);
	for (size_t i = ps->size; i > 0; i--)
	{
		ps->a[i] = ps->a[i - 1];
	}
	ps->a[0] = x;
	ps->size++;
}

//尾删
void SLPopBack(SL* ps)
{
	assert(ps);
	assert(!SLIsEmpty(ps));
	//ps->a[ps->size - 1] = 0; // 可以不要这一步
	ps->size--;
}

//头删
void SLPopFront(SL* ps)
{
	assert(ps);
	assert(!SLIsEmpty(ps));
	for (int i = 0; i < ps->size - 1; i++)
	{
		//最后一次进来的值为  ps->size - 1
		ps->a[i] = ps->a[i + 1];
	}
	ps->size--;
}

//在指定位置之前插入(pos是下标)
void SLInsert(SL* ps, int pos, SLDataType x)
{
	assert(ps);
	assert(pos >= 0 && pos <= ps->size);
	SLCheckCapacity(ps);
	for (int i = ps->size; i > pos; i--)
	{
		ps->a[i] = ps->a[i - 1];
	}
	ps->a[pos] = x;
	ps->size++;
}


//删除指定位置的数据
void SLErase(SL* ps, int pos)
{
	assert(ps);
	assert(!SLIsEmpty(ps));
	assert(pos >= 0 && pos < ps->size);
	for (int i = pos; i < ps->size - 1; i++)
	{
		ps->a[i] = ps->a[i + 1];
	}
	ps->size--;
}

//bool SLFind(SL* ps, SLDataType x)
//{
//	assert(ps);
//	for (int i = 0; i < ps->size; i++)
//	{
//		if (ps->a[i] == x)//contact时要取出单个成员进行比较，因为结构体不能直接使用==
//		{
//			return true;
//		}
//	}
//	return false;
//}
