#include"SeqList.h"
#include"contact.h"

//void SLtest()
//{
//	SL sl;
//	//初始化
//	SLInit(&sl);
//	//SLPopBack(&sl);//空表时不能删除，会触发assert(!SLIsEmpty(ps))
//	//顺序表的具体操作
//	SLPushBack(&sl, 1);
//	SLPushBack(&sl, 2);
//	SLPushBack(&sl, 3);
//	SLPushBack(&sl, 4);
//	SLPrint(&sl);
//	printf("\n");
//
//	SLPushFront(&sl, 5);
//	SLPushFront(&sl, 6);
//	SLPushFront(&sl, 7);
//	SLPrint(&sl);
//	printf("\n");
//
//	SLPopBack(&sl);
//	SLPrint(&sl);
//	SLPopBack(&sl);
//	SLPrint(&sl);
//	SLPopBack(&sl);
//	SLPrint(&sl);
//	printf("\n");
//
//	SLPopFront(&sl);
//	SLPrint(&sl);
//	SLPopFront(&sl);
//	SLPrint(&sl);
//	SLPopFront(&sl);
//	SLPrint(&sl);
//	printf("\n");
//
//	//
//	SLInsert(&sl, 1, 11);
//	SLPrint(&sl);
//	SLInsert(&sl, 2, 12);
//	SLPrint(&sl);
//
//	SLErase(&sl, 1);
//	SLPrint(&sl);
//
//	if (SLFind(&sl, 11))
//	{
//		printf("找到了");
//	}
//	else if (!SLFind(&sl, 11))
//	{
//		printf("没找到");
//	}
//	//销毁
//	SLDestroy(&sl);
//}

void contact1()
{
	contact con;
	ContactInit(&con);
	//往通讯录中插入数据
	ContactAdd(&con);
	ContactAdd(&con);
	ContactShow(&con);

	//ContactDel(&con);
	//ContactShow(&con);
	ContactModify(&con);
	ContactShow(&con);

	ContactFind(&con);

	ContactDestroy(&con);
}

//通讯录界面
void menu()
{
	printf("******************通讯录************************\n");
	printf("************1.添加联系人       2.删除联系人*****\n");
	printf("************3.修改联系人       4.查找联系人*****\n");
	printf("************5.查看通讯录       0.退出      *****\n");
	printf("************************************************\n");

}
int main()
{
	int op = -1;
	contact con;
	ContactInit(&con);
	do {
		menu();
		printf("请选择您的操作：\n");
		scanf("%d", &op);
		switch (op)
		{
		case 1:
			ContactAdd(&con);
			break;		
		case 2:
			ContactDel(&con);
			break;		
		case 3:
			ContactModify(&con);
			break;		
		case 4:
			ContactFind(&con);
			break;		
		case 5:
			ContactShow(&con);
			break;		
		case 0:
			printf("goodbye.\n");
			break;
		default:
			printf("您输入的数字有误，请重新输入。\n");
			break;
		}
	} while (op != 0);

	ContactDestroy(&con);

	//contact1();
	return 0;
}
