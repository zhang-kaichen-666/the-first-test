﻿#include<stdio.h>
#include<assert.h>
#include<windows.h>

//重叠拷贝使用memmove函数
//int main()
//{
//	int arr1[] = { 1,2,3,4,5,6,7,8,9,10 };
//	memmove(arr1 + 2, arr1, 20);
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", arr1[i]);
//	}
//	return 0;
//}

//模拟实现memmove函数
//数组元素在内存中由低到高存放
//void* my_memmove(void* dest, const void* src, size_t num)
//{
//	void* ret = dest;
//	assert(dest && src);
//	//分情况讨论
//	if (dest < src)
//	{
//		//前->后
//		while (num--)
//		{
//			*(char*)dest = *(char*)src;
//			dest = (char*)dest + 1;
//			src = (char*)src + 1;
//		}
//	}
//	else
//	{
//		//后->前
//		while (num--)
//		{
//			*((char*)dest + num) = *((char*)src + num);
//
//		}
//	}
//	return ret;
//}
//int main()
//{
//	int arr1[] = { 1,2,3,4,5,6,7,8,9,10 };
//	my_memmove(arr1 + 2, arr1, 20);
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", arr1[i]);
//	}
//	return 0;
//}

//3.memset函数:从ptr开始将前num个字节设置成value的值  
//void * memset ( void * ptr, int value, size_t num );
//int main()
//{
//	char arr[] = "hello bit\n";
//	//将其改为xxxxx bit
//	memset(arr,'x',5);
//	printf("%s\n", arr);
//	//xxxxx xxx
//	memset(arr + 6,'x',3);
//	printf("%s\n", arr);
//	//注意点：memset是以字节为单位设置内存值的
//	return 0;
//}

//4.memcmp函数：用来比较内存中的值，按照字节来进行比较。
//int main()
//{
//	//前>后返回大于0的数
//	char arr1[] = "abq";
//	char arr2[] = "abcwertyuiop";
//	int ret = memcmp(arr1, arr2, 3);
//	printf("%d\n", ret);
//
//	//前 == 后返回0
//
//	//前<后返回小于0的数
//	//按照字节来比较
//	int arr3[] = { 1,2,3,4,5 };
//	int arr4[] = { 1,3,5,7,9 };
//	ret = memcmp(arr3, arr4, 5);
//	printf("%d\n", ret);
//	return 0;
//}

//数据在内存中的储存
//1.整数在内存中的存储:补码
// 注意：数据在内存中存储时是2进制
//       但是在vs的窗口展示时为16进制
//2.⼤⼩端字节序和字节序判断
// 当一个数值超过一个字节时，存储在内存中有存储的顺序问题，内存中的存储单元是1字节的
//大端存储模式：将一个数值的低位字节序的内容存储到高地址处，高位字节序的内容存储到低地址处
//                字节的顺序为 11 22 33 44 
//小端存储模式：将一个数值的高位字节序的内容存储到高地址处，低位字节序的内容存储到低地址处
//                字节的顺序为 44 33 22 11
//x86一般为小端存储模式，是大端还是小段由硬件决定

//设计⼀个⼩程序来判断当前机器的字节序
//int check_sys()
//{
//	int a = 1;
//	return (*(char*)&a);
//	//不能使用(char)a,因为此时已经发生截断。
//}
//int main()
//{
//	int a = 1;
//	if (check_sys() == 1)
//	{
//		printf("小端");
//	}
//	else
//		printf("大端");
//	return 0;
//}

//int main()
//{
//	char a = -1;
//	signed char b = -1;
//	unsigned char c = -1;
//	printf("a=%d,b=%d,c=%d", a, b, c);//signed用符号位提升，unsigned用0提升
//	return 0;
//}//-1 -1 255
//char 类型比较特殊，不确定是signed 还是unsigned。取决于编译器。

//int main()
//{
//	char a = -128;	//signed char类型取值范围为-128^127
//	printf("%u\n", a);//%u是按照无符号形式进行打印
//	char b = 128;
//	printf("%u\n", b);
//	return 0;
//}

//int main()
//{
//	char a[1000];
//	int i;
//	for (i = 0; i < 1000; i++)
//	{
//		a[i] = -1 - i;//从-128->0进行轮回
//	}
//	printf("%d", strlen(a));// '\0'的ASCII码值是0，打印结果为128 + 127 == 255。
//	return 0;
//}

//unsigned char i = 0;
//int main()
//{
//	for (i = 0; i <= 255; i++)
//	{
//		printf("hello world\n");
//	}
//	return 0;
//}

//int main()
//{
//	unsigned int i;
//	for (i = 9; i >= 0; i--)
//	{
//		printf("%u\n", i);
//		Sleep(1000);
//	}
//	return 0;
//}

