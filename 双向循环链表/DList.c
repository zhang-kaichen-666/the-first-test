#include"DList.h"
// 创建返回链表的头结点.
ListNode* ListCreate()
{
	ListNode* phead;
	phead = CreatLTNode(-1);
	phead->_next = phead;
	phead->_prev = phead;
	return phead;
}
// 创建新节点
ListNode* CreatLTNode(LTDataType x)
{
	ListNode* newnode = (ListNode*)malloc(sizeof(ListNode));
	if (newnode == NULL)
	{
		perror("newnode malloc");
	}
	newnode->_data = x;
	newnode->_next = NULL;
	newnode->_prev = NULL;
	return newnode;
}
// 双向链表销毁
void ListDestory(ListNode* pHead)
{
	ListNode* pdel = pHead->_next;
	while (pdel != pHead)
	{
		ListNode* next = pdel->_next;
		free(pdel);
		pdel = next;
	}
	pHead->_next = pHead;
	pHead->_prev = pHead;
}
// 双向链表打印
void ListPrint(ListNode* pHead)
{
	assert(pHead);
	ListNode* pcur = pHead->_next;
	printf("哨兵位 <->");
	while (pcur != pHead)
	{
		printf("%d <-> ", pcur->_data);
		pcur = pcur->_next;
	}
	printf("哨兵位\n");
}
// 双向链表尾插
void ListPushBack(ListNode* pHead, LTDataType x)
{
	assert(pHead);
	ListNode* ptail = pHead->_prev;
	ListNode* newnode = CreatLTNode(x);
	newnode->_next = pHead;
	newnode->_prev = ptail;
	ptail->_next = newnode;
	pHead->_prev = newnode;
}
// 双向链表尾删
void ListPopBack(ListNode* pHead)
{
	assert(pHead);
	assert(pHead->_next != pHead);
	ListNode* ptail = pHead->_prev;
	pHead->_prev =  ptail->_prev;
	ptail->_prev->_next = pHead;
	free(ptail);
	ptail = NULL;
}
// 双向链表头插
void ListPushFront(ListNode* pHead, LTDataType x)
{
	assert(pHead);
	ListNode* newnode = CreatLTNode(x);
	newnode->_next = pHead->_next;
	newnode->_prev = pHead;
	pHead->_next->_prev = newnode;
	pHead->_next = newnode;
}
// 双向链表头删
void ListPopFront(ListNode* pHead)
{
	assert(pHead);
	assert(pHead->_next != pHead);
	ListNode* phead = pHead->_next;
	pHead->_next = phead->_next;
	phead->_next->_prev = pHead;
	free(phead);
	phead = NULL;
}
// 双向链表查找
ListNode* ListFind(ListNode* pHead, LTDataType x)
{
	ListNode* pcur = pHead->_next;
	while (pcur != pHead)
	{
		if (pcur->_data == x)
		{
			return pcur;
		}
		pcur = pcur->_next;
	}
	return NULL;
}
// 双向链表在pos的前面进行插入
void ListInsert(ListNode* pos, LTDataType x)
{
	assert(pos);
	ListNode* newnode = CreatLTNode(x);
	newnode->_prev = pos->_prev;
	newnode->_next = pos;
	pos->_prev->_next = newnode;
	pos->_prev = newnode;
}
// 双向链表删除pos位置的节点
void ListErase(ListNode* pos)
{
	pos->_next->_prev = pos->_prev;
	pos->_prev->_next = pos->_next;
	free(pos);
	pos = NULL;
}