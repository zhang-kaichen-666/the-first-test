#include"DList.h"


void test()
{
	ListNode* plist = ListCreate();
	ListPushBack(plist,1);
	ListPushBack(plist,2);
	ListPushBack(plist,3);
	ListPushBack(plist,4);
	ListPrint(plist);//1 2 3 4

	ListPopBack(plist);//1 2 3
	ListPrint(plist);

	ListPushFront(plist, 5);
	ListPrint(plist);//5 1 2 3

	ListPopFront(plist);
	ListPrint(plist);//1 2 3

	ListNode* pos =  ListFind(plist,3);
	ListInsert(pos, 20);
	ListPrint(plist);//1 2 20 3

	pos = ListFind(plist, 20);
	ListErase(pos);
	ListPrint(plist);//1 2 3

	ListDestory(plist);
	ListPrint(plist);
}

int main()
{
	test();
	return 0;
}