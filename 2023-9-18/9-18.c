﻿#include<stdio.h>
#include<stdlib.h>

//1.动态内存开辟函数 - malloc     void* malloc (size_t size);     头文件：stdlib.h
//这个函数向内存申请⼀块连续可⽤的空间，并返回指向这块空间的指针。
//• 如果开辟成功，则返回⼀个指向开辟好空间的指针。
//• 如果开辟失败，则返回⼀个 NULL 指针，因此malloc的返回值⼀定要做检查。
//• 返回值的类型是 void* ，所以malloc函数并不知道开辟空间的类型，具体在使⽤的时候使⽤者⾃⼰来决定。
//• 如果参数 size 为0，malloc的⾏为是标准是未定义的，取决于编译器。
//malloc申请的空间回收：
//1.free回收
//2.自己不释放的时候，程序结束后，也会由操作系统回收
//3.malloc在堆区上申请内存
//int main()
//{
//	int *p = (int *)malloc(10 * sizeof(int)); 
//	if (p == NULL)
//	{
//		perror("malloc");//输出：malloc: + 错误原因
//		return 1;//失败返回
//	}
//	//使用空间
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		*(p + i) = i;
//	}
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", p[i]);
//	}
//
//	//释放
//	free(p);//void free (void* ptr);
//	p = NULL;
//	//free函数⽤来释放动态开辟的内存。
//	//	• 如果参数 ptr 指向的空间不是动态开辟的，那free函数的⾏为是未定义的。
//	//	• 如果参数 ptr 是NULL指针，则函数什么事都不做。
//
//	return 0;//正常返回
//}
//栈区存放 ：局部变量，形参，临时变量
//堆区存放 ：动态分配的空间
//静态区存放 ：全局变量，静态变量

//2.calloc函数       void* calloc(size_t num, size_t size);    申请num个大小为size的空间
//int main()
//{
//	//calloc申请的空间默认被初始化为0
//	//而malloc申请的空间不会初始化
//	int* p = (int*)calloc(10,sizeof(int));
//	if (p == NULL)
//	{
//		perror("calloc");
//		return 1;
//	}
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", p[i]);
//	}
//	free(p);
//	return 0;
//}
//使用时不需要初始化->使用malloc
//需要初始化->calloc
//释放时都用free来释放

//3.realloc函数          void* realloc (void* ptr, size_t size);   ptr 是要调整的内存地址  size 调整之后新⼤⼩
//对空间大小进行调整
//返回值是调整之后的起始内存地址，扩容失败返回NULL
//这个函数调整原内存空间⼤⼩的基础上，还会将原来内存中的数据移动到新的空间。
//调整空间时存在两种情况：
//1.原有空间之后有⾜够⼤的空间：直接原有内存之后直接追加空间，原来空间的数据不发⽣变化。
//2.原有空间之后没有⾜够⼤的空间：在堆空间上另找⼀个合适⼤⼩的连续空间来使⽤。这样函数返回的是⼀个新的内存地址。
//int main()
//{
//	int *p = (int*)malloc(5 * sizeof(int));
//	if (p == NULL)
//	{
//		perror("malloc");
//		return 1;
//	}
//	int i = 0;
//	for (i = 0; i < 5; i++)
//	{
//		*(p + i) = i + 1;
//	}
//	for (i = 0; i < 5; i++)
//	{
//		printf("%d ", p[i]);
//	}
//	printf("\n");
//
//	//调整申请的堆上的内存
//	int *ptr = realloc(p, 40);//不要写成p = realloc(p,40)因为如果扩容失败会导致原来的内存大小丢失
//	//旧空间不需要手动free,因为realloc已经帮助进行free了
//	if (ptr != NULL)
//	{
//		p = ptr;
//		ptr = NULL;//不能free(ptr)，这样会使p所在的空间也被释放掉
//	}
//	else
//	{
//		perror("realloc");
//		return 1;
//	}
//	//使用
//	for (i = 0; i < 10; i++)
//	{
//		*(p + i) = i + 1;
//	}
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", p[i]);
//	}
//	free(p);
//	p = NULL;
//	return 0;
//}

//realloc当参数为NULL时相当于malloc函数
//int main()
//{
//	int* p = (int*)realloc(NULL, 40);
//	return 0;
//}

//常见的动态内存的错误
//1.对NULL指针的解引用操作  
//2.对动态开辟空间的越界访问
//3.对非动态开辟内存使用free释放
//4.用free释放一块动态开辟内存的一部分
//5.对同一块动态内存多次释放
//6.动态开辟空间忘记释放(内存泄漏)

//例1：
//void GetMemory(char* p)
//{
//	p = (char*)malloc(100);
//}
//void Test(void)
//{
//	char* str = NULL;
//	GetMemory(str);
//	strcpy(str, "hello world");
//	printf(str);
//}
//int main()
//{
//	Test();//无法正常使用
//	//用GetMemory( str )后， str并未产生变化，依然是NULL.只是改变的str的一个拷贝的内存的变化
//	return 0;
//}
//下面是改法
//void GetMemory(char** p)
//{
//	*p = (char*)malloc(100);
//}
//void Test(void)
//{
//	char* str = NULL;
//	GetMemory(&str);
//	strcpy(str, "hello world");
//	printf(str);
//}
//int main()
//{
//	Test();
//	return 0;
//}
//另一种改法：在GetMemory加入返回值    return p 

//例2：
//char* GetMemory(void)
//{
//	char p[] = "hello world";
//	return p;//p数组空间销毁
//}
//void Test(void)
//{
//	char* str = NULL;
//	str = GetMemory();
//	printf(str);//非法访问
//}
//int main()
//{
//	Test();
//	return 0;
//}

//柔性数组
//C99 中，结构中的最后⼀个元素允许是未知⼤⼩的数组，这就叫做『柔性数组』成员。
//(在结构中)(最后一个元素)(未知大小的数组)(配合动态内存管理来使用的)
//struct st_type
//{
//	int i;
//	int a[0];//也可写成a[],表示数组大小未知
//};
//柔性数组的特点：
//• 结构中的柔性数组成员前⾯必须⾄少⼀个其他成员。
//• sizeof 返回的这种结构⼤⼩不包括柔性数组的内存。
//• 包含柔性数组成员的结构⽤malloc()函数进⾏内存的动态分配，并且分配的内存应该⼤于结构的⼤⼩，以适应柔性数组的预期⼤⼩。
//使用:
//typedef struct st_type
//{
//	int i;
//	int a[0];//柔性数组成员
//	};
//int main()
//{
//	struct st_type* p = (struct st_type*)malloc(sizeof(struct st_type) + 10 * sizeof(int));
//	if (p == NULL)
//	{
//		perror("malloc");
//		return 1;
//	}
//	int i = 0;
//	p->i = 100;
//	for (i = 0; i < 10; i++)
//	{
//		p->a[i] = i;
//	}
//	int * ptr = (int*)realloc(p->a,sizeof(int)*15);
//	if (ptr == NULL)
//	{
//		perror("realloc");
//		return 1;
//	}
//	else
//	{
//		p->a = ptr;
//		ptr = NULL;
//	}
//	//使用
//	
//	//释放
//	free(p);
//	return 0;
//}

//使用malloc函数模拟开辟一个3*5的整型二维数组，开辟好后，使用二维数组的下标访问形式，访问空间。
//int main()
//{
//	int i = 0;
//	int j = 0;
//	int** p = (int**)malloc(sizeof(int*) * 3);
//	if (p == NULL)
//	{
//		printf("内存不足\n");
//		free(p);
//	}
//	for (i = 0; i < 3; i++)
//	{
//		p[i] = (int*)malloc(sizeof(int) * 5);
//		if (p[i] == NULL)
//		{
//			printf("内存不足");
//			free(p[i]);
//		}
//	}
//	for (i = 0; i < 3; i++)
//	{
//		for (j = 0; j < 5; j++)
//		{
//			*(*(p + i) + j) = 0;
//		}
//	}
//	for (i = 0; i < 3; i++)
//	{
//		for (j = 0; j < 5; j++)
//		{
//			printf("%d ", p[i][j]);
//		}
//		printf("\n");
//	}
//	for (i = 0; i < 3; i++)
//	{
//		free(p[i]);
//	}
//	free(p);
//	return 0;
//}