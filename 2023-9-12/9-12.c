#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<assert.h>
#include<ctype.h>

//判断机器大小端
//int main()
//{
//	int a = 1;
//	if (*((char*)&a) == 1)
//		printf("小端");
//	else
//		printf("大端");
//	return 0;
//}

//atoi函数：把参数 str 所指向的字符串转换为一个整数
//int atoi (const char * str)
//int main()
//{
//	char str[] = "1234 56789";
//	int val = atoi(str);
//	printf("字符串值为:%s,\n转换为整型为%d\n", str, val);
//	return 0;
//}

//模拟实现atoi函数
//int my_atoi(const char* str)
//{
//	assert(str != NULL);
//	int flag = 1;
//	if (*str == '\0')
//		return 0;
//	while(isspace(*str))
//	{
//		str++;
//	}
//	if (*str == '-')
//	{
//		flag = -1;
//		str++;
//	}
//	if (*str == '+')
//	{
//		flag = 1;
//		str++;
//	}
//	int n = 0;
//	while (*str != '\0')
//	{
//		if (isspace(*str))
//			return n*flag;
//		if (isdigit(*str) == 0)
//			return n*flag;
//		n = n * 10 + (*str - '0');
//		str++;
//	}
//	
//	return n*flag;
//}
//int main()
//{
//	char str[] = "   -1234 56789";
//	int val = my_atoi(str);
//	printf("字符串值为:%s,\n转换为整型为%d\n", str, val);
//	return 0;
//}