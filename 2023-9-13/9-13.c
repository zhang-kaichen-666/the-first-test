﻿#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stddef.h>

//自定义类型1:结构体
//结构体中可以包含各种类型的数据，用来描述一个复杂对象的各种属性。

//使用结构体描述一个学生
//struct Stu
//{
//	char name[20];
//	int age;
//	float score;
//};//这个分号不可省略

//struct Stu
//{
//	char name[20];
//	int age;
//	float score;
//}s4,s5;//全局变量
//int main()
//{
//	struct Stu s1 = {"张三",20,98.5f};//局部变量及初始化
//	struct Stu s2 = {"李四",33,68.5f};
//	struct Stu s3 = { .age = 22,.name = "王五",.score = 55.5f };//另一种初始化方法
//	printf("%s %d %f\n", s1.name, s1.age, s1.score);
//	return 0;
//}

//匿名结构体类型
//struct
//{
//	int a;
//	char b;
//	float c;
//}s = {0};//这种方法只能在这里命名
//struct
//{
//	int a;
//	char b;
//	float c;
//}*ps;
//int main()
//{
//	ps = &s;//error:虽然结构类型的成员一摸一样，但是编译器认为等号两边是不同的结构体类型
//}

//结构体自引用
//struct Node
//{
//	int data;
//	struct Node* next;//不能直接放struct Node next  因为结构体占用内存会无穷大
//};

//这种写法也是不行的
//因为Node是对前⾯的匿名结构体类型的重命名产⽣的，但是在匿名结构体内部提前使⽤Node类型来创建成员变量，这是不⾏的。
//typedef struct
//{
//	int data;
//	Node* next;
//}Node;
//解决方案如下
//typedef struct Node
//{
//	int data;
//	struct Node* next;
//}Node;

//结构体内存对⻬(拿空间来换取时间)(否则读取一个数据可能执行两次内存访问)
//1. 结构体的第⼀个成员对⻬到相对结构体变量起始位置偏移量为0的地址处
//2. 其他成员变量要对⻬到某个数字（对⻬数）的整数倍的地址处。
//对⻬数 = 编译器默认的⼀个对⻬数 与 该成员变量⼤⼩的 较⼩值。
//- VS中默认的值为8
//- Linux中没有默认对⻬数，对⻬数就是成员⾃⾝的⼤⼩
//3. 结构体总⼤⼩为最⼤对⻬数（结构体中每个成员变量都有⼀个对⻬数，所有对⻬数中最⼤的）的
//整数倍。
//4. 如果嵌套了结构体的情况，嵌套的结构体成员对⻬到⾃⼰的成员中最⼤对⻬数的整数倍处，结构
//体的整体⼤⼩就是所有最⼤对⻬数（含嵌套结构体中成员的对⻬数）的整数倍。
//struct S1
//{
//	char c1;
//	char c2;
//	int a;
//};
//struct S2
//{
//	char c1;
//	int a;
//	char c2;
//};
//struct S3
//{
//	double d;
//	char c;
//	int i;
//};
//struct S4
//{
//	char c1;
//	struct S3 s3;
//	double d;
//};
//int main()
//{
//	struct S1 s1 = {'a','b',100};
//	struct S2 s2 = { 'a',100,'b'};
//	printf("%zd\n", sizeof(s1));//8
//	printf("%zd\n", sizeof(s2));//12
//	printf("%zd\n", sizeof(struct S3));//16
//	printf("%zd\n", sizeof(struct S4));//32
//	//offsetof();//宏，计算结构体成员相较于起始位置偏移量
//	printf("%d\n", offsetof(struct S4, c1));//0
//	printf("%d\n", offsetof(struct S4, s3));//8
//	printf("%d\n", offsetof(struct S4, d));//24
//	return 0;
//}
//设计原则：情况允许的情况下，将占用内存小的成员尽量集中在一起。
//修改默认对齐数如下：
//#pragma pack(n):设置默认对齐数为n,一般设置为2的次方数
//#pragma pack():取消设置的对齐数

//结构体传参:
//struct S
//{
//	int data[1000];
//	int num;
//};
//struct S s = { {1,2,3,4}, 1000 };
////结构体传参
//void print1(struct S s)
//{
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", s.data[i]);
//	}
//	printf("\n");
//	printf("%d\n", s.num);
//}
////结构体地址传参
//void print2(struct S* ps)
//{
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", ps->data[i]);
//	}
//	printf("\n");
//	printf("%d\n", ps->num);
//}
//int main()
//{
//
//	print1(s); //传结构体-传值调用
//	print2(&s); //传地址-传址调用
//	return 0;
//}